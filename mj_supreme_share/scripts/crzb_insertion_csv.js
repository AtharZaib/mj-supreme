const csv_parser = require('csv')['parse']
const fs = require('fs')
const _ = require('lodash')

const mysql = require("mysql")

const config = require('../server/config.js')

const db = mysql.createPool((config.dev === true) ? config.db.local : config.db.live)
const db_util = require('../server/func/db-util.js')
const dir_path = __dirname + "/crzb_lists/csvs"
// const list = require('./crzb_lists/list_final.json')

call()

async function call() {
  return await new Promise(resolve => {

    fs.readdir(dir_path, async function (err, files) {
      if (err) return console.log(err)

      let thr_err
      let f_inc = 0
      let reg_par = null
      for (file of files) {
        f_inc++
        let data = fs.readFileSync(dir_path + '/' + file, 'utf8')
        await new Promise(res_2 => {
          csv_parser(data, {
            columns: true,
            ltrim: true,
            rtrim: true
          }, function (err, records) {
            if (err) {
              thr_err = err
              return res_2()
            }
            db.getConnection(function (err, connection) {
              if (err) {
                thr_err = err
                return res_2()
              } else {
                db_util.connectTrans(connection, async function (resolve_q, err_cb) {
                    let row_err = null
                    let zon_par = null
                    let bar_par = null
                    let branch_inc = 0,
                      zone_inc = 0

                    if (f_inc === 1) {
                      // Country insertion start
                      await new Promise(res_1 => {
                        connection.query(
                          `INSERT INTO \`crzb_list\` SET ?`, {
                            name: "Pakistan",
                            rd_id: "PK",
                            parent_id: 0,
                            type: 0
                          },
                          function (error, result) {
                            if (error) {
                              row_err = error
                            } else {
                              reg_par = result.insertId
                              console.log("insert country", "Pakistan")
                            }
                            res_1()
                          }
                        )
                      })
                      if (row_err) {
                        err_cb(row_err)
                        return resolve_q()
                      }
                      // Country insertion end
                    }

                    for (row of records) {
                      // insertion region
                      if (row.REGIONAL) {
                        await new Promise(res_1 => {
                          connection.query(
                            `INSERT INTO \`crzb_list\` SET ?`, {
                              name: _.startCase(_.toLower(row.REGIONAL)),
                              rd_id: getFileName(file),
                              parent_id: reg_par,
                              type: 1
                            },
                            function (error, result) {
                              if (error) {
                                row_err = error
                              } else {
                                zon_par = result.insertId
                                console.log("insert region", row.REGIONAL)
                              }
                              res_1()
                            }
                          )
                        })
                        if (row_err) break;
                      }

                      if (row.ZONAL && zon_par) {
                        branch_inc = 0
                        await new Promise(res_1 => {
                          zone_inc++
                          connection.query(
                            `INSERT INTO \`crzb_list\` SET ?`, {
                              name: _.startCase(_.toLower(row.ZONAL)),
                              rd_id: getRdChar(row.ZONAL, zone_inc),
                              parent_id: zon_par,
                              type: 2
                            },
                            function (error, result) {
                              if (error) {
                                row_err = error
                              } else {
                                bar_par = result.insertId
                                console.log("insert zone", row.ZONAL)
                              }
                              res_1()
                            }
                          )
                        })
                        if (row_err) break;
                      }

                      if (row.BRANCH && bar_par) {
                        branch_inc++
                        await new Promise(res_1 => {
                          let parms = []
                          if (branch_inc === 1) {
                            parms.push(['Others', '00', bar_par, 4])
                          }
                          parms.push([
                            _.startCase(_.toLower(row.BRANCH)),
                            getIntRd(branch_inc, 2, '00'),
                            bar_par,
                            3
                          ])
                          connection.query(
                            `INSERT INTO \`crzb_list\` (name, rd_id, parent_id, type) VALUES ?`, [parms],
                            function (error, result) {
                              if (error) {
                                row_err = error
                              } else {
                                console.log("insert branch", row.BRANCH)
                              }
                              res_1()
                            }
                          )
                        })
                        if (row_err) break;
                      }

                      if (row_err) break;
                    }
                    if (row_err) {
                      err_cb(row_err)
                    }
                    resolve_q()
                  },
                  function (error) {
                    if (error) {
                      thr_err = error
                    } else {
                      console.log("successfully file insertion", file)
                    }
                    return res_2()
                  })
              }
            })
          })
        })
        if (thr_err) break
      }
      if (thr_err) console.log(thr_err)
    })

    return resolve()
  })
}

function getIntRd(numb_int, min_str, prep_str) {
  let new_val = numb_int.toString()
  new_val = (new_val.length < min_str) ? (prep_str + new_val).substr(-(min_str), min_str) : new_val

  return new_val
}

function getRdChar(str, inc) {
  let numb = getIntRd(inc ? inc : 1, 2, '00')
  return str.split('')[0] + numb
}

function getFileName(name) {
  let splt = name.split('.')
  splt.splice(-1, 1)
  return splt.join('.')
}