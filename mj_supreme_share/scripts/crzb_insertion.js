const mysql = require("mysql")

const config = require('../server/config.js')

const db = mysql.createPool((config.dev === true) ? config.db.local : config.db.live)
const db_util = require('../server/func/db-util.js')
const list = require('./crzb_lists/list.json')

call()

async function call() {
  return await new Promise(resolve => {
    db.getConnection(async function (err, connection) {
      if (err) {
        console.log(err)
        return resolve()
      } else {
        db_util.connectTrans(connection, async function (resolve_q, err_cb) {

            let c_inc = 0;
            for (country of list.countries) {
              c_inc++;
              let r_inc = 0;

              let c_last_id = 0
              await new Promise(res_1 => {
                connection.query(
                  `INSERT INTO \`crzb_list\` SET ?`, {
                    name: country.name,
                    rd_id: country.rd_id,
                    parent_id: 0,
                    type: 0
                  },
                  function (error, result) {
                    if (error) {
                      err_cb(error)
                      resolve_q()
                    } else {
                      c_last_id = result.insertId
                      console.log("cn", c_inc)
                    }
                    res_1()
                  }
                )
              })

              for (region of country.regions) {
                r_inc++;
                let z_inc = 0;

                let r_last_id = null
                await new Promise(res_2 => {
                  connection.query(
                    `INSERT INTO \`crzb_list\` SET ?`, {
                      name: region.name,
                      rd_id: region.rd_id,
                      parent_id: c_last_id,
                      type: 1
                    },
                    function (error, result) {
                      if (error) {
                        err_cb(error)
                        resolve_q()
                      } else {
                        r_last_id = result.insertId
                        console.log("rg", r_inc+"-"+c_inc)
                      }
                      res_2()
                    })
                })


                for (zone of region.zones) {
                  z_inc++;
                  let b_inc = 0;

                  let z_last_id = null
                  await new Promise(res_3 => {
                    connection.query(
                      `INSERT INTO \`crzb_list\` SET ?`, {
                        name: zone.name,
                        rd_id: zone.rd_id + (z_inc),
                        parent_id: r_last_id,
                        type: 2
                      },
                      function (error, result) {
                        if (error) {
                          err_cb(error)
                          resolve_q()
                        } else {
                          z_last_id = result.insertId
                          console.log("zn", z_inc+"-"+r_inc+"-"+c_inc)
                        }
                        res_3()
                      })
                  })

                  for (branch of zone.branches) {
                    b_inc++;
                    let uc_inc = 0
                    let br_last_id = null
                    await new Promise(res_4 => {
                      let b_name = typeof branch === 'object' ? branch.name : branch
                      connection.query(
                        `INSERT INTO \`crzb_list\` SET ?`, {
                          name: b_name,
                          rd_id: getIntRd(b_inc, 2, "00"),
                          parent_id: z_last_id,
                          type: 3
                        },
                        function (error, result) {
                          if (error) {
                            err_cb(error)
                            resolve_q()
                          } else {
                            br_last_id = result.insertId
                            console.log("br", b_inc+"-"+z_inc+"-"+r_inc+"-"+c_inc)
                          }
                          res_4()
                        })
                    })
                    if (typeof branch === 'object') {
                      for (uc_name of branch.uc) {
                        uc_inc++
                        await new Promise(res_5 => {
                          connection.query(
                            `INSERT INTO \`franchises\` SET ?`, {
                              name: uc_name,
                              rd_id: getIntRd(uc_inc, 2, "00"),
                              branch_id: br_last_id
                            },
                            function (error, result) {
                              if (error) {
                                err_cb(error)
                              }
                              console.log("uc", uc_inc+"-"+b_inc+"-"+z_inc+"-"+r_inc+"-"+c_inc)
                              resolve_q()
                              res_5()
                            })
                        })
                      }
                    }
                  }
                }
              }
            }

          },
          function (error) {
            if (error) {
              console.log(error)
            } else {
              console.log("success fully insertion")
            }
          })
      }
    })
  })
}

function getIntRd(numb_int, min_str, prep_str) {
  let new_val = numb_int.toString()
  new_val = (new_val.length < min_str) ? (prep_str + new_val).substr(-(min_str), min_str) : new_val

  return new_val
}