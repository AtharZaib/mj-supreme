const mysql = require("mysql")

const config = require('../server/config.js')

const db = mysql.createPool((config.dev === true) ? config.db.local : config.db.live)
const db_util = require('../server/func/db-util.js')
const list = require('./crzb_lists/list_final.json')

call()

async function call() {
  return await new Promise(resolve => {
    db.getConnection(async function (err, connection) {
      if (err) {
        console.log(err)
        return resolve()
      } else {
        db_util.connectTrans(connection, async function (resolve_q, err_cb) {

            let c_inc = 0;
            for (country of list.countries) {
              c_inc++;
              let r_inc = 0;

              let c_last_id = 0
              await new Promise(res_1 => {
                connection.query(
                  `INSERT INTO \`crc_list\` SET ?`, {
                    name: country.name,
                    rd_id: country.rd_id,
                    parent_id: 0,
                    type: 0
                  },
                  function (error, result) {
                    if (error) {
                      err_cb(error)
                      resolve_q()
                    } else {
                      c_last_id = result.insertId
                      console.log("cn", c_inc)
                    }
                    res_1()
                  }
                )
              })

              for (region of country.regions) {
                r_inc++;
                let ct_inc = 0;

                let r_last_id = null
                await new Promise(res_2 => {
                  connection.query(
                    `INSERT INTO \`crc_list\` SET ?`, {
                      name: region.name,
                      rd_id: region.rd_id,
                      parent_id: c_last_id,
                      type: 1
                    },
                    function (error, result) {
                      if (error) {
                        err_cb(error)
                        resolve_q()
                      } else {
                        r_last_id = result.insertId
                        console.log("rg", r_inc + "-" + c_inc)
                      }
                      res_2()
                    })
                })


                for (city of region.cities) {
                  ct_inc++;

                  await new Promise(res_3 => {
                    connection.query(
                      `INSERT INTO \`crc_list\` SET ?`, {
                        name: city,
                        rd_id: getIntRd(ct_inc, 2, '00'),
                        parent_id: r_last_id,
                        type: 2
                      },
                      function (error, result) {
                        if (error) {
                          err_cb(error)
                          resolve_q()
                        } else {
                          console.log("ct", ct_inc + "-" + r_inc + "-" + c_inc)
                        }
                        resolve_q()
                        res_3()
                      })
                  })
                }
              }
            }

          },
          function (error) {
            if (error) {
              console.log(error)
            } else {
              console.log("success fully insertion")
            }
          })
      }
    })
  })
}

function getIntRd(numb_int, min_str, prep_str) {
  let new_val = numb_int.toString()
  new_val = (new_val.length < min_str) ? (prep_str + new_val).substr(-(min_str), min_str) : new_val

  return new_val
}