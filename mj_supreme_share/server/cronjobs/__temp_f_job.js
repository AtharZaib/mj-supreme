db_util.connectTrans(connection, async function (resolve_q, err_cb) {
    connection.query(
      `select count(*) as tot_rows from (
        select 
          count(*) as total_trans
        from info_var_m as info_mem
        join transactions_m as trans
        on info_mem.member_id = trans.member_id and (if((info_mem.pending*1) > 0, info_mem.last_p_tr_id <= trans.id, false) OR if((info_mem.wallet*1) > 0, info_mem.last_w_tr_id <= trans.id, false))
        group by trans.member_id
      ) as \`data\``,
      async function (error, result) {
        if (error) {
          err_cb(error)
        } else {
          let tot_rows = result.length > 0 ? result[0].tot_rows : 0;
          let err;
          f1: for (let i = 0; i < tot_rows; i++) {
            await new Promise(innRes1 => {
              connection.query(
                `select 
                    info_mem.member_id,
                    info_mem.last_p_tr_id,
                    info_mem.last_w_tr_id,
                    ifnull((select sum(debit*1) from transactions_m where member_id=info_mem.member_id and (if((info_mem.pending*1) > 0, id < info_mem.last_p_tr_id, false) OR if((info_mem.wallet*1) > 0, id < info_mem.last_w_tr_id, false) )), 0) as old_debit,
                    ifnull((select sum(credit*1) from transactions_m where member_id=info_mem.member_id and (if((info_mem.pending*1) > 0, id < info_mem.last_p_tr_id, false) OR if((info_mem.wallet*1) > 0, id < info_mem.last_w_tr_id, false) )), 0) as old_credit,
                    
                    count(*) as total_trans
                from info_var_m as info_mem
                
                join transactions_m as trans
                on info_mem.member_id = trans.member_id and (if((info_mem.pending*1) > 0, info_mem.last_p_tr_id <= trans.id, false) OR if((info_mem.wallet*1) > 0, info_mem.last_w_tr_id <= trans.id, false))
                
                group by trans.member_id
                order by trans.created_at
                
                limit 1
                offset ${i}`,
                async function (error, result) {
                  if (error) {
                    err = error;
                    return innRes1()
                  } else {
                    if (result.length > 0) {
                      let mem_id = result[0].member_id
                      let trans_start_id = parseInt(result[0].pending) > 0 ? result[0].last_p_tr_id : result[0].last_w_tr_id

                      let old_debit = parseInt(result[0].old_debit)
                      let old_credit = parseInt(result[0].old_credit)

                      let available = 0,
                        wallet = 0,
                        pending = 0,
                        last_w_tr_id = null,
                        last_p_tr_id = null
                      let tot_trans = result[0].total_trans
                      console.log('mem_id', mem_id, '\t', 'total_trans', tot_trans)
                      bar.start(tot_trans, 0)
                      f2: for (let i2 = 0; i2 < tot_trans; i2++) {
                        await new Promise(innRes2 => {
                          connection.query(
                            `select 
                              *
                            from transactions_m
                            where member_id=${mem_id} and id >= ${trans_start_id}
                            order by created_at
                            limit 1
                            offset ${i2}`,
                            function (error, result) {
                              if (error) {
                                err = error;
                                return innRes2()
                              } else {
                                if (result.length > 0) {
                                  bar.update(i2 + 1)
                                  old_debit += parseInt(result[0].debit)
                                  old_credit += parseInt(result[0].credit)

                                  let trans_date_diff = job_dat.diff(moment(new Date(result[0].created_at)), 'd')

                                  if (trans_date_diff > 3 && trans_date_diff <= 18) {
                                    pending += parseInt(result[0].debit)
                                    if (last_p_tr_id == null) {
                                      last_p_tr_id = result[0].id
                                    }
                                  } else if (trans_date_diff <= 3) {
                                    wallet += parseInt(result[0].debit)
                                    if (last_w_tr_id == null) {
                                      last_w_tr_id = result[0].id
                                    }
                                  }
                                }
                                return innRes2()
                              }
                            }
                          )
                        })
                        if (err) break f2;
                      }
                      bar.stop()
                      if (err) {
                        return innRes1()
                      }
                      available = (old_debit - old_credit) - wallet - pending;
                      connection.query(
                        `update info_var_m set ? where member_id=${mem_id}`, {
                          wallet,
                          pending,
                          available,
                          last_p_tr_id,
                          last_w_tr_id
                        },
                        function (error) {
                          if (error) {
                            err = error
                          }
                          return innRes1()
                        }
                      )
                    } else {
                      return innRes1()
                    }
                  }

                }
              )
            })
            if (err) break f1;
            // if (i === 2) break f1;
          }
          if (err) {
            err_cb(err)
          }
        }
        return resolve_q()
      })
  },
  function (error) {
    if (error) {
      console.log(error)
    } else {
      console.log("Successfully job done. Job End:", moment().format('YYYY-MM-DD HH:mm:ss'))
      setTimeout(call, 60000)
    }
    return resolve()
  })