const fs = require('fs')

const dirPath = __dirname + "/../uploads/reports"

setInterval(call, 1500)

async function call() {

    return await new Promise(resolve => {
        fs.readdir(dirPath, function (err, result) {
            if (err) {
                console.log(err)
                return resolve()
            } else {
                console.log(result)
            }
        })
    })
}