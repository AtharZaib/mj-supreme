const fs = require('fs')

const confPath = __dirname + "/../config.js"
const client_confPath = __dirname + "/../../client_config.js"
const defaultLtPath = __dirname + "/../../layouts/default.vue"

call()

async function call() {

    return await new Promise(resolve => {

        let readFileConf = fs.readFileSync(confPath, 'utf8')
        let readFileCltConf = fs.readFileSync(client_confPath, 'utf8')
        let defaultFile = fs.readFileSync(defaultLtPath, 'utf8')

        let repFileDefLt = '',
            defInc = 0
        defaultFile.split('\r\n').forEach(function (line) {
            defInc++
            let alrdCom = line.indexOf(' //autocomment ')

            if (alrdCom === 0)
                line = line.replace(' //autocomment ', '')

            if (defInc > 1)
                repFileDefLt += '\r\n' + line
            else
                repFileDefLt += line

        })
        fs.writeFileSync(defaultLtPath, repFileDefLt)

        let repFileClnConf = readFileCltConf.replace(/dev: true/, 'dev: false')
        fs.writeFileSync(client_confPath, repFileClnConf)

        let repFileConf = '',
            inc = 0

        readFileConf.split('\r\n').forEach(function (line) {
            inc++
            let findLine = line.indexOf(' //autocomment ')

            if (findLine === 0)
                line = line.replace(' //autocomment ', '')

            if (inc > 1)
                repFileConf += '\r\n' + line
            else
                repFileConf += line
        })
        repFileConf = repFileConf.replace(/dev: true/, 'dev: false')
        fs.writeFileSync(confPath, repFileConf)

        return resolve()

    })
}