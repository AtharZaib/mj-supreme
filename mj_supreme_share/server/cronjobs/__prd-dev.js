const fs = require('fs')

const confPath = __dirname + "/../config.js"
const client_confPath = __dirname + "/../../client_config.js"
const defaultLtPath = __dirname + "/../../layouts/default.vue"

call()

async function call() {

    return await new Promise(resolve => {

        let readFileConf = fs.readFileSync(confPath, 'utf8')
        let readFileCltConf = fs.readFileSync(client_confPath, 'utf8')
        let defaultFile = fs.readFileSync(defaultLtPath, 'utf8')


        let commentDefLt = false,
            repFileDefLt = '',
            defInc = 0
        defaultFile.split('\r\n').forEach(function (line) {
            defInc++
            let alrdCom = line.indexOf(' //autocomment ')
            let findCTagE = line.indexOf('//comment-in-dev-end')
            if (findCTagE > -1) {
                commentDefLt = false
            }

            if (commentDefLt && alrdCom !== 0) {
                repFileDefLt += '\r\n //autocomment ' + line
            } else {
                if (defInc > 1)
                    repFileDefLt += '\r\n' + line
                else
                    repFileDefLt += line
            }

            let findCTagS = line.indexOf('//comment-in-dev-start')
            if (findCTagS > -1) {
                commentDefLt = true
            }
        })
        fs.writeFileSync(defaultLtPath, repFileDefLt)

        let repFileClnConf = readFileCltConf.replace(/dev: false/, 'dev: true')
        fs.writeFileSync(client_confPath, repFileClnConf)

        let repFileConf = '',
            comment = false,
            inc = 0

        readFileConf.split('\r\n').forEach(function (line) {
            inc++
            let findLine = line.indexOf('live: {')
            let lastLine = line.indexOf('local: {')
            let alrdCom = line.indexOf(' //autocomment ')
            if (comment === false && findLine > -1)
                comment = true

            if (comment === true && lastLine > -1)
                comment = false

            if (comment && alrdCom !== 0) {
                repFileConf += '\r\n //autocomment ' + line
            } else {
                if (inc > 1)
                    repFileConf += '\r\n' + line
                else
                    repFileConf += line
            }
        })
        repFileConf = repFileConf.replace(/dev: false/, 'dev: true')
        fs.writeFileSync(confPath, repFileConf)



        return resolve()

    })
}