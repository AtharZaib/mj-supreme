require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./nuxt.config.js":
/*!************************!*\
  !*** ./nuxt.config.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const config = __webpack_require__(/*! ./server/config */ "./server/config.js");

module.exports = {
  dev: "development" !== 'production',
  loading: '~/components/loadingComp.vue',
  head: {
    title: 'MJ SUPREME',
    meta: [{
      charset: 'utf-8'
    }, {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1'
    }, {
      hid: 'description',
      name: 'description',
      content: 'Build your Future'
    }],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.png'
    }, {
      rel: 'stylesheet',
      href: '/fontawesome/css/all.min.css'
    }, {
      rel: 'stylesheet',
      href: '/vendors/slick/slick.css'
    }, {
      rel: 'stylesheet',
      href: '/vendors/slick/slick-theme.css'
    }],
    script: [{
      src: '/js/jquery-3.3.1.min.js'
    }, {
      src: '/vendors/slick/slick.min.js'
    }]
  },
  css: ['~/css/main.css'],
  plugins: ['~/plugins/s-vue-validator.js', {
    src: '~/plugins/route-chg.js',
    ssr: false
  }, {
    src: '~/plugins/axios.js',
    ssr: false
  }, {
    src: '~/plugins/socketio.js',
    ssr: false
  }, {
    src: '~/plugins/localstorage.js',
    ssr: false
  }],
  modules: ['@nuxtjs/axios', ['nuxt-buefy', {
    defaultIconPack: 'fas',
    materialDesignIcons: false
  }]],
  axios: {
    // baseURL: (config.dev) ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com'
    proxy: true
  },
  proxy: {
    '/api/': {
      target: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
      changeOrigin: false,
      prependPath: false
    }
  },
  router: {
    scrollBehavior: function (to, from, savedPosition) {
      return {
        x: 0,
        y: 0
      };
    }
  },
  build: {// vendors: ['babel-polyfill'],
    // extend(config, {
    //   isClient
    // }) {
    //   if (!isClient) {
    //     config.externals.splice(0, 0, function (context, request, callback) {
    //       if (/^vue2-google-maps($|\/)/.test(request)) {
    //         callback(null, false)
    //       } else {
    //         callback()
    //       }
    //     })
    //   }
    // }
  } //mode: "spa"

};

/***/ }),

/***/ "./server/apis/admin.js":
/*!******************************!*\
  !*** ./server/apis/admin.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_utils = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.get('/wallet', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('SELECT wallet FROM `company_var` WHERE id=1', function (error, results, fields) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let comp_wallet = 0;

          if (results.length > 0) {
            comp_wallet = results[0].wallet !== null ? results[0].wallet : 0;
          }

          connection.query('SELECT SUM(wallet) as user_wallet FROM info_var_m', function (error, results, fields) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              let user_wallet = 0;

              if (results.length > 0) {
                user_wallet = results[0].user_wallet !== null ? results[0].user_wallet : 0;
              }

              res.json({
                comp_wallet,
                user_wallet
              });
            }
          });
        }
      });
    }
  });
});
router.post('/withdraw', function (req, res) {
  if (/^[1-9]+[0-9]*$/.test(req.body.amount)) {
    let amount = parseInt(req.body.amount);
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.beginTransaction(async function (err) {
          if (err) {
            connection.release();
            res.status(500).json({
              err
            });
          } else {
            let throw_error = null;
            await new Promise(resolve => {
              connection.query("INSERT INTO `transactions_comp` SET ?", {
                remarks: `Deduct amount in your wallet Rs.${amount}/- From Withdrawal.`,
                credit: amount
              }, function (error, results) {
                if (error) {
                  throw_error = error;
                  return resolve();
                } else {
                  connection.query('INSERT INTO `notifications` SET ?', {
                    from_type: 1,
                    to_type: 1,
                    from_id: 1,
                    to_id: 1,
                    message: `Successfully Withdrawal Amount Rs.${amount}/-`,
                    notify_type: 0
                  }, function (error, results) {
                    if (error) {
                      throw_error = error;
                      return resolve();
                    } else {
                      connection.query('SELECT wallet FROM company_var WHERE id=1', function (error, results, fields) {
                        if (error) {
                          throw_error = error;
                          return resolve();
                        } else {
                          let upd_wallet = parseInt(results[0].wallet) - amount;
                          connection.query('UPDATE company_var SET wallet=? WHERE id=1', upd_wallet, function (error, results, fields) {
                            if (error) {
                              throw_error = error;
                            }

                            return resolve();
                          });
                        }
                      });
                    }
                  });
                }
              });
            });

            if (throw_error) {
              return connection.rollback(function () {
                connection.release();
                res.status(500).json({
                  throw_error
                });
              });
            } else {
              connection.commit(function (err) {
                if (err) {
                  return connection.rollback(function () {
                    connection.release();
                    res.status(500).json({
                      err
                    });
                  });
                } else {
                  connection.release();
                  res.json({
                    status: true
                  });
                }
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid parameters!"
    });
  }
});
router.get("/member_counts", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('SELECT COUNT(*) AS total, SUM(is_paid_m) as paid_total FROM `members`', function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data: {
              paid_mem: results[0].paid_total,
              un_paid_mem: results[0].total - results[0].paid_total
            }
          });
        }
      });
    }
  });
});
router.get("/monthly_reg_members", function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let throw_error = null;
      let grab_months = {
        1: {},
        2: {},
        3: {},
        4: {},
        5: {},
        6: {},
        7: {},
        8: {},
        9: {},
        10: {},
        11: {},
        12: {}
      };
      let date = moment().set('M', 0).set('Y', moment().get('Y') - 1).endOf('Y');

      for (month in grab_months) {
        await new Promise(resolve => {
          date.add(1, 'month');
          let start = date.clone().startOf('month').format("YYYY-MM-DD HH-mm-ss");
          let end = date.clone().endOf('month').format("YYYY-MM-DD HH-mm-ss");
          connection.query('SELECT COUNT(*) as count FROM members WHERE is_paid_m = 1 AND created_at >= ? AND created_at <= ?', [start, end], function (error, results) {
            if (error) {
              throw_error = error;
              resolve();
            } else {
              grab_months[month]['paid'] = results[0].count;
              connection.query('SELECT COUNT(*) as count FROM members WHERE is_paid_m = 0 AND created_at >= ? AND created_at <= ?', [start, end], function (error, results) {
                if (error) {
                  throw_error = error;
                  resolve();
                } else {
                  grab_months[month]['un_paid'] = results[0].count;
                  resolve();
                }
              });
            }
          });
        });

        if (throw_error) {
          break;
        }
      }

      connection.release();

      if (throw_error) {
        res.status(500).json({
          error: throw_error
        });
      } else {
        res.json({
          data: grab_months
        });
      }
    }
  });
});
router.get('/total_cm', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query("SELECT SUM(amount) as total_paid FROM commissions WHERE status=1", function (error, results) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let total_paid = results[0].total_paid !== null ? results[0].total_paid : 0;
          connection.query("SELECT SUM(amount) as total_un_paid FROM commissions WHERE status=0", function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              let total_un_paid = results[0].total_un_paid !== null ? results[0].total_un_paid : 0;
              res.json({
                paid: total_paid,
                un_paid: total_un_paid
              });
            }
          });
        }
      });
    }
  });
});
router.get('/trans_list', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`SELECT SUM(debit) - SUM(credit) as tot_balance
        FROM transactions_comp`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_balance = result[0].tot_balance;
          connection.query(`SELECT COUNT(*) as tot_rows 
              FROM transactions_comp
              ${search !== '' ? 'WHERE id LIKE ? OR remarks LIKE ? OR debit LIKE ? OR credit LIKE ? OR created_at LIKE ?' : ''}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, result) {
            if (error) {
              connection.release();
              res.status(500).json({
                error
              });
            } else {
              let tot_rows = result[0].tot_rows;
              connection.query(`SELECT * 
                    FROM transactions_comp
                    ${search !== '' ? 'WHERE id LIKE ? OR remarks LIKE ? OR debit LIKE ? OR credit LIKE ? OR created_at LIKE ?' : ''}
                    ORDER BY id DESC
                    LIMIT ${limit}
                    OFFSET ${offset}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, main_result) {
                if (error) {
                  connection.release();
                  res.status(500).json({
                    error
                  });
                } else {
                  if (main_result.length > 0) {
                    connection.query(`SELECT SUM(debit) - SUM(credit) as rows_balance, 
                                  (SELECT SUM(debit) - SUM(credit) FROM transactions_comp where id < ${main_result[main_result.length - 1].id}) as last_balance
                              FROM transactions_comp
                              WHERE
                              (id >= ${main_result[main_result.length - 1].id} AND id <= ${main_result[0].id})`, [req.params.id, req.params.id], function (error, result) {
                      connection.release();

                      if (error) {
                        res.status(500).json({
                          error
                        });
                      } else {
                        let rows_balance = result[0].rows_balance !== null ? result[0].rows_balance : 0;
                        let last_balance = result[0].last_balance !== null ? result[0].last_balance : 0;
                        res.json({
                          data: main_result,
                          tot_rows,
                          tot_balance,
                          last_balance,
                          rows_balance
                        });
                      }
                    });
                  } else {
                    res.json({
                      data: main_result,
                      tot_rows,
                      tot_balance,
                      last_balance: 0,
                      rows_balance: 0
                    });
                  }
                }
              });
            }
          });
        }
      });
    }
  });
});
router.get('/voucher_list', function (req, res) {
  if (req.decoded.data.type === 2) {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let offset = 0,
            limit = 10,
            search = "";

        if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
          limit = req.query.limit;
        }

        if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
          offset = (parseInt(req.query.page) - 1) * limit;
        }

        if (req.query.search) {
          search = req.query.search;
        }

        connection.query(`SELECT SUM(debit) - SUM(credit) as tot_balance
          FROM transactions_comp
          WHERE type=2`, function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let tot_balance = result[0].tot_balance;
            connection.query(`SELECT COUNT(*) as tot_rows 
                FROM transactions_comp
                WHERE type=2
                ${search !== '' ? 'AND (id LIKE ? OR remarks LIKE ? OR debit LIKE ? OR credit LIKE ? OR created_at LIKE ?)' : ''}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, result) {
              if (error) {
                connection.release();
                res.status(500).json({
                  error
                });
              } else {
                let tot_rows = result[0].tot_rows;
                connection.query(`SELECT * 
                      FROM transactions_comp
                      WHERE type=2
                      ${search !== '' ? 'AND (id LIKE ? OR remarks LIKE ? OR debit LIKE ? OR credit LIKE ? OR created_at LIKE ?)' : ''}
                      ORDER BY id DESC
                      LIMIT ${limit}
                      OFFSET ${offset}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results) {
                  connection.release();

                  if (error) {
                    res.status(500).json({
                      error
                    });
                  } else {
                    res.json({
                      data: results,
                      tot_rows,
                      tot_balance
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Not Permission Yet!'
    });
  }
});
router.post('/add_voucher', function (req, res) {
  if (req.decoded.data.type === 2) {
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        db_utils.connectTrans(connection, function (resolve, err_cb) {
          // this is in query promise handler
          connection.query(`INSERT INTO \`transactions_comp\` SET ?`, {
            remarks: req.body.remarks,
            debit: parseInt(req.body.debit),
            credit: parseInt(req.body.credit),
            type: 2
          }, function (error, results) {
            if (error) {
              err_cb(error);
              resolve();
            } else {
              connection.query(`SELECT wallet FROM company_var WHERE id=1`, function (error, results) {
                if (error) {
                  err_cb(error);
                  resolve();
                } else {
                  let company_params = {
                    wallet: parseInt(results[0].wallet) + (parseInt(req.body.debit) - parseInt(req.body.credit))
                  };
                  connection.query('UPDATE company_var SET ? WHERE id=1', company_params, function (error, results) {
                    if (error) {
                      err_cb(error);
                    }

                    resolve();
                  });
                }
              });
            }
          });
        }, function (error) {
          // this is finalize response handler
          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              status: true
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Not Permission Yet!'
    });
  }
});
router.post('/user_id_check', function (req, res) {
  if (req.decoded.data.type > 0 && /^[0-9]*$/.test(req.body.recv_user_ans_id)) {
    let recv_u_asn_id = req.body.recv_user_ans_id;
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.query('SELECT count(*) as count, full_name FROM members WHERE user_asn_id=?', recv_u_asn_id, function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              data: results[0]
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
router.post("/transfer_funds", function (req, res) {
  if (req.decoded.data.type > 0 && /^[0-9]*$/.test(req.body.user_asn_id) && /^[0-9]*$/.test(req.body.amount)) {
    let recv_user_asn_id = req.body.user_asn_id;
    let amount = parseInt(req.body.amount);

    if (amount >= 100) {
      db.getConnection(function (error, connection) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          connection.beginTransaction(async function (err) {
            if (err) {
              connection.release();
              res.status(500).json({
                err
              });
            } else {
              let throw_error = null;
              await new Promise(resolve => {
                connection.query(`SELECT wallet FROM company_var WHERE id=1`, function (error, results, fields) {
                  if (error) {
                    throw_error = error;
                    return resolve();
                  } else {
                    let company_params = {
                      wallet: parseInt(results[0].wallet) - amount
                    };
                    connection.query('UPDATE company_var SET ? WHERE id=1', company_params, function (error, results, fields) {
                      if (error) {
                        throw_error = error;
                        return resolve();
                      } else {
                        connection.query(`SELECT ivm.wallet, ivm.member_id
                            FROM members as m
                            JOIN info_var_m as ivm
                            ON m.id=ivm.member_id
                            WHERE m.user_asn_id=?`, recv_user_asn_id, function (error, results, fields) {
                          if (error) {
                            throw_error = error;
                            return resolve();
                          } else {
                            let recv_id = results[0].member_id;
                            let recv_params = {
                              wallet: parseInt(results[0].wallet) + amount
                            };
                            connection.query(`UPDATE info_var_m SET ? WHERE member_id=?`, [recv_params, recv_id], function (error, results, fields) {
                              if (error) {
                                throw_error = error;
                                return resolve();
                              } else {
                                connection.query('INSERT INTO `transactions_comp` SET ?', {
                                  remarks: "Funds Transfered To User ID " + recv_user_asn_id,
                                  credit: amount
                                }, function (error, results, fields) {
                                  if (error) {
                                    throw_error = error;
                                    return resolve();
                                  } else {
                                    connection.query(`INSERT INTO \`notifications\` SET ?`, {
                                      from_type: 1,
                                      to_type: 1,
                                      from_id: 1,
                                      to_id: 1,
                                      message: "Successfully Funds Transfered To User ID " + recv_user_asn_id,
                                      notify_type: 0
                                    }, function (error, results, fields) {
                                      if (error) {
                                        throw_error = error;
                                        return resolve();
                                      } else {
                                        connection.query(`INSERT INTO \`transactions_m\` SET ?`, {
                                          member_id: recv_id,
                                          remarks: "Funds Received From Admin",
                                          debit: amount
                                        }, function (error, results, fields) {
                                          if (error) {
                                            throw_error = error;
                                            return resolve();
                                          } else {
                                            connection.query(`INSERT INTO \`notifications\` SET ?`, {
                                              from_type: 1,
                                              to_type: 0,
                                              from_id: 1,
                                              to_id: recv_id,
                                              from_txt: 'Admin',
                                              message: "Funds Received From Admin",
                                              notify_type: 0
                                            }, function (error, results, fields) {
                                              if (error) {
                                                throw_error = error;
                                              }

                                              return resolve();
                                            });
                                          }
                                        });
                                      }
                                    });
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                });
              });

              if (throw_error) {
                return connection.rollback(function () {
                  connection.release();
                  res.status(500).json({
                    throw_error
                  });
                });
              } else {
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      connection.release();
                      res.status(500).json({
                        err
                      });
                    });
                  } else {
                    connection.release();
                    res.json({
                      status: true
                    });
                  }
                });
              }
            }
          });
        }
      });
    } else {
      res.json({
        status: false,
        message: 'Minimum transfer funds send 100.'
      });
    }
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
module.exports = router;

/***/ }),

/***/ "./server/apis/assign-role-fr.js":
/*!***************************************!*\
  !*** ./server/apis/assign-role-fr.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 0) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/list/:branch_id', function (req, res) {
  if (!/^[0-9]*$/.test(req.params.branch_id)) {
    return res.status(500).json({
      error: "Invalid Branch ID!"
    });
  }

  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`SELECT COUNT(*) as tot_rows 
        from assign_role_fr as asn_fr
        left join members as m
        on asn_fr.member_id = m.id
        join (
          select 
            fr.*,
            get_franchise_rd_code(fr.id) as fr_code
          from franchises as fr
        ) as fr
        on asn_fr.fr_id = fr.id
        join (
          select 
            crzb_l.id,
            get_crzb_with_p_name(crzb_l.id) as name
          from crzb_list as crzb_l
        ) as crzb_var
        on fr.branch_id = crzb_var.id
        
        where fr.branch_id=${req.params.branch_id} and (
          asn_fr.id like '%${search}%' or
          m.user_asn_id like '%${search}%' or
          m.full_name like '%${search}%' or
          fr.name like '%${search}%' or
          fr.fr_code collate utf8mb4_general_ci like '%${search}%' or
          crzb_var.name collate utf8mb4_general_ci like '%${search}%'
        )`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = result[0].tot_rows;
          connection.query(`select 
                  asn_fr.id,
                  asn_fr.role_status,
                  m.user_asn_id,
                  m.full_name,
                  fr.fr_code,
                  fr.id as fr_id,
                  fr.name as fr_name,
                  crzb_var.id as br_id,
                  crzb_var.name as br_name
                from assign_role_fr as asn_fr
                left join members as m
                on asn_fr.member_id = m.id
                join (
                  select 
                    fr.*,
                    get_franchise_rd_code(fr.id) as fr_code
                  from franchises as fr
                ) as fr
                on asn_fr.fr_id = fr.id
                join (
                  select 
                    crzb_l.id,
                    get_crzb_with_p_name(crzb_l.id) as name
                  from crzb_list as crzb_l
                ) as crzb_var
                on fr.branch_id = crzb_var.id
                
                where fr.branch_id=${req.params.branch_id} and (
                  asn_fr.id like '%${search}%' or
                  m.user_asn_id like '%${search}%' or
                  m.full_name like '%${search}%' or
                  fr.name like '%${search}%' or
                  fr.fr_code collate utf8mb4_general_ci like '%${search}%' or
                  crzb_var.name collate utf8mb4_general_ci like '%${search}%'
                )
                
                ORDER BY asn_fr.id DESC
                LIMIT ${limit}
                OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/get-fr-list/:branch_id', (req, res) => {
  if (req.params.branch_id) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`select 
              fr.id,
              fr.name
            from franchises as fr
            
            where fr.branch_id=${req.params.branch_id} and fr.active=1`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              result
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.post('/get-user-check', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('SELECT id, full_name FROM `members` where (binary `email`=? OR binary `user_asn_id`=?) AND is_paid_m=1', [req.body.email, req.body.email], function (error, results, fields) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (results.length > 0) {
            let user_data = results[0];
            connection.query(`SELECT COUNT(*) as count FROM assign_roles WHERE member_id=${user_data.id} and role_status=1`, function (error, result) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                if (result[0].count > 0) {
                  res.json({
                    status: false,
                    message: "This user is already assigned parent!"
                  });
                } else {
                  res.json({
                    result: user_data
                  });
                }
              }
            });
          } else {
            connection.release();
            res.json({
              status: false,
              message: "Invalid User!"
            });
          }
        }
      });
    }
  });
});
router.post('/assign', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      db_util.connectTrans(connection, async function (resolve, err_hdl) {
        let frn_ids = req.body.fr_ids,
            thr_err = null;

        for (fr_id of frn_ids) {
          // check franchise id already assign this member id but role status 0
          let skip = false;
          await new Promise(inn_res => {
            connection.query(`update assign_role_fr set role_status=0 where fr_id=${fr_id} and member_id<>${req.body.mem_id} and role_status=1`, function (error) {
              if (error) {
                thr_err = error;
                inn_res();
              } else {
                connection.query(`select member_id, role_status from assign_role_fr where fr_id=${fr_id} and member_id=${req.body.mem_id}`, function (error, result) {
                  if (error) {
                    thr_err = error;
                    inn_res();
                  } else {
                    if (!result.length) {
                      inn_res();
                    } else {
                      if (result[0].role_status == 1) {
                        skip = true;
                        inn_res();
                      } else {
                        connection.query(`update assign_role_fr set role_status=1 where fr_id=${fr_id} and member_id=${req.body.mem_id} and role_status=0`, function (error) {
                          if (error) {
                            thr_err = error;
                          }

                          skip = true;
                          inn_res();
                        });
                      }
                    }
                  }
                });
              }
            });
          });
          if (thr_err) break;
          if (skip) continue;
          await new Promise(inn_res => {
            connection.query(`insert into assign_role_fr set ?`, {
              fr_id,
              member_id: req.body.mem_id
            }, function (error) {
              if (error) {
                thr_err = error;
              }

              inn_res();
            });
          });
          if (thr_err) break;
        }

        if (thr_err) {
          err_hdl(err_hdl);
        }

        resolve();
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/toggle-status', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      db_util.connectTrans(connection, async function (resolve, err_hdl) {
        if (req.body.change_sts == true) {
          let throw_err = null;
          await new Promise(in_resolve => {
            connection.query(`SELECT COUNT(*) as tot_rows FROM assign_role_fr WHERE fr_id=${req.body.fr_id} AND role_status=1`, function (error, result) {
              if (error) {
                throw_err = error;
                in_resolve();
              } else {
                if (result[0].tot_rows > 0) {
                  connection.query(`UPDATE assign_role_fr SET ? WHERE fr_id=${req.body.fr_id} AND role_status=1`, {
                    role_status: 0
                  }, function (error, result) {
                    if (error) {
                      throw_err = error;
                    }

                    in_resolve();
                  });
                } else {
                  in_resolve();
                }
              }
            });
          });

          if (throw_err) {
            err_hdl(throw_err);
            resolve();
          }
        }

        connection.query(`UPDATE assign_role_fr SET ? WHERE id=${req.body.row_id}`, {
          role_status: req.body.change_sts
        }, function (error) {
          if (error) {
            err_hdl(error);
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/assign-role-trans-fr.js":
/*!*********************************************!*\
  !*** ./server/apis/assign-role-trans-fr.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment"); // const _ = require('lodash')


const db = __webpack_require__(/*! ../db.js */ "./server/db.js"); // const db_util = require('../func/db-util.js')


router.use(function (req, res, next) {
  if (req.decoded.data.type === 0) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/sale-list', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 9,
          search = "";

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select 
                count(distinct asnr_fr.fr_id) as tot_rows
                from assign_role_fr as asnr_fr
                            
                join franchises as fr
                on asnr_fr.fr_id = fr.id
                
                join (
                    select 
                        crzb_l.id as crzb_id,
                        get_crzb_with_p_name(crzb_l.id) as crzb_name
                    from crzb_list as crzb_l
                ) as crzb_var
                on fr.branch_id = crzb_var.crzb_id
                
                where asnr_fr.member_id =${req.decoded.data.user_id}`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_start_year = date.clone().startOf('year').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_year = date.clone().endOf('year').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select 
                                fr.name,
                                crzb_var.crzb_name,
                                get_fr_mem_sale(asnr_fr.fr_id, asnr_fr.member_id) as total_sale,
                                get_fr_mem_date_sale(asnr_fr.fr_id, asnr_fr.member_id, '${gen_start_month}', '${gen_end_month}') as total_month_sale,
                                get_fr_mem_date_sale(asnr_fr.fr_id, asnr_fr.member_id, '${gen_start_year}', '${gen_end_year}') as total_year_sale
                            from assign_role_fr as asnr_fr
                            
                            join franchises as fr
                            on asnr_fr.fr_id = fr.id
                            
                            join (
                                select 
                                    crzb_l.id as crzb_id,
                                    get_crzb_with_p_name(crzb_l.id) as crzb_name
                                from crzb_list as crzb_l
                            ) as crzb_var
                            on fr.branch_id = crzb_var.crzb_id
                            
                            where asnr_fr.member_id =${req.decoded.data.user_id}
                            group by asnr_fr.fr_id
                            order by total_sale desc, total_year_sale desc, total_month_sale desc
                            LIMIT ${limit}
                            OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/assign-role-trans.js":
/*!******************************************!*\
  !*** ./server/apis/assign-role-trans.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const _ = __webpack_require__(/*! lodash */ "lodash");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 2) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/sale-list', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select count(*) as tot_rows from (
          select 
            asn_mem.user_asn_id as role_asn_mj_id,
            asn_mem.full_name as role_asn_mj_name,
            get_crzb_rd_code(z_list.id) as crzb_code,
            get_crzb_with_p_name(z_list.id) as crzb_name

          from mem_link_crzb as mem_lk_crzb
          join members as m
          on mem_lk_crzb.member_id = m.id and m.is_paid_m=1

          left join crzb_list as b_list
          on mem_lk_crzb.crzb_id = b_list.id
          left join crzb_list as z_list
          on b_list.parent_id = z_list.id

          left join assign_roles_trans as asn_role_tns
          on z_list.id = asn_role_tns.crzb_id and mem_lk_crzb.member_id = asn_role_tns.linked_member_id

          left join members as asn_mem
          on asn_role_tns.member_id = asn_mem.id

          where mem_lk_crzb.linked_mem_type=1
          group by z_list.id, asn_role_tns.member_id
        ) as all_data

        where 
          all_data.role_asn_mj_id like '%${search}%' or
          all_data.role_asn_mj_name like '%${search}%' or
          all_data.crzb_code like '%${search}%' or 
          all_data.crzb_name like '%${search}%'`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select * from (
                select 
                  asn_mem.user_asn_id as role_asn_mj_id,
                  asn_mem.full_name as role_asn_mj_name,
                  get_crzb_rd_code(z_list.id) as crzb_code,
                  get_crzb_with_p_name(z_list.id) as crzb_name,
                  count(*) as total_sale,
                  get_zonal_sale_monthly(z_list.id, asn_mem.id, '${gen_start_month}', '${gen_end_month}') as total_month_sale

                from mem_link_crzb as mem_lk_crzb
                join members as m
                on mem_lk_crzb.member_id = m.id and m.is_paid_m=1

                left join crzb_list as b_list
                on mem_lk_crzb.crzb_id = b_list.id
                left join crzb_list as z_list
                on b_list.parent_id = z_list.id

                left join assign_roles_trans as asn_role_tns
                on z_list.id = asn_role_tns.crzb_id and mem_lk_crzb.member_id = asn_role_tns.linked_member_id

                left join members as asn_mem
                on asn_role_tns.member_id = asn_mem.id

                where mem_lk_crzb.linked_mem_type=1
                group by z_list.id, asn_role_tns.member_id
              ) as all_data

              where 
                all_data.role_asn_mj_id like '%${search}%' or
                all_data.role_asn_mj_name like '%${search}%' or
                all_data.crzb_code like '%${search}%' or 
                all_data.crzb_name like '%${search}%'
              
              order by all_data.total_sale desc
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/commission-list', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select count(*) as tot_rows from (
          select 
            m.id,
            m.user_asn_id as mj_id,
            m.full_name as mj_name,
            get_crzb_rd_code(asr_trans.crzb_id) as crzb_code,
            get_crzb_with_p_name(asr_trans.crzb_id) as crzb_name

          from assign_roles_trans as asr_trans
          join members as m
          on asr_trans.member_id = m.id
            
          group by m.id
        ) as all_data
        
        where (
          all_data.mj_id like '%${search}%' or
          all_data.mj_name like '%${search}%' or
          all_data.crzb_code like '%${search}%' or
          all_data.crzb_name like '%${search}%'
        )`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select * from (
                select 
                  m.id,
                  m.user_asn_id as mj_id,
                  m.full_name as mj_name,
                  get_crzb_rd_code(asr_trans.crzb_id) as crzb_code,
                  get_crzb_with_p_name(asr_trans.crzb_id) as crzb_name,
                  sum(asr_trans.amount) as total_comm,
                  get_crzb_mem_comm_monthly(asr_trans.crzb_id, m.id, '${gen_start_month}', '${gen_end_month}') as total_month_comm
                  
                from assign_roles_trans as asr_trans
                join members as m
                on asr_trans.member_id = m.id
                  
                group by m.id
              ) as all_data
              
              where (
                all_data.mj_id like '%${search}%' or
                all_data.mj_name like '%${search}%' or
                all_data.crzb_code like '%${search}%' or
                all_data.crzb_name like '%${search}%'
              )
              
              order by all_data.total_comm desc, all_data.total_month_comm desc
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/sale-count', (req, res) => {
  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let data = {
        total_sale: 0,
        yearly_sale: 0,
        monthly_sale: 0
      };
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query(`SELECT COUNT(*) as sale FROM mem_link_crzb as mem_lk_crzb
          JOIN members as m
          ON mem_lk_crzb.member_id=m.id AND m.is_paid_m=1 AND mem_lk_crzb.linked_mem_type=1`, function (error, result) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            data['total_sale'] = result[0].sale;
            let date = moment();
            let gen_start_year = date.clone().startOf('year').format('YYYY-MM-DD HH:mm:ss'),
                gen_end_year = date.clone().endOf('year').format('YYYY-MM-DD HH:mm:ss');
            connection.query(`SELECT COUNT(*) as sale FROM mem_link_crzb as mem_lk_crzb
                JOIN members as m
                ON mem_lk_crzb.member_id=m.id AND m.is_paid_m=1
                WHERE mem_lk_crzb.linked_at>='${gen_start_year}' AND mem_lk_crzb.linked_at<='${gen_end_year}' AND mem_lk_crzb.linked_mem_type=1`, function (error, result) {
              if (error) {
                err_hdl(error);
                resolve();
              } else {
                data['yearly_sale'] = result[0].sale;
                let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
                    gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
                connection.query(`SELECT COUNT(*) as sale FROM mem_link_crzb as mem_lk_crzb
                      JOIN members as m
                      ON mem_lk_crzb.member_id=m.id AND m.is_paid_m=1
                      WHERE mem_lk_crzb.linked_at>='${gen_start_month}' AND mem_lk_crzb.linked_at<='${gen_end_month}' AND mem_lk_crzb.linked_mem_type=1`, function (error, result) {
                  if (error) {
                    err_hdl(error);
                  } else {
                    data['monthly_sale'] = result[0].sale;
                  }

                  resolve();
                });
              }
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data
          });
        }
      });
    }
  });
});
router.get('/sale-region', (req, res) => {
  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let regions = [];
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query(`SELECT id, name FROM crzb_list WHERE type=1`, function (error, result) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            regions = result;
            connection.query(`SELECT 
                  COUNT(*) as sale,
                  l_r.id,
                  l_r.name
                FROM mem_link_crzb as mem_lk_crzb
                JOIN members as m
                ON mem_lk_crzb.member_id=m.id AND m.is_paid_m=1
                LEFT JOIN crzb_list as l_b
                ON mem_lk_crzb.crzb_id = l_b.id
                LEFT JOIN crzb_list as l_z
                ON l_b.parent_id = l_z.id
                LEFT JOIN crzb_list as l_r
                ON l_z.parent_id = l_r.id
                WHERE mem_lk_crzb.linked_mem_type=1
                group by l_r.id`, function (error, result) {
              if (error) {
                err_hdl(error);
              } else {
                _.each(result, o => {
                  let f_ind = _.findIndex(regions, {
                    id: o.id
                  });

                  if (f_ind > -1) {
                    regions[f_ind]['sale'] = o.sale;
                  }
                });
              }

              resolve();
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            regions
          });
        }
      });
    }
  });
});
router.get('/sale-country', (req, res) => {
  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let countries = [];
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query(`SELECT id as c_id, name as c_name FROM crzb_list WHERE type=0`, function (error, result) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            countries = result;
            connection.query(`SELECT 
                  COUNT(*) as sale,
                  l_c.id as c_id,
                  l_c.name as c_name,
                  l_c.type as type
                FROM mem_link_crzb as mem_lk_crzb
                JOIN members as m
                ON mem_lk_crzb.member_id=m.id AND m.is_paid_m=1

                LEFT JOIN crzb_list as l_b
                ON mem_lk_crzb.crzb_id = l_b.id
                LEFT JOIN crzb_list as l_z
                ON l_b.parent_id = l_z.id
                LEFT JOIN crzb_list as l_r
                ON l_z.parent_id = l_r.id
                LEFT JOIN crzb_list as l_c
                ON l_r.parent_id = l_c.id

                WHERE mem_lk_crzb.linked_mem_type=1
                group by l_c.id`, function (error, result) {
              if (error) {
                err_hdl(error);
              } else {
                _.each(result, o => {
                  let f_ind = _.findIndex(countries, {
                    c_id: o.c_id
                  });

                  if (f_ind > -1) {
                    countries[f_ind]['sale'] = o.sale;
                  }
                });
              }

              resolve();
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            countries
          });
        }
      });
    }
  });
});
router.get('/commission-count', (req, res) => {
  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let data = {
        total_comm: 0,
        yearly_comm: 0,
        monthly_comm: 0
      };
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query(`SELECT SUM(amount) as comm FROM assign_roles_trans`, function (error, result) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            data['total_comm'] = null_to_0(result[0].comm);
            let date = moment();
            let gen_start_year = date.clone().startOf('year').format('YYYY-MM-DD HH:mm:ss'),
                gen_end_year = date.clone().endOf('year').format('YYYY-MM-DD HH:mm:ss');
            connection.query(`SELECT SUM(amount) as comm FROM assign_roles_trans
                WHERE created_at>='${gen_start_year}' AND created_at<='${gen_end_year}'`, function (error, result) {
              if (error) {
                err_hdl(error);
                resolve();
              } else {
                data['yearly_comm'] = null_to_0(result[0].comm);
                let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
                    gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
                connection.query(`SELECT SUM(amount) as comm FROM assign_roles_trans
                      WHERE created_at>='${gen_start_month}' AND created_at<='${gen_end_month}'`, function (error, result) {
                  if (error) {
                    err_hdl(error);
                  } else {
                    data['monthly_comm'] = null_to_0(result[0].comm);
                  }

                  resolve();
                });
              }
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data
          });
        }
      });
    }
  });
});
router.get('/commission-region', (req, res) => {
  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let regions = [];
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query(`select 
            r_l.id as r_id,
            r_l.name as r_name,
            (select sum(amount) from assign_roles_trans where crzb_id=r_l.id) as r_comm,
            sum(zone.z_comm) as z_comm
            
          from crzb_list as r_l 
          join 
          (
            select 
              z_l.parent_id,
              (select sum(amount) from assign_roles_trans where crzb_id=z_l.id) as z_comm
            from crzb_list as z_l
            group by z_l.id
          ) as zone
          on r_l.id = zone.parent_id
          where r_l.type=1
          group by r_l.id
          order by r_l.id`, function (error, result) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            _.each(result, o => {
              let p_item = {};
              p_item['r_id'] = o.r_id;
              p_item['name'] = o.r_name;
              p_item['comm'] = null_to_0(o.r_comm) + null_to_0(o.z_comm);
              regions.push(p_item);
            });
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            regions
          });
        }
      });
    }
  });
});
router.get('/commission-country', (req, res) => {
  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let countries = [];
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query(`select 
            c_l.id as c_id,
            c_l.name as c_name,
            c_l.type,
            (select sum(amount) from assign_roles_trans where crzb_id=c_l.id) as c_comm,
            sum(region.r_comm) as r_comm,
            sum(region.z_comm) as z_comm
          from crzb_list as c_l
          join (
            select 
              r_l.parent_id,
              (select sum(amount) from assign_roles_trans where crzb_id=r_l.id) as r_comm,
              sum(zone.z_comm) as z_comm
            from crzb_list as r_l 
            join 
              (
              select 
                z_l.parent_id,
                (select sum(amount) from assign_roles_trans where crzb_id=z_l.id) as z_comm
              from crzb_list as z_l
              group by z_l.id
              ) as zone
            on r_l.id = zone.parent_id
            where r_l.type=1
            group by r_l.id
          ) as region
          on c_l.id = region.parent_id
          where c_l.type=0
          group by c_l.id
          order by c_l.id`, function (error, result) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            _.each(result, o => {
              let p_item = {};
              p_item['c_id'] = o.c_id;
              p_item['c_name'] = o.c_name;
              p_item['type'] = o.type;
              p_item['comm'] = null_to_0(o.c_comm) + null_to_0(o.r_comm) + null_to_0(o.z_comm);
              countries.push(p_item);
            });
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            countries
          });
        }
      });
    }
  });
});
module.exports = router;

function null_to_0(int) {
  return int ? parseInt(int) : 0;
}

/***/ }),

/***/ "./server/apis/assign-role.js":
/*!************************************!*\
  !*** ./server/apis/assign-role.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 2 || req.decoded.data.type === 0) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/crct-head-info', function (req, res) {
  if (req.decoded.data.type === 2) {
    res.json({
      type: 5,
      hod_mem_id: req.decoded.data.user_id,
      hod_id: 1,
      role_status: 1
    });
  } else {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`select 
              asn_role.member_id, 
              crzb_l.id as crzb_id,
              crzb_l.type as crzb_type,
              asn_role.role_status
            from assign_roles as asn_role 
            join crzb_list as crzb_l
            on asn_role.crzb_id = crzb_l.id
            where asn_role.member_id=${req.decoded.data.user_id}`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (result.length > 0) {
              res.json({
                type: result[0].crzb_type,
                hod_mem_id: result[0].member_id,
                hod_id: result[0].crzb_id,
                role_status: result[0].role_status
              });
            } else {
              res.json({
                type: null,
                hod_mem_id: null,
                hod_id: null,
                role_status: 0
              });
            }
          }
        });
      }
    });
  }
});
router.use(function (req, res, next) {
  if (req.decoded.data.type === 2) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/list', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select
          COUNT(*) as tot_rows
        from (
          select 
            asn_role.*,
            crzb_l.*,
            m.user_asn_id as mj_id,
            m.full_name as mj_name
          from assign_roles as asn_role
          join (
          select 
            ls1.id as crzb_l_id, ls1.name, ls1.type, concat(if(ls3.rd_id is null, "", concat(ls3.rd_id, "-")), if(ls2.rd_id is null, "", concat(ls2.rd_id, "-")), ls1.rd_id) as code
          from crzb_list as ls1
              left join crzb_list as ls2
              on ls1.parent_id = ls2.id
              left join crzb_list as ls3
              on ls2.parent_id = ls3.id
          ) as crzb_l
          on asn_role.crzb_id = crzb_l.crzb_l_id
          join members as m
          on asn_role.member_id = m.id
        ) as tbl
        where tbl.name like '%${search}%' or tbl.code like '%${search}%' or tbl.mj_id like '%${search}%' or tbl.mj_name like '%${search}%'`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = result[0].tot_rows;
          connection.query(`select
                tbl.id,
                tbl.crzb_id,
                tbl.role_status,
                tbl.name,
                tbl.type,
                tbl.code,
                tbl.mj_id,
                tbl.mj_name
                from (
                select 
                  asn_role.*,
                  crzb_l.*,
                  m.user_asn_id as mj_id,
                  m.full_name as mj_name
                from assign_roles as asn_role
                join (
                  select 
                    ls1.id as crzb_l_id, 
                        ls1.name, 
                        ls1.type, 
                        concat(if(ls3.rd_id is null, "", concat(ls3.rd_id, "-")), if(ls2.rd_id is null, "", concat(ls2.rd_id, "-")), ls1.rd_id) as code
                  from crzb_list as ls1
                  left join crzb_list as ls2
                  on ls1.parent_id = ls2.id
                  left join crzb_list as ls3
                  on ls2.parent_id = ls3.id
                ) as crzb_l
                on asn_role.crzb_id = crzb_l.crzb_l_id
                join members as m
                on asn_role.member_id = m.id
              ) as tbl
              where tbl.name like '%${search}%' or tbl.code like '%${search}%' or tbl.mj_id like '%${search}%' or tbl.mj_name like '%${search}%'
              ORDER BY id DESC
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/exist-check/:crz_id', (req, res) => {
  if (req.params.crz_id) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let query = `SELECT COUNT(*) as count FROM assign_roles where crzb_id=${req.params.crz_id} AND role_status=1`;
        connection.query(query, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (result[0].count > 0) {
              res.json({
                count: result[0].count
              });
            } else {
              res.json({
                count: 0
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.post('/get-user-check', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('SELECT id, full_name FROM `members` where (binary `email`=? OR binary `user_asn_id`=?) AND is_paid_m=1', [req.body.email, req.body.email], function (error, results, fields) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (results.length > 0) {
            let user_data = results[0];
            connection.query(`SELECT COUNT(*) as count FROM assign_roles WHERE member_id=${user_data.id}`, function (error, result) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                if (result[0].count > 0) {
                  res.json({
                    status: false,
                    message: "This user is already assigned role!"
                  });
                } else {
                  res.json({
                    result: user_data
                  });
                }
              }
            });
          } else {
            connection.release();
            res.json({
              status: false,
              message: "Invalid User!"
            });
          }
        }
      });
    }
  });
});
router.post('/assign', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('INSERT INTO `assign_roles` SET ?', {
        member_id: req.body.mem_id,
        crzb_id: req.body.sel_crz_id
      }, function (error, results) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/update-row', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      db_util.connectTrans(connection, async function (resolve, err_hdl) {
        let throw_err = null;
        await new Promise(in_resolve => {
          connection.query(`SELECT COUNT(*) as tot_rows FROM assign_roles WHERE crzb_id=${req.body.crz_id} AND role_status=1`, function (error, result) {
            if (error) {
              throw_err = error;
              in_resolve();
            } else {
              if (result[0].tot_rows > 0) {
                connection.query(`UPDATE assign_roles SET ? WHERE crzb_id=${req.body.crz_id} AND role_status=1`, {
                  role_status: 0
                }, function (error, result) {
                  if (error) {
                    throw_err = error;
                  }

                  in_resolve();
                });
              } else {
                in_resolve();
              }
            }
          });
        });

        if (throw_err) {
          err_hdl(throw_err);
          resolve();
        }

        connection.query(`UPDATE assign_roles SET ? WHERE id=${req.body.updated_id}`, {
          crzb_id: req.body.crz_id,
          role_status: 1
        }, function (error, results) {
          if (error) {
            err_hdl(error);
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/toggle-status', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      db_util.connectTrans(connection, async function (resolve, err_hdl) {
        if (req.body.change_sts == true) {
          let throw_err = null;
          await new Promise(in_resolve => {
            connection.query(`SELECT COUNT(*) as tot_rows FROM assign_roles WHERE crzb_id=${req.body.crz_id} AND role_status=1`, function (error, result) {
              if (error) {
                throw_err = error;
                in_resolve();
              } else {
                if (result[0].tot_rows > 0) {
                  connection.query(`UPDATE assign_roles SET ? WHERE crzb_id=${req.body.crz_id} AND role_status=1`, {
                    role_status: 0
                  }, function (error, result) {
                    if (error) {
                      throw_err = error;
                    }

                    in_resolve();
                  });
                } else {
                  in_resolve();
                }
              }
            });
          });

          if (throw_err) {
            err_hdl(throw_err);
            resolve();
          }
        }

        connection.query(`UPDATE assign_roles SET ? WHERE id=${req.body.row_id}`, {
          role_status: req.body.change_sts
        }, function (error) {
          if (error) {
            err_hdl(error);
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/bank-details.js":
/*!*************************************!*\
  !*** ./server/apis/bank-details.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const _ = __webpack_require__(/*! lodash */ "lodash");

const shortId = __webpack_require__(/*! shortid */ "shortid");

const jwt = __webpack_require__(/*! jsonwebtoken */ "jsonwebtoken");

const config = __webpack_require__(/*! ../config */ "./server/config.js");

const trans_email = __webpack_require__(/*! ../e-conf.js */ "./server/e-conf.js");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get("/:id", function (req, res, next) {
  if (/^[0-9]*$/.test(req.params.id)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error
        });
      } else {
        connection.query('SELECT account_number, account_title, address, bank_name, branch_code, iban_number FROM `user_bank_details` WHERE member_id=?', req.params.id, function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              data: results.length > 0 ? results[0] : {}
            });
          }
        });
      }
    });
  } else {
    next();
  }
});
router.post("/update", function (req, res) {
  if (req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error
        });
      } else {
        connection.beginTransaction(async function (err) {
          if (err) {
            connection.release();
            res.status(500).json({
              err
            });
          } else {
            let throw_error = null;
            await new Promise(resolve => {
              connection.query(`SELECT u_bk.id, m.email 
                FROM \`members\` as m
                LEFT JOIN \`user_bank_details\` as u_bk
                ON m.id=u_bk.member_id
                WHERE m.id=?`, req.decoded.data.user_id, function (error, results, fields) {
                if (error) {
                  throw_error = error;
                  return resolve();
                } else {
                  let bk_query = '';
                  let params = [];

                  let save_data = _.cloneDeep(req.body.data);

                  delete save_data['secure'];

                  if (results.length > 0 && results[0].id !== null) {
                    bk_query = 'UPDATE user_bank_details SET ? WHERE member_id=?';
                    params = [save_data, req.decoded.data.user_id];
                  } else {
                    bk_query = 'INSERT INTO user_bank_details SET ?';
                    save_data['member_id'] = req.decoded.data.user_id;
                    params = [save_data];
                  }

                  if (req.body.data['secure'] === true) {
                    let send_email = results[0].email;
                    let token = jwt.sign({
                      data: {
                        user_id: req.decoded.data.user_id,
                        u_id: shortId.generate(),
                        type: 5
                      }
                    }, config.secret, {
                      expiresIn: "1 day"
                    });
                    connection.query(`INSERT INTO tokens SET ?`, {
                      type: 5,
                      member_id: req.decoded.data.user_id,
                      token: token,
                      token_data: JSON.stringify(save_data)
                    }, function (error, results) {
                      if (error) {
                        throw_error = error;
                        return resolve();
                      } else {
                        res.render("verify-token", {
                          host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                          name: "Member",
                          token: token
                        }, function (errPug, html) {
                          if (errPug) {
                            throw_error = errPug;
                            return resolve();
                          } else {
                            trans_email.sendMail({
                              from: '"MJ Supreme" <info@mj-supreme.com>',
                              to: send_email,
                              subject: 'Verification Token',
                              html: html
                            }, function (err, info) {
                              if (err) {
                                throw_error = err;
                              }

                              return resolve();
                            });
                          }
                        });
                      }
                    });
                  } else {
                    connection.query(bk_query, params, function (error, results, fields) {
                      if (error) {
                        throw_error = error;
                        return resolve();
                      } else {
                        return resolve();
                      }
                    });
                  }
                }
              });
            });

            if (throw_error) {
              return connection.rollback(function () {
                connection.release();
                res.status(500).json({
                  throw_error
                });
              });
            } else {
              connection.commit(function (err) {
                if (err) {
                  return connection.rollback(function () {
                    connection.release();
                    res.status(500).json({
                      err
                    });
                  });
                } else {
                  connection.release();
                  res.json({
                    status: true
                  });
                }
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid User!"
    });
  }
});
module.exports = router;

/***/ }),

/***/ "./server/apis/c_subsidiary.js":
/*!*************************************!*\
  !*** ./server/apis/c_subsidiary.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 2) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get("/subs_name/:id", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query(`SELECT name
        FROM c_subsidiary
        WHERE id=${req.params.id}`, function (error, result, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          let name = '';

          if (result.length > 0) {
            name = result[0].name;
          }

          res.json({
            name
          });
        }
      });
    }
  });
});
router.get("/list_controllers", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query(`SELECT c_s.id, c_s.code, c_s.name
        FROM c_subsidiary as c_s
        WHERE c_s.type=0`, function (error, result, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            result
          });
        }
      });
    }
  });
});
router.get("/list", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query(`SELECT c_s.id, c_s.name, c_s2.name as p_name, c_s2.code as p_code
        FROM c_subsidiary as c_s
        LEFT JOIN c_subsidiary as c_s2
        ON c_s.parent_code = c_s2.code
        WHERE c_s.type=1
        ORDER BY c_s.id`, function (error, result, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            result
          });
        }
      });
    }
  });
});
router.get("/subs/:search", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query(`SELECT id, name
        FROM c_subsidiary
        WHERE type=1 AND name LIKE '%${req.params.search}%'
        ORDER BY id LIMIT 10`, function (error, result, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            result
          });
        }
      });
    }
  });
});
router.post("/add", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`SELECT code 
          FROM c_subsidiary 
          WHERE parent_code='${req.body.sel_control}' 
          ORDER BY code DESC LIMIT 1`, function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            let new_inc = result.length > 0 ? (parseInt(result[0].code) + 1).toString() : '1';
            new_inc = new_inc.length < 4 ? ("0000" + new_inc).substr(-4, 4) : new_inc;
            connection.query(`INSERT INTO c_subsidiary SET ?`, {
              code: new_inc,
              name: req.body.subs_name,
              type: 1,
              parent_code: req.body.sel_control
            }, function (error, result) {
              if (error) {
                err_cb(error);
              }

              resolve();
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post("/update", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`SELECT * 
          FROM c_subsidiary 
          WHERE id='${req.body.update_id}'`, async function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            let p_code = result[0].parent_code;
            let code = result[0].code;

            if (p_code !== req.body.sel_control) {
              let thr_err = null;
              await new Promise(resolve2 => {
                connection.query(`SELECT code 
                    FROM c_subsidiary 
                    WHERE parent_code='${req.body.sel_control}' 
                    ORDER BY code DESC LIMIT 1`, function (error, result) {
                  if (error) {
                    thr_err = error;
                    return resolve2();
                  } else {
                    let new_inc = result.length > 0 ? (parseInt(result[0].code) + 1).toString() : '1';
                    code = new_inc.length < 4 ? ("0000" + new_inc).substr(-4, 4) : new_inc;
                    return resolve2();
                  }
                });
              });

              if (thr_err) {
                err_cb(thr_err);
                return resolve();
              }
            }

            connection.query(`UPDATE c_subsidiary SET ? WHERE id=?`, [{
              code: code,
              name: req.body.subs_name,
              type: 1,
              parent_code: req.body.sel_control
            }, req.body.update_id], function (error, result) {
              if (error) {
                err_cb(error);
              }

              resolve();
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/delete', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query(`DELETE FROM c_subsidiary WHERE id=?`, req.body.del_id, function (error, result) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/campaign.js":
/*!*********************************!*\
  !*** ./server/apis/campaign.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const {
  DateTime
} = __webpack_require__(/*! luxon */ "luxon");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.get('/member-tot-ref', (req, res) => {
  if (req.decoded.data.type === 0) {
    db.getConnection(async function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.query(`select total_ref
          from mem_in_campaign
          where
          member_id=${req.decoded.data.user_id} and campaign_id=1`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              total: result.length ? result[0].total_ref : 0
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid User Type!"
    });
  }
});
router.get('/list-team-ref', (req, res) => {
  if (req.decoded.data.type === 0) {
    db.getConnection(async function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        let offset = 0,
            limit = 9,
            search = "";

        if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
          offset = (parseInt(req.query.page) - 1) * limit;
        }

        if (req.query.search) {
          search = req.query.search;
        }

        connection.query(`select 
          count(*) as tot_rows
          from mem_in_campaign as mem_camp
                
          join members as m
          on mem_camp.member_id = m.id
          
          join members as team_m
          on m.user_asn_id = team_m.ref_user_asn_id
          
          join mem_in_campaign as team_mem_camp
          on team_m.id = team_mem_camp.member_id and team_mem_camp.campaign_id=1
          
          where mem_camp.member_id=${req.decoded.data.user_id} and mem_camp.campaign_id=1 and (
            team_m.user_asn_id like '%${search}%' or
            team_m.full_name like '%${search}%'
          )`, function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            if (result.length < 1) {
              connection.release();
              return res.json({
                data: [],
                tot_rows: 0
              });
            }

            let tot_rows = result[0].tot_rows;
            connection.query(`select team_m.user_asn_id as mj_id, team_m.full_name as mj_name, team_mem_camp.total_ref
      
                from mem_in_campaign as mem_camp
                
                join members as m
                on mem_camp.member_id = m.id
                
                join members as team_m
                on m.user_asn_id = team_m.ref_user_asn_id
                
                join mem_in_campaign as team_mem_camp
                on team_m.id = team_mem_camp.member_id and team_mem_camp.campaign_id=1
                
                where mem_camp.member_id=${req.decoded.data.user_id} and mem_camp.campaign_id=1 and (
                  team_m.user_asn_id like '%${search}%' or
                  team_m.full_name like '%${search}%'
                )
                LIMIT ${limit}
                OFFSET ${offset}`, function (error, result) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                res.json({
                  data: result,
                  tot_rows
                });
              }
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid User Type!"
    });
  }
});
router.use(function (req, res, next) {
  if (req.decoded.data.type === 2) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.post('/add', (req, res) => {
  db.getConnection(async function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let str_err = null;
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        req.body['start_date'] = moment(req.body['start_date']).format('YYYY-MM-DD HH:mm:ss');
        req.body['end_date'] = moment(req.body['end_date']).format('YYYY-MM-DD HH:mm:ss');
        let curr_date = DateTime.local().setZone("UTC+5").toFormat("yyyy-LL-dd HH:mm:ss");
        connection.query(`SELECT count(*) as \`rows\` FROM campaigns WHERE start_date<='${curr_date}' AND end_date>='${curr_date}'`, function (error, result) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            if (result[0].rows > 0) {
              str_err = "Campaign already in Proccess.";
              resolve();
            } else {
              connection.query(`INSERT INTO campaigns SET ?`, req.body, function (error) {
                if (error) {
                  err_hdl(error);
                }

                resolve();
              });
            }
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          if (str_err) {
            res.json({
              status: false,
              message: str_err
            });
          } else {
            res.json({
              status: true
            });
          }
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/commissions.js":
/*!************************************!*\
  !*** ./server/apis/commissions.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get('/monthly_data', function (req, res) {
  let grab_months = {
    1: {},
    2: {},
    3: {},
    4: {},
    5: {},
    6: {},
    7: {},
    8: {},
    9: {},
    10: {},
    11: {},
    12: {}
  };
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        error: err
      });
    } else {
      let throw_error = null;
      let date = moment().set('M', 0).set('Y', moment().get('Y') - 1).endOf('Y');

      for (month in grab_months) {
        await new Promise(resolve => {
          date.add(1, 'month');
          let start = date.clone().startOf('month').format("YYYY-MM-DD HH-mm-ss");
          let end = date.clone().endOf('month').format("YYYY-MM-DD HH-mm-ss");
          connection.query('SELECT SUM(amount) as total FROM commissions WHERE status = 1 AND created_at >= ? AND created_at <= ?', [start, end], function (error, results) {
            if (error) {
              throw_error = error;
              resolve();
            } else {
              grab_months[month]['paid'] = results[0].total !== null ? results[0].total : 0;
              connection.query('SELECT SUM(amount) as total FROM commissions WHERE status = 0 AND created_at >= ? AND created_at <= ?', [start, end], function (error, results) {
                if (error) {
                  throw_error = error;
                  resolve();
                } else {
                  grab_months[month]['un_paid'] = results[0].total !== null ? results[0].total : 0;
                  resolve();
                }
              });
            }
          });
        });

        if (throw_error) {
          break;
        }
      }

      connection.release();

      if (throw_error) {
        res.status(500).json({
          error: throw_error
        });
      } else {
        res.json({
          data: grab_months
        });
      }
    }
  });
});
router.get("/un_paid", function (req, res) {
  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`SELECT COUNT(*) as tot_rows
                FROM commissions
                WHERE status=0 
                ${search !== '' ? 'AND (trans_id LIKE ? OR remarks LIKE ? OR amount LIKE ? OR created_at LIKE ?)' : ''}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = results[0].tot_rows;
          connection.query(`SELECT 
                                cm.trans_id as id, 
                                cm.member_id, 
                                cm.remarks as description, 
                                cm.amount,
                                cm.created_at as date
                            FROM commissions as cm
                            WHERE cm.status=0 
                            ${search !== '' ? 'AND (cm.trans_id LIKE ? OR cm.remarks LIKE ? OR cm.amount LIKE ? OR cm.created_at LIKE ?)' : ''}
                            ORDER BY cm.trans_id DESC
                            LIMIT ${limit}
                            OFFSET ${offset}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get("/paid", function (req, res) {
  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`SELECT COUNT(*) as tot_rows
                FROM commissions
                WHERE status=1 
                ${search !== '' ? 'AND (trans_id LIKE ? OR remarks LIKE ? OR amount LIKE ? OR created_at LIKE ?)' : ''}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = result[0].tot_rows;
          connection.query(`SELECT 
                                cm.trans_id as id, 
                                cm.member_id, 
                                cm.remarks as description, 
                                cm.amount, 
                                COUNT(u_rcp.id) as tot_rcp_up,
                                cm.created_at as date
                            FROM commissions as cm
                            LEFT JOIN user_receipts as u_rcp
                            ON u_rcp.ref_id = cm.trans_id AND u_rcp.type = 1 
                            WHERE cm.status=1 
                            ${search !== '' ? 'AND (cm.trans_id LIKE ? OR cm.remarks LIKE ? OR cm.amount LIKE ? OR cm.created_at LIKE ?)' : ''}
                            GROUP BY cm.trans_id
                            ORDER BY cm.trans_id DESC
                            LIMIT ${limit}
                            OFFSET ${offset}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.post('/set_sts', function (req, res) {
  if (/^[0-9]*$/.test(req.body.id) && /^[1|2]$/i.test(req.body.type)) {
    let id = req.body.id;
    let type = req.body.type;
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.beginTransaction(async function (err) {
          if (err) {
            connection.release();
            res.status(500).json({
              err
            });
          } else {
            let throw_error = null;
            await new Promise(resolve => {
              connection.query('UPDATE commissions SET ? WHERE trans_id=?', [{
                status: type
              }, id], function (error, results) {
                if (error) {
                  throw_error = error;
                  return resolve();
                } else {
                  connection.query(`SELECT cm.member_id, cm.amount, m.user_asn_id
                    FROM commissions as cm
                    INNER JOIN members as m
                    ON cm.member_id = m.id
                    WHERE cm.trans_id=?`, id, function (error, results) {
                    if (error) {
                      throw_error = error;
                      return resolve();
                    } else {
                      let mem_id = results[0].member_id;
                      let mem_amount = parseInt(results[0].amount); // - tax apply and fees

                      let gen_amount = mem_amount;
                      let tax = Math.round(mem_amount * .13),
                          fees = 25;

                      if (type === 1) {
                        gen_amount = mem_amount - (tax + fees);
                      }

                      connection.query('INSERT INTO transactions_comp SET ?', {
                        remarks: "Withdrawal Liability Amount Deduct In Your Wallet User ID " + results[0].user_asn_id,
                        credit: gen_amount
                      }, function (error, results) {
                        if (error) {
                          throw_error = error;
                          return resolve();
                        } else {
                          connection.query('SELECT wallet FROM company_var WHERE id=1', function (error, results, fields) {
                            if (error) {
                              throw_error = error;
                              return resolve();
                            } else {
                              let upd_wallet = parseInt(results[0].wallet) - gen_amount;
                              connection.query('UPDATE company_var SET wallet=? WHERE id=1', upd_wallet, function (error, results, fields) {
                                if (error) {
                                  throw_error = error;
                                  return resolve();
                                } else {
                                  if (type === 2) {
                                    // cancel request -- start
                                    connection.query('INSERT INTO transactions_m SET ?', {
                                      member_id: mem_id,
                                      remarks: "Your Withdraw Request Has Been Canceled!",
                                      debit: mem_amount
                                    }, function (error, results) {
                                      if (error) {
                                        throw_error = error;
                                        return resolve();
                                      } else {
                                        connection.query('SELECT available FROM info_var_m WHERE member_id=?', mem_id, function (error, results) {
                                          if (error) {
                                            throw_error = error;
                                            return resolve();
                                          } else {
                                            let set_available = parseInt(results[0].available) + parseInt(mem_amount);
                                            connection.query('UPDATE info_var_m SET ? WHERE member_id=?', [{
                                              available: set_available
                                            }, mem_id], function (error, results) {
                                              if (error) {
                                                throw_error = error;
                                                return resolve();
                                              } else {
                                                connection.query('INSERT INTO notifications SET ?', {
                                                  from_type: 1,
                                                  to_type: 0,
                                                  from_id: 1,
                                                  to_id: mem_id,
                                                  from_txt: "Admin",
                                                  message: "Your Withdrawal Request Has Been Canceled Transaction ID " + id,
                                                  notify_type: 0
                                                }, function (error, results) {
                                                  if (error) {
                                                    throw_error = error;
                                                  }

                                                  return resolve();
                                                });
                                              }
                                            });
                                          }
                                        });
                                      }
                                    }); // cancel request -- end
                                  } else {
                                    connection.query(`select 
                                          has_avlb_at, 
                                          max_trans_limit 
                                        from info_var_m 
                                        where member_id=?`, mem_id, (error, result) => {
                                      if (error) {
                                        throw_error = error;
                                        return resolve();
                                      } else {
                                        let f_data = result.length ? result[0] : {
                                          has_avlb_at: null,
                                          max_trans_limit: 0
                                        };
                                        f_data['max_trans_limit'] = parseInt(f_data['max_trans_limit']) + 500;
                                        f_data['has_avlb_at'] = !f_data['has_avlb_at'] ? moment().format("YYYY-MM-DD HH-mm-ss") : f_data['has_avlb_at'];
                                        connection.query(`update info_var_m set ? where member_id=?`, [f_data, mem_id], (error, result) => {
                                          if (error) {
                                            throw_error = error;
                                            return resolve();
                                          } else {
                                            connection.query('INSERT INTO notifications SET ?', {
                                              from_type: 1,
                                              to_type: 0,
                                              from_id: 1,
                                              to_id: mem_id,
                                              from_txt: "Admin",
                                              message: `Your Withdrawal Request Has Been Approved Transaction ID ${id}.\nWithdraw Amount: ${mem_amount}\nApply Tax: ${tax}\nApply Fee: ${fees}\nPaid Amount: ${gen_amount}`,
                                              notify_type: 0
                                            }, function (error, results) {
                                              if (error) {
                                                throw_error = error;
                                              }

                                              return resolve();
                                            });
                                          }
                                        });
                                      }
                                    });
                                  }
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            });

            if (throw_error) {
              return connection.rollback(function () {
                connection.release();
                res.status(500).json({
                  throw_error
                });
              });
            } else {
              connection.commit(function (err) {
                if (err) {
                  return connection.rollback(function () {
                    connection.release();
                    res.status(500).json({
                      err
                    });
                  });
                } else {
                  connection.release();
                  res.json({
                    status: true
                  });
                }
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
router.post('/user_id_check', function (req, res) {
  if (/^[0-9]*$/.test(req.body.user_id) && /^[0-9]*$/.test(req.body.recv_user_ans_id)) {
    let user_id = req.body.user_id;
    let recv_u_asn_id = req.body.recv_user_ans_id;
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.query('SELECT count(*) as count, full_name FROM members WHERE user_asn_id=? AND id<>?', [recv_u_asn_id, user_id], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              data: results[0]
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
router.post('/wd_check', (req, res) => {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`select available, max_trans_limit from info_var_m where member_id=${req.decoded.data.user_id}`, (error, result) => {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (result.length) {
              let req_amount = parseInt(req.body.amount);
              let avlb = parseInt(result[0].available);
              let limit_trans = parseInt(result[0].max_trans_limit);

              if (req_amount > limit_trans) {
                return res.json({
                  status: false,
                  message: `Maximum amount withdraw only PKR ${limit_trans}/-`
                });
              } else if (avlb < req_amount) {
                return res.json({
                  status: false,
                  message: `Withdraw request is exceed to available amount PKR ${avlb}/-`
                });
              } else {
                return res.json({
                  status: true
                });
              }
            } else {
              res.status(500).json({
                error: "Nothing Found!"
              });
            }
          }
        });
      }
    });
  } else {
    res.status(500).json({
      error: "Invalid Request!"
    });
  }
});
router.post("/withdraw", function (req, res) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    let id = req.decoded.data.user_id;
    let amount = parseInt(req.body.amount);
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.beginTransaction(async function (err) {
          if (err) {
            connection.release();
            res.status(500).json({
              err
            });
          } else {
            let throw_error = null;
            await new Promise(resolve => {
              connection.query(`SELECT 
                  ivm.available, 
                  m.user_asn_id, 
                  m.email
                FROM info_var_m as ivm
                INNER JOIN members as m
                ON ivm.member_id=m.id
                WHERE ivm.member_id=?`, id, function (error, results, fields) {
                if (error) {
                  throw_error = error;
                  return resolve();
                } else {
                  let mem_avlb = results[0].available;
                  let mem_asn_id = results[0].user_asn_id;
                  let mem_email = results[0].email;
                  let info_param = {
                    available: parseInt(mem_avlb) - amount
                  };
                  connection.query('UPDATE info_var_m SET ? WHERE member_id=?', [info_param, id], function (error, results, fields) {
                    if (error) {
                      throw_error = error;
                      return resolve();
                    } else {
                      connection.query('INSERT INTO `transactions_m` SET ?', {
                        member_id: id,
                        remarks: `Withdraw Available Amount PKR ${amount}/-`,
                        credit: amount
                      }, function (error, results, fields) {
                        if (error) {
                          throw_error = error;
                          return resolve();
                        } else {
                          let trans_id = results.insertId;
                          connection.query('INSERT INTO `notifications` SET ?', {
                            from_type: 1,
                            to_type: 0,
                            from_id: 1,
                            to_id: id,
                            from_txt: "Admin",
                            message: `Deduct available amount Rs.${amount}/-`,
                            notify_type: 0
                          }, function (error, results, fields) {
                            if (error) {
                              throw_error = error;
                              return resolve();
                            } else {
                              connection.query('INSERT INTO `commissions` SET ?', {
                                trans_id: trans_id,
                                member_id: id,
                                remarks: "Withdrawal Request From User ID " + mem_asn_id,
                                amount: amount
                              }, function (error, results, fields) {
                                if (error) {
                                  throw_error = error;
                                  return resolve();
                                } else {
                                  connection.query('INSERT INTO `notifications` SET ?', {
                                    from_type: 0,
                                    to_type: 1,
                                    from_id: id,
                                    to_id: 1,
                                    from_txt: mem_email,
                                    message: `Withdrawal Request From User ID ${mem_asn_id} Amount Rs.${amount}/- Transaction ID ${trans_id}`,
                                    notify_type: 2
                                  }, function (error, results, fields) {
                                    if (error) {
                                      throw_error = error;
                                      return resolve();
                                    } else {
                                      connection.query('INSERT INTO `transactions_comp` SET ?', {
                                        remarks: `Withdrawal Liability Amount Add In Your Wallet From User ID ${mem_asn_id}`,
                                        debit: amount
                                      }, function (error, results, fields) {
                                        if (error) {
                                          throw_error = error;
                                          return resolve();
                                        } else {
                                          connection.query('SELECT wallet FROM company_var WHERE id=1', function (error, results, fields) {
                                            if (error) {
                                              throw_error = error;
                                              return resolve();
                                            } else {
                                              let upd_wallet = parseInt(results[0].wallet) + amount;
                                              connection.query('UPDATE company_var SET wallet=? WHERE id=1', upd_wallet, function (error, results, fields) {
                                                if (error) {
                                                  throw_error = error;
                                                }

                                                return resolve();
                                              });
                                            }
                                          });
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            });

            if (throw_error) {
              return connection.rollback(function () {
                connection.release();
                res.status(500).json({
                  throw_error
                });
              });
            } else {
              connection.commit(function (err) {
                if (err) {
                  return connection.rollback(function () {
                    connection.release();
                    res.status(500).json({
                      err
                    });
                  });
                } else {
                  connection.release();
                  res.json({
                    status: true
                  });
                }
              });
            }
          }
        });
      }
    });
  } else {
    res.status(500).json({
      error: "Invalid Request!"
    });
  }
});
router.post("/transfer_funds", function (req, res) {
  if (/^[0-9]*$/.test(req.body.id) && /^[0-9]*$/.test(req.body.amount)) {
    let id = req.body.id; // let recv_user_asn_id = req.body.user_asn_id

    let amount = parseInt(req.body.amount);

    if (amount >= 100) {
      db.getConnection(function (error, connection) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          connection.beginTransaction(async function (err) {
            if (err) {
              connection.release();
              res.status(500).json({
                err
              });
            } else {
              let throw_error = null;
              await new Promise(resolve => {
                // select sender wallet
                let sender_opt = {
                  sql: `SELECT ivm.wallet, m.user_asn_id, m.email
                        FROM members as m
                        JOIN info_var_m as ivm
                        ON m.id=ivm.member_id
                        WHERE m.id=?`
                };
                connection.query(sender_opt, id, function (error, results, fields) {
                  if (error) {
                    throw_error = error;
                    return resolve();
                  } else {
                    let sender_user_asn_id = results[0].user_asn_id;
                    let sender_email = results[0].email;
                    let sender_params = {
                      wallet: parseInt(results[0].wallet) - amount // sender wallet update

                    };
                    connection.query('UPDATE info_var_m SET ? WHERE member_id=?', [sender_params, id], function (error, results, fields) {
                      if (error) {
                        throw_error = error;
                        return resolve();
                      } else {
                        // select admin wallet
                        connection.query(`select wallet from company_var where id=1`, function (error, results, fields) {
                          if (error) {
                            throw_error = error;
                            return resolve();
                          } else {
                            let adm_wallet = results[0].wallet;
                            let upd_wallet = {
                              wallet: parseInt(adm_wallet) + amount // update admin wallet

                            };
                            connection.query('UPDATE company_var SET ? WHERE id=1', upd_wallet, function (error, results, fields) {
                              if (error) {
                                throw_error = error;
                                return resolve();
                              } else {
                                connection.query('INSERT INTO `transactions_m` SET ?', {
                                  member_id: id,
                                  remarks: "Funds Transfered To Admin",
                                  credit: amount
                                }, function (error, results, fields) {
                                  if (error) {
                                    throw_error = error;
                                    return resolve();
                                  } else {
                                    connection.query('INSERT INTO `notifications` SET ?', {
                                      from_type: 1,
                                      to_type: 0,
                                      from_id: 1,
                                      to_id: id,
                                      from_txt: "Admin",
                                      message: "Successfully Funds Transfered To Admin",
                                      notify_type: 0
                                    }, function (error, results, fields) {
                                      if (error) {
                                        throw_error = error;
                                        return resolve();
                                      } else {
                                        connection.query('INSERT INTO `transactions_comp` SET ?', {
                                          remarks: "Funds Received From User ID " + sender_user_asn_id,
                                          debit: amount
                                        }, function (error, results, fields) {
                                          if (error) {
                                            throw_error = error;
                                            return resolve();
                                          } else {
                                            connection.query('INSERT INTO `notifications` SET ?', {
                                              from_type: 0,
                                              to_type: 1,
                                              from_id: id,
                                              to_id: 1,
                                              from_txt: sender_email,
                                              message: "Funds Received From User ID " + sender_user_asn_id,
                                              notify_type: 0
                                            }, function (error, results, fields) {
                                              if (error) {
                                                throw_error = error;
                                              }

                                              return resolve();
                                            });
                                          }
                                        });
                                      }
                                    });
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                });
              });

              if (throw_error) {
                return connection.rollback(function () {
                  connection.release();
                  res.status(500).json({
                    throw_error
                  });
                });
              } else {
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      connection.release();
                      res.status(500).json({
                        err
                      });
                    });
                  } else {
                    connection.release();
                    res.json({
                      status: true
                    });
                  }
                });
              }
            }
          });
        }
      });
    } else {
      res.json({
        status: false,
        message: 'Minimum transfer funds send 100.'
      });
    }
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
module.exports = router;

/***/ }),

/***/ "./server/apis/company-hierarchy.js":
/*!******************************************!*\
  !*** ./server/apis/company-hierarchy.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const _ = __webpack_require__(/*! lodash */ "lodash");

const config = __webpack_require__(/*! ../config.js */ "./server/config.js");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js"); // const db_util = require('../func/db-util.js')


router.use(function (req, res, next) {
  if (req.decoded.data.type === 2) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/crzb-list/:type/:parent_id', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "",
          type = 0,
          parent_id = 0;

      if (/^0$|^1$|^2$|^3$/.test(req.params.type)) {
        type = req.params.type;
      }

      if (/^[0-9]*$/.test(req.params.parent_id)) {
        parent_id = req.params.parent_id;
      }

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select count(*) as tot_rows from (
          select
            m.user_asn_id,
            m.full_name,
            get_crzb_rd_code(crzb_l.id) as crzb_code,
            get_crzb_with_p_name(crzb_l.id) as crzb_name
            
          from crzb_list as crzb_l
          left join assign_roles as asn_role
          on crzb_l.id = asn_role.crzb_id and asn_role.role_status=1
          left join members as m
          on asn_role.member_id = m.id

          where 
            crzb_l.type=${type} and
            crzb_l.parent_id=${parent_id} and 
            crzb_l.active=1
        ) as all_data
        
        where 
          all_data.user_asn_id like '%${search}%' or
          all_data.full_name like '%${search}%' or
          all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
          all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select * from (
                select
                  m.user_asn_id,
                  m.full_name,
                  crzb_l.id as crzb_id,
                  get_crzb_rd_code(crzb_l.id) as crzb_code,
                  get_crzb_with_p_name(crzb_l.id) as crzb_name,
                  get_crzb_total_sale(crzb_l.id) as total_sale,
                  get_crzb_month_sale(crzb_l.id, '${gen_start_month}', '${gen_end_month}') as monthly_sale,
                  get_crzb_total_comm(crzb_l.id) as total_comm,
                  get_crzb_month_comm(crzb_l.id, '${gen_start_month}', '${gen_end_month}') as monthly_comm
                  
                from crzb_list as crzb_l
                left join assign_roles as asn_role
                on crzb_l.id = asn_role.crzb_id and asn_role.role_status=1
                left join members as m
                on asn_role.member_id = m.id

                where 
                  crzb_l.type=${type} and
                  crzb_l.parent_id=${parent_id} and 
                  crzb_l.active=1
              ) as all_data
              
              where 
                all_data.user_asn_id like '%${search}%' or
                all_data.full_name like '%${search}%' or
                all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
                all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
              
              order by all_data.crzb_id
              
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
module.exports = router;

function null_to_0(int) {
  return int ? parseInt(int) : 0;
}

/***/ }),

/***/ "./server/apis/crzb_list.js":
/*!**********************************!*\
  !*** ./server/apis/crzb_list.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 2) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/branch-load/:id', (req, res) => {
  if (req.params.id) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT ls_b.id, ls_b.name, ls_b.description, ls_b.parent_id,
            (SELECT concat(ls_z.name, ", ", ls_r.name, ", ", ls_c.name) as name 
              FROM crzb_list as ls_z
              join crzb_list as ls_r
              on ls_z.parent_id = ls_r.id
              join crzb_list as ls_c
              on ls_r.parent_id = ls_c.id
            where ls_z.id=ls_b.parent_id) as parent_name
          FROM crzb_list as ls_b where id=${req.params.id};`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (result.length > 0) {
              res.json({
                result: result[0]
              });
            } else {
              res.json({
                status: false,
                result: {}
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get('/branch-list', (req, res) => {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`SELECT COUNT(*) as tot_rows 
          FROM crzb_list as ls_b
          left join crzb_list as ls_z
          on ls_b.parent_id = ls_z.id
          left join crzb_list as ls_r
          on ls_z.parent_id = ls_r.id
          left join crzb_list as ls_c
          on ls_r.parent_id = ls_c.id
          where ls_b.name like '%${search}%' AND ls_b.type=3;`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = result[0].tot_rows;
          connection.query(`SELECT ls_b.id, ls_b.name, ls_b.description, ls_b.active, concat(ls_c.rd_id, "-", ls_r.rd_id, "-", ls_z.rd_id, "-", ls_b.rd_id) as code
                FROM crzb_list as ls_b
                left join crzb_list as ls_z
                on ls_b.parent_id = ls_z.id
                left join crzb_list as ls_r
                on ls_z.parent_id = ls_r.id
                left join crzb_list as ls_c
                on ls_r.parent_id = ls_c.id
                where ls_b.name like '%${search}%' AND ls_b.type=3
                ORDER BY id DESC
                LIMIT ${limit}
                OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/zone-load/:id', (req, res) => {
  if (req.params.id) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT ls1.id, ls1.name, ls1.description, ls1.parent_id,
            (SELECT concat(ls2.name, ", ", ls3.name) as name 
              FROM crzb_list as ls2
              join crzb_list as ls3
              on ls2.parent_id = ls3.id
            where ls2.id=ls1.parent_id) as parent_name
          FROM crzb_list as ls1 where ls1.id=${req.params.id}`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (result.length > 0) {
              res.json({
                result: result[0]
              });
            } else {
              res.json({
                status: false,
                result: {}
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get('/zone-list', (req, res) => {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select COUNT(*) as tot_rows from (
          select 
            ls1.name,
            concat(ls3.rd_id, "-", ls2.rd_id, "-", ls1.rd_id) as code 
          from crzb_list as ls1
          join crzb_list as ls2
          on ls1.parent_id = ls2.id
          join crzb_list as ls3
          on ls2.parent_id = ls3.id
          where ls1.type=2
        ) as tbl1
        where (tbl1.name like '%${search}%' OR tbl1.code like '%${search}%')`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = result[0].tot_rows;
          connection.query(`select * from (
                select 
                  ls1.id, 
                  ls1.name,
                  ls1.description, 
                  ls1.active, 
                  concat(ls3.rd_id, "-", ls2.rd_id, "-", ls1.rd_id) as code 
                from crzb_list as ls1
                join crzb_list as ls2
                on ls1.parent_id = ls2.id
                join crzb_list as ls3
                on ls2.parent_id = ls3.id
                where ls1.type=2
              ) as tbl1
              where (tbl1.name like '%${search}%' OR tbl1.code like '%${search}%')
              ORDER BY tbl1.id DESC
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/ac_search_cr/:search', (req, res) => {
  if (req.params.search) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`select * from (
            select 
              concat(ls1.name, ', ', ls2.name) as name, ls1.id
              from crzb_list as ls1
              join crzb_list as ls2
              on ls1.parent_id = ls2.id
              where ls1.type=1
          ) as tbl1
          where tbl1.name like '%${req.params.search}%'
          limit 10`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              result
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get('/exist-check/:crzb_parent_id/:search', (req, res) => {
  if (req.params.search) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT list.id, list.name FROM crzb_list as list
          WHERE list.name='${req.params.search}' and list.parent_id=${req.params.crzb_parent_id}
          LIMIT 1`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (result.length > 0) {
              res.json({
                count: 1
              });
            } else {
              res.json({
                count: 0
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get('/ac_search_zone/:search', (req, res) => {
  if (req.params.search) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let sel_query = `SELECT CONCAT(l_z.name, ", ", l_r.name, ", ", l_c.name) as name, l_z.id FROM crzb_list as l_z`,
            join_b = ` join crzb_list as l_r
          on l_z.parent_id = l_r.id
          join crzb_list as l_c
          on l_r.parent_id = l_c.id`,
            where_b = ` where (l_z.name like '%${req.params.search}%' OR l_r.name like '%${req.params.search}%' OR l_c.name like '%${req.params.search}%') AND l_z.type=2`,
            limit_b = ` LIMIT 10`;
        connection.query(sel_query + join_b + where_b + limit_b, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              result
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get('/ac_search_list/:role/:search', (req, res) => {
  if (req.params.role && req.params.search) {
    if (/^(0|1|2|3)$/.test(req.params.role)) {
      db.getConnection(function (err, connection) {
        if (err) {
          res.status(500).json({
            err
          });
        } else {
          const role = req.params.role;
          connection.query(`SELECT 
              crzb_var.crzb_name as name, 
              crzb_l.id 
            FROM crzb_list as crzb_l
            join (
              select 
                crzb_l.id,
                get_crzb_with_p_name(crzb_l.id) as crzb_name
              from crzb_list as crzb_l
            ) as crzb_var
            on crzb_l.id = crzb_var.id
            where 
              (crzb_var.crzb_name like '%${req.params.search}%') 
              AND crzb_l.type=${role}
              AND crzb_l.active=1
            ORDER BY crzb_l.id LIMIT 10`, function (error, result) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                result
              });
            }
          });
        }
      });
    } else {
      res.json({
        status: false,
        message: "Invalid Parameters Values!"
      });
    }
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.post('/zone-add', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`SELECT rd_id FROM crzb_list where parent_id='${req.body.sel_cr_id}' order by id desc limit 1`, function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            let name_char = req.body.zone.charAt(0).toUpperCase();
            let last_rd_id = result.length > 0 ? result[0].rd_id : 'P01';
            let split_rd_id = last_rd_id.split("");
            split_rd_id.shift();
            let last_inc_rd_id = parseInt(split_rd_id.join("")) + 1;
            let gen_rd_id = name_char + getIntRd(last_inc_rd_id, 2, "00");
            connection.query(`INSERT INTO crzb_list SET ?`, {
              name: req.body.zone,
              description: req.body.desc,
              rd_id: gen_rd_id,
              parent_id: req.body.sel_cr_id,
              type: 2
            }, function (error, result) {
              if (error) {
                err_cb(error);
              }

              resolve();
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/zone-update', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let str_err = null;
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`SELECT * FROM crzb_list WHERE id=${req.body.update_id}`, async function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            if (result.length > 0) {
              const old_data_row = result[0];
              let gen_rd_id = old_data_row.rd_id;

              if (old_data_row.parent_id != req.body.sel_cr_id) {
                await new Promise(resp => {
                  connection.query(`SELECT * FROM crzb_list where parent_id='${req.body.sel_cr_id}' order by id desc limit 1`, function (error, result) {
                    if (error) {
                      err_cb(error);
                      resolve();
                    } else {
                      let name_char = req.body.zone.charAt(0).toUpperCase();
                      let last_rd_id = result.length > 0 ? result[0].rd_id : 'P00';
                      let split_rd_id = last_rd_id.split("");
                      split_rd_id.shift();
                      let last_inc_rd_id = parseInt(split_rd_id.join("")) + 1;
                      gen_rd_id = name_char + getIntRd(last_inc_rd_id, 2, "00");
                    }

                    resp();
                  });
                });
              }

              connection.query(`UPDATE crzb_list SET ? WHERE id=${req.body.update_id}`, {
                name: req.body.zone,
                description: req.body.desc,
                rd_id: gen_rd_id,
                parent_id: req.body.sel_cr_id
              }, function (error, result) {
                if (error) {
                  err_cb(error);
                }

                resolve();
              });
            } else {
              str_err = "Invalid Update ID!";
              resolve();
            }
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          if (str_err !== null) {
            res.json({
              status: false,
              message: str_err
            });
          } else {
            res.json({
              status: true
            });
          }
        }
      });
    }
  });
});
router.post('/tg-act-zone', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let str_err = null;
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`UPDATE crzb_list SET ? WHERE id=${req.body.tgl_id}`, {
          active: !req.body.sts
        }, function (error, result) {
          if (error) {
            err_cb(error);
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          if (str_err) {
            res.json({
              message: str_err,
              status: false
            });
          } else {
            res.json({
              status: true
            });
          }
        }
      });
    }
  });
});
router.post('/branch-added', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`SELECT * FROM crzb_list where parent_id='${req.body.sel_crzb_id}' order by rd_id desc limit 1`, function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            let last_branch_inc_rd_id = result.length > 0 ? parseInt(result[0].rd_id) + 1 : 1;
            let gen_rd_id = getIntRd(last_branch_inc_rd_id, 2, "00");
            connection.query(`INSERT INTO crzb_list SET ?`, {
              name: req.body.branch,
              description: req.body.branch_desc,
              rd_id: gen_rd_id,
              parent_id: req.body.sel_crzb_id,
              type: 3
            }, function (error, result) {
              if (error) {
                err_cb(error);
              }

              resolve();
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/branch-update', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let str_err = null;
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`SELECT * FROM crzb_list WHERE id=${req.body.update_id}`, async function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            if (result.length > 0) {
              const old_data_row = result[0];
              let gen_rd_id = old_data_row.rd_id;

              if (old_data_row.parent_id != req.body.sel_crzb_id) {
                await new Promise(resp => {
                  connection.query(`SELECT * FROM crzb_list where parent_id='${req.body.sel_crzb_id}' order by rd_id desc limit 1`, function (error, result) {
                    if (error) {
                      err_cb(error);
                      resolve();
                    } else {
                      let last_branch_inc_rd_id = result.length > 0 ? parseInt(result[0].rd_id) + 1 : 1;
                      gen_rd_id = getIntRd(last_branch_inc_rd_id, 2, "00");
                    }

                    resp();
                  });
                });
              }

              connection.query(`UPDATE crzb_list SET ? WHERE id=${req.body.update_id}`, {
                name: req.body.branch,
                description: req.body.branch_desc,
                rd_id: gen_rd_id,
                parent_id: req.body.sel_crzb_id,
                type: 3
              }, function (error, result) {
                if (error) {
                  err_cb(error);
                }

                resolve();
              });
            } else {
              str_err = "Invalid Update ID!";
              resolve();
            }
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          if (str_err !== null) {
            res.json({
              status: false,
              message: str_err
            });
          } else {
            res.json({
              status: true
            });
          }
        }
      });
    }
  });
});
router.post('/tg-act-branch', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let str_err = null;
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`UPDATE crzb_list SET ? WHERE id=${req.body.del_id}`, {
          active: !req.body.sts
        }, function (error, result) {
          if (error) {
            err_cb(error);
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          if (str_err) {
            res.json({
              message: str_err,
              status: false
            });
          } else {
            res.json({
              status: true
            });
          }
        }
      });
    }
  });
});
module.exports = router;

function getIntRd(numb_int, min_str, prep_str) {
  let new_val = numb_int.toString();
  new_val = new_val.length < min_str ? (prep_str + new_val).substr(-min_str, min_str) : new_val;
  return new_val;
}

/***/ }),

/***/ "./server/apis/emails.js":
/*!*******************************!*\
  !*** ./server/apis/emails.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const jwt = __webpack_require__(/*! jsonwebtoken */ "jsonwebtoken");

const config = __webpack_require__(/*! ../config */ "./server/config.js");

const trans_email = __webpack_require__(/*! ../e-conf.js */ "./server/e-conf.js");

const moment = __webpack_require__(/*! moment */ "moment");

const Request = __webpack_require__(/*! request */ "request");

const fs = __webpack_require__(/*! fs */ "fs");

const shortId = __webpack_require__(/*! shortid */ "shortid");

const multer = __webpack_require__(/*! multer */ "multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/../uploads/cvs');
  },
  filename: function (req, file, cb) {
    let file_id = Date.now();
    let newFile = file_id + typeGet(file.mimetype);
    req.body.file_id = file_id;
    req.body.file_name = newFile;
    cb(null, newFile);
  }
});
const upload = multer({
  storage
});
const captcha_secret = config.dev ? '6Le-JHcUAAAAAMWQGZRZ8_-WnUcvNyGOT8-2U-CE' : '6LdmTncUAAAAAOPZduWVaGRJ3HcxPrxhN78lEWYN';

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.post('/subs_email', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('SELECT email FROM `subscribers` where binary `email`=?', [req.body.email], function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            count: results.length
          });
        }
      });
    }
  });
});
router.post('/subscribe', function (req, res) {
  if (req.body.email && req.body.email !== '') {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        const token = shortId.generate();
        connection.query(`INSERT INTO subscribers SET ?`, {
          email: req.body.email,
          unsubscribe_id: token
        }, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.render('subscribe', {
              host: __webpack_require__(/*! ./../config */ "./server/config.js").dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
              token: token
            }, function (errPug, html) {
              if (errPug) {
                res.status(500).json({
                  errPug
                });
              } else {
                trans_email.sendMail({
                  from: '"MJ Supreme" <info@mj-supreme.com>',
                  to: req.body.email,
                  subject: 'Thank You For Subscribing!',
                  html: html
                }, function (err, info) {
                  if (err) {
                    res.status(500).json({
                      err
                    });
                  } else {
                    res.json({
                      status: true
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.post('/verify', function (req, res) {
  if (req.decoded.data.type === 0) {
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.beginTransaction(async function (error) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let throw_error = null;
            let temp_data = {};
            let send_email = null;
            let insert_id = null;
            await new Promise(resolve => {
              connection.query(`SELECT full_name, email FROM members WHERE id=${req.decoded.data.user_id}`, function (error, result) {
                if (error) {
                  throw_error = error;
                  return resolve();
                } else {
                  temp_data['name'] = result[0].full_name;
                  send_email = result[0].email;
                  temp_data['token'] = jwt.sign({
                    data: {
                      email: result[0].email,
                      user_id: req.decoded.data.user_id,
                      type: 0
                    }
                  }, config.secret, {
                    expiresIn: "1 day"
                  });
                  connection.query(`INSERT INTO tokens SET ?`, {
                    type: 0,
                    member_id: req.decoded.data.user_id,
                    token: temp_data['token']
                  }, function (error, result) {
                    if (error) {
                      throw_error = error;
                    } else {
                      insert_id = result.insertId;
                    }

                    return resolve();
                  });
                }
              });
            });

            if (throw_error) {
              return connection.rollback(function () {
                connection.release();
                res.status(500).json({
                  throw_error
                });
              });
            } else {
              connection.commit(function (err) {
                if (err) {
                  return connection.rollback(function () {
                    connection.release();
                    res.status(500).json({
                      err
                    });
                  });
                } else {
                  connection.release();

                  if (send_email !== null) {
                    res.render("verify-token", {
                      host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                      name: temp_data.name,
                      token: temp_data.token
                    }, function (errPug, html) {
                      if (errPug) {
                        last_id_delete_token(insert_id, function (err) {
                          res.json({
                            status: false,
                            message: err ? err.message : errPug.message
                          });
                        });
                      } else {
                        trans_email.sendMail({
                          from: '"MJ Supreme" <info@mj-supreme.com>',
                          to: send_email,
                          subject: 'Verification Token',
                          html: html
                        }, function (err, info) {
                          if (err) {
                            last_id_delete_token(insert_id, function (cb_err) {
                              res.json({
                                status: false,
                                message: cb_err ? cb_err.message : err.message
                              });
                            });
                          } else {
                            res.json({
                              status: true
                            });
                          }
                        });
                      }
                    });
                  } else {
                    res.json({
                      status: false,
                      message: "Email not found!"
                    });
                  }
                }
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Request!"
    });
  }
});
router.post('/forgot-password', function (req, res) {
  if (req.body.email && req.body.email !== '') {
    let email_or_id = req.body.email;
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.beginTransaction(async function (error) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let throw_error = null;
            let str_error = null;
            let temp_data = {};
            let send_email = null;
            let insert_id = null;
            await new Promise(resolve => {
              let last_3_hours = moment().subtract(3, 'h').format('YYYY-MM-DD HH-mm-ss');
              connection.query(`SELECT m.id, m.full_name, m.email, t.created_at
                FROM members as m
                LEFT JOIN tokens as t
                ON m.id=t.member_id AND t.created_at>'${last_3_hours}' AND t.type=1
                WHERE m.email=? OR m.user_asn_id=?
                ORDER BY t.created_at DESC
                LIMIT 1`, [email_or_id, email_or_id], function (error, result) {
                if (error) {
                  throw_error = error;
                  return resolve();
                } else {
                  if (result.length > 0) {
                    if (result[0].created_at !== null) {
                      str_error = "Your request under process please wait...";
                      return resolve();
                    } else {
                      if (result[0].email === null || result[0].email === '') {
                        str_error = "Your e-mail is not found. Contact administrator to add your e-mail.";
                        return resolve();
                      } else {
                        temp_data['name'] = result[0].full_name;
                        send_email = result[0].email;
                        temp_data['token'] = jwt.sign({
                          data: {
                            email: result[0].email,
                            user_id: result[0].id,
                            type: 1
                          }
                        }, config.secret, {
                          expiresIn: "3 hours"
                        });
                        connection.query(`INSERT INTO tokens SET ?`, {
                          type: 1,
                          member_id: result[0].id,
                          token: temp_data['token']
                        }, function (error, result) {
                          if (error) {
                            throw_error = error;
                          } else {
                            insert_id = result.insertId;
                          }

                          return resolve();
                        });
                      }
                    }
                  } else {
                    str_error = "Invalid E-mail or MJ-ID!";
                    return resolve();
                  }
                }
              });
            });

            if (throw_error || str_error) {
              return connection.rollback(function () {
                connection.release();

                if (throw_error) {
                  res.status(500).json({
                    throw_error
                  });
                } else {
                  res.json({
                    status: false,
                    message: str_error
                  });
                }
              });
            } else {
              connection.commit(function (err) {
                if (err) {
                  return connection.rollback(function () {
                    connection.release();
                    res.status(500).json({
                      err
                    });
                  });
                } else {
                  connection.release();
                  res.render("forgot-password", {
                    host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                    name: temp_data.name,
                    token: temp_data.token
                  }, function (errPug, html) {
                    if (errPug) {
                      last_id_delete_token(insert_id, function (err) {
                        res.json({
                          status: false,
                          message: err ? err.message : errPug.message
                        });
                      });
                    } else {
                      trans_email.sendMail({
                        from: '"MJ Supreme" <info@mj-supreme.com>',
                        to: send_email,
                        subject: 'Forgot Password!',
                        html: html
                      }, function (err, info) {
                        if (err) {
                          last_id_delete_token(insert_id, function (cb_err) {
                            res.json({
                              status: false,
                              message: cb_err ? cb_err.message : err.message
                            });
                          });
                        } else {
                          res.json({
                            status: true
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.post('/contact', function (req, res) {
  Request.post('https://www.google.com/recaptcha/api/siteverify', {
    form: {
      secret: captcha_secret,
      response: req.body.captcha
    }
  }, function (err, httpRes, body) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let resBody = JSON.parse(body);

      if (resBody.success === true) {
        db.getConnection(function (error, connection) {
          if (error) {
            res.status(500).json({
              error
            });
          } else {
            connection.query(`INSERT INTO contact_us SET ?`, {
              full_name: req.body.full_name,
              email: req.body.email,
              cont_number: req.body.cont_number,
              subject: req.body.subject,
              message: req.body.message
            }, function (error, result) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                res.render("contact", {
                  host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                  full_name: req.body.full_name,
                  email: req.body.email,
                  cont_number: req.body.cont_number,
                  subject: req.body.subject,
                  message: req.body.message
                }, function (errPug, html) {
                  if (errPug) {
                    res.json({
                      status: false,
                      message: errPug.message
                    });
                  } else {
                    trans_email.sendMail({
                      from: `"${req.body.full_name}" <${req.body.email}>`,
                      to: 'info@mj-supreme.com',
                      subject: `Contact Subject: ${req.body.subject}`,
                      html: html
                    }, function (err, info) {
                      if (err) {
                        res.json({
                          status: false,
                          message: err.message
                        });
                      } else {
                        res.json({
                          status: true
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      } else {
        res.json({
          status: false,
          err_code: '111',
          message: "Invalid Captcha!"
        });
      }
    }
  });
});
router.post('/career', upload.single('cv'), function (req, res) {
  Request.post('https://www.google.com/recaptcha/api/siteverify', {
    form: {
      secret: captcha_secret,
      response: req.body.captcha
    }
  }, function (err, httpRes, body) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let resBody = JSON.parse(body);

      if (resBody.success === true) {
        db.getConnection(function (error, connection) {
          if (error) {
            res.status(500).json({
              error
            });
          } else {
            connection.query(`INSERT INTO career SET ?`, {
              full_name: req.body.full_name,
              email: req.body.email,
              cont_number: req.body.cont_number,
              attachment: req.body.file_name
            }, function (error, result) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                res.render("career", {
                  host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                  full_name: req.body.full_name,
                  email: req.body.email,
                  cont_number: req.body.cont_number,
                  file: req.body.file_name
                }, function (errPug, html) {
                  if (errPug) {
                    res.json({
                      status: false,
                      message: errPug.message
                    });
                  } else {
                    trans_email.sendMail({
                      from: `"${req.body.full_name}" <${req.body.email}>`,
                      to: 'info@mj-supreme.com',
                      subject: `Career Form`,
                      html: html,
                      attachments: [{
                        filename: req.body.file_name,
                        content: fs.createReadStream(__dirname + '/../uploads/cvs/' + req.body.file_name)
                      }]
                    }, function (err, info) {
                      if (err) {
                        res.json({
                          status: false,
                          message: err.message
                        });
                      } else {
                        res.json({
                          status: true
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      } else {
        res.json({
          status: false,
          err_code: '111',
          message: "Invalid Captcha!"
        });
      }
    }
  });
});
module.exports = router;

function last_id_delete_token(lastId, cb) {
  db.getConnection(function (error, connection) {
    if (error) {
      cb(error);
    } else {
      connection.query('DELETE FROM tokens WHERE id=?', lastId, function (error, result) {
        connection.release();

        if (error) {
          cb(error);
        } else {
          cb();
        }
      });
    }
  });
}

function typeGet(mimetype) {
  let type = "";

  if (mimetype === "image/png") {
    type = ".png";
  }

  if (mimetype === "image/jpeg") {
    type = ".jpg";
  }

  if (mimetype === "application/pdf") {
    type = ".pdf";
  }

  if (mimetype === "application/msword") {
    type = ".doc";
  }

  if (mimetype === "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
    type = ".docx";
  }

  if (mimetype === "application/rtf") {
    type = ".rtf";
  }

  if (mimetype === "text/plain") {
    type = ".txt";
  }

  if (mimetype === "image/photoshop" || mimetype === "image/x-photoshop" || mimetype === "image/psd" || mimetype === "application/photoshop" || mimetype === "application/psd" || mimetype === "zz-application/zz-winassoc-psd") {
    type = ".psd";
  }

  return type;
}
/* WEBPACK VAR INJECTION */}.call(this, "server\\apis"))

/***/ }),

/***/ "./server/apis/franchise.js":
/*!**********************************!*\
  !*** ./server/apis/franchise.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 2) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/franchise-list', (req, res) => {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`SELECT COUNT(*) as tot_rows 
          from franchises as f
                  
          join (
            select 
              crzb_l.id as inner_id,
                get_crzb_rd_code(crzb_l.id) as crzb_code,
              get_crzb_with_p_name(crzb_l.id) as crzb_name
            from crzb_list as crzb_l
          ) as crzb_var
          on f.branch_id = crzb_var.inner_id
          
          join (
            select
              f_l.id as uc_id,
                  concat('UC', (f_l.rd_id * 1)) as uc_code
            from franchises as f_l
          ) as uc_gen
          on f.id = uc_gen.uc_id
          
          where (
            uc_gen.uc_code like '%${search}%' or
              f.name like '%${search}%' or
            crzb_var.crzb_code collate utf8mb4_general_ci like '%${search}%' or
            crzb_var.crzb_name collate utf8mb4_general_ci like '%${search}%'
          )`, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = result[0].tot_rows;
          connection.query(`select 
                  f.id, 
                  uc_gen.uc_code,
                  f.name, 
                  f.active,
                  crzb_var.crzb_code as branch_code,
                  crzb_var.crzb_name as branch
                    
                from franchises as f
                
                join (
                  select 
                    crzb_l.id as inner_id,
                      get_crzb_rd_code(crzb_l.id) as crzb_code,
                    get_crzb_with_p_name(crzb_l.id) as crzb_name
                  from crzb_list as crzb_l
                ) as crzb_var
                on f.branch_id = crzb_var.inner_id
                
                join (
                  select
                    f_l.id as uc_id,
                        concat('UC', (f_l.rd_id * 1)) as uc_code
                  from franchises as f_l
                ) as uc_gen
                on f.id = uc_gen.uc_id
                
                where (
                  uc_gen.uc_code like '%${search}%' or
                    f.name like '%${search}%' or
                  crzb_var.crzb_code collate utf8mb4_general_ci like '%${search}%' or
                  crzb_var.crzb_name collate utf8mb4_general_ci like '%${search}%'
                )
                ORDER BY id DESC
                LIMIT ${limit}
                OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.post('/multi-add', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`SELECT * FROM franchises where branch_id=${req.body.b_id} order by rd_id desc limit 1`, async function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            let b_id = req.body.b_id;
            let inc_start = result.length > 0 ? parseInt(result[0].rd_id) : 0;
            let err = null;

            for (let name of req.body.names) {
              inc_start++;
              let gen_rd_id = getIntRd(inc_start, 2, "00");
              await new Promise(res2 => {
                connection.query(`INSERT INTO franchises SET ?`, {
                  name,
                  rd_id: gen_rd_id,
                  branch_id: b_id
                }, function (error, result) {
                  if (error) {
                    err = error;
                  }

                  res2();
                });
              });
              if (err) break;
            }

            if (err) {
              err_cb(err);
            }

            resolve();
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/tg-act-franchise', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`UPDATE franchises SET ? WHERE id=${req.body.del_id}`, {
          active: !req.body.sts
        }, function (error, result) {
          if (error) {
            err_cb(error);
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
module.exports = router;

function getIntRd(numb_int, min_str, prep_str) {
  let new_val = numb_int.toString();
  new_val = new_val.length < min_str ? (prep_str + new_val).substr(-min_str, min_str) : new_val;
  return new_val;
}

/***/ }),

/***/ "./server/apis/gen_excels.js":
/*!***********************************!*\
  !*** ./server/apis/gen_excels.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const _ = __webpack_require__(/*! lodash */ "lodash");

const fs = __webpack_require__(/*! fs */ "fs");

const createCsvWriter = __webpack_require__(/*! csv-writer */ "csv-writer").createObjectCsvWriter;

router.post("/export_member_info", function (req, res) {
  if (req.decoded.data.type > 0 && _.isObject(req.body) && !_.isEmpty(req.body)) {
    let file_name = new Date().getTime() + '.csv';
    const csvWriter = createCsvWriter({
      path: __dirname + '/../uploads/reports/' + file_name,
      header: ['name', 'value']
    });
    csvWriter.writeRecords(req.body).then(() => {
      let file = __dirname + "/../uploads/reports/" + file_name;
      res.download(file);
      setTimeout(function () {
        fs.unlinkSync(__dirname + "/../uploads/reports/" + file_name);
      }, 60000);
    }).catch(err => {
      res.json({
        status: false,
        message: "Download error."
      });
    });
  } else {
    res.json({
      status: false,
      message: "Permission denied!"
    });
  }
});
module.exports = router;
/* WEBPACK VAR INJECTION */}.call(this, "server\\apis"))

/***/ }),

/***/ "./server/apis/hierarchy.js":
/*!**********************************!*\
  !*** ./server/apis/hierarchy.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const _ = __webpack_require__(/*! lodash */ "lodash");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.post("/", function (req, res) {
  let id = req.body.id;

  if (!/^[0-9]*$/.test(id)) {
    res.status(403).json({
      error: {
        message: "Invalid parameter!"
      }
    });
  } else {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        h_users(connection, id, function (err, result) {
          connection.release();

          if (err) {
            res.status(500).json({
              err
            });
          } else {
            res.json({
              data: result
            });
          }
        });
      }
    });
  }
});
router.get('/first_user', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query(`SELECT member_id FROM hierarchy_m WHERE id=1`, function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data: results
          });
        }
      });
    }
  });
});
router.get('/refl/direct/:id', function (req, res) {
  if (/^[0-9]*$/.test(req.params.id)) {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          error
        });
      } else {
        let member = {};
        await new Promise(resolve => {
          connection.query(gen_h_sql('member_id', req.params.id, 1), async function (error, results, fields) {
            if (error) {
              connection.release();
              resolve();
              res.status(500).json({
                error
              });
            } else {
              if (results.length > 0) {
                member = results[0];
              } else {
                connection.release();
              }

              resolve();
            }
          });
        });

        if (!_.isEmpty(member)) {
          let grab_results = [];
          await new Promise(resolve => {
            let opt = {
              sql: `SELECT m.id, m.user_asn_id, m.full_name, m.ref_user_asn_id, hm.id as hm_id, hm.parent_id, ivm.level, ivm.direct_ref_count, ivm.in_direct_ref_count
                                FROM members as m
                                LEFT JOIN hierarchy_m AS hm
                                ON m.id=hm.member_id
                                LEFT JOIN info_var_m AS ivm
                                ON m.id=ivm.member_id
                                WHERE m.ref_user_asn_id = ${member.user_asn_id} AND m.is_paid_m=1
                                ORDER BY hm_id ASC LIMIT 20`
            };
            connection.query(opt, async function (error, results, fields) {
              connection.release();

              if (error) {
                resolve();
                res.status(500).json({
                  error
                });
              } else {
                if (results.length > 0) {
                  let i = -1;
                  grab_results = await _.groupBy(results, function (o) {
                    i++;
                    return Math.floor(i / 4);
                  });
                }

                resolve();
              }
            });
          }); // create tree

          for (ind in grab_results) {
            let i = -1;

            for (item of grab_results[ind]) {
              i++;

              if (ind < 1) {
                item['directRef'] = true;

                _.set(member, ['childrens', i], item);
              } else {
                item['directRef'] = true;

                _.set(member, ['childrens', ind - 1, 'childrens', i], item);
              }
            }
          }

          let tree = [member];
          res.json({
            status: true,
            results: tree
          });
        } else {
          res.json({
            status: false,
            message: 'No member found!'
          });
        }
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
router.get('/find/:param', function (req, res) {
  let is_user = false,
      user_id = null;

  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    is_user = true;
    user_id = req.decoded.data.user_id;
  }

  let param = req.params.param;
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      if (is_user === true) {
        connection.query(`WITH RECURSIVE cte (id, user_asn_id, full_name, email) AS
                    (
                     SELECT h_m.member_id as id, m.user_asn_id, m.full_name, m.email
                     FROM hierarchy_m AS h_m
                     LEFT JOIN members AS m
                     ON h_m.member_id = m.id
                     WHERE m.id = ${user_id}
                     
                     UNION ALL
                     
                     SELECT h_m.member_id as id, m.user_asn_id, m.full_name, m.email
                     FROM hierarchy_m AS h_m
                     LEFT JOIN members AS m
                     ON h_m.member_id = m.id
                     INNER JOIN cte
                     ON m.ref_user_asn_id = cte.user_asn_id
                    )
                    SELECT * FROM cte 
                    WHERE id <> ${user_id} 
                    AND (user_asn_id LIKE ? OR full_name LIKE ? OR email LIKE ?)
                    ORDER BY id
                    LIMIT 10;`, ['%' + param + '%', '%' + param + '%', '%' + param + '%'], function (err, result) {
          connection.release();

          if (err) {
            res.status(500).json({
              err
            });
          } else {
            res.json({
              result: result
            });
          }
        });
      } else {
        connection.query(`
                SELECT h_m.member_id AS id, m.email, m.user_asn_id, m.full_name
                FROM hierarchy_m AS h_m
                LEFT JOIN members AS m
                ON h_m.member_id = m.id
                WHERE 
                (m.user_asn_id LIKE ?
                OR
                m.email LIKE ?
                OR
                m.full_name LIKE ?)
                ORDER BY h_m.id DESC
                LIMIT 10
                `, ['%' + param + '%', '%' + param + '%', '%' + param + '%'], function (err, result) {
          connection.release();

          if (err) {
            res.status(500).json({
              err
            });
          } else {
            res.json({
              result: result
            });
          }
        });
      }
    }
  });
});
module.exports = router;

async function h_users(connection, id, cb) {
  let g_results = [];
  let grab_data = [];
  let throw_err = null;
  await new Promise(resolve => {
    connection.query(gen_h_sql('member_id', id, 1), async function (error, results, fields) {
      if (error) {
        throw_err = error;
        resolve();
      } else {
        if (results.length > 0) {
          g_results.push(results);
        }

        resolve();
      }
    });
  });

  if (throw_err) {
    return cb(throw_err, grab_data);
  }

  for (result of g_results) {
    let is_full = false;
    await new Promise(async resolve => {
      row_loop: for (row of result) {
        grab_data.push(row);

        if (grab_data.length < 21) {
          await new Promise(resolve2 => {
            connection.query(gen_h_sql('parent_id', row.hm_id, 21 - grab_data.length), async function (error, results, fields) {
              if (error) {
                throw_err = error;
                resolve2();
              } else {
                if (results.length > 0) {
                  g_results.push(results);
                }

                resolve2();
              }
            });
          });

          if (throw_err) {
            resolve();
            break row_loop;
          }
        } else {
          is_full = true;
          resolve();
          break row_loop;
        }
      }

      resolve();
    });

    if (throw_err || is_full) {
      break;
    }
  }

  return cb(throw_err, grab_data);
}

function gen_h_sql(param, id, limit) {
  return {
    sql: `SELECT m.id, m.user_asn_id, m.full_name, m.ref_user_asn_id, hm.id as hm_id, hm.parent_id, ivm.level, ivm.direct_ref_count, ivm.in_direct_ref_count
            FROM hierarchy_m AS hm
            LEFT JOIN members AS m
            ON hm.member_id=m.id
            LEFT JOIN info_var_m AS ivm
            ON m.id=ivm.member_id
            WHERE hm.${param} = ${id}
            LIMIT ${limit}`
  };
}

/***/ }),

/***/ "./server/apis/hod.js":
/*!****************************!*\
  !*** ./server/apis/hod.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 0) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/zonal-sale-list/:hod_id', (req, res) => {
  if (!/^[0-9]*$/.test(req.params.hod_id)) {
    return res.status(500).json({
      error: "Invalid Parameters!"
    });
  }

  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select 
        COUNT(*) as tot_rows
        from (
          select 
            get_crzb_rd_code(b_list.id) as crzb_code,
            get_crzb_with_p_name(b_list.id) as crzb_name

          from mem_link_crzb as mem_lk_crzb
          join members as m
          on mem_lk_crzb.member_id = m.id and m.is_paid_m=1

          left join crzb_list as b_list
          on mem_lk_crzb.crzb_id = b_list.id
          left join crzb_list as z_list
          on b_list.parent_id = z_list.id

          join assign_roles_trans as asn_role_tns
          on z_list.id = asn_role_tns.crzb_id and mem_lk_crzb.member_id = asn_role_tns.linked_member_id

          where mem_lk_crzb.linked_mem_type=1 and z_list.id=${req.params.hod_id} and asn_role_tns.member_id=${req.decoded.data.user_id}
          group by b_list.id
        ) as all_data
        where 
          all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
          all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
        `, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select all_data.* from (
                select 
                  get_crzb_rd_code(b_list.id) as crzb_code,
                  get_crzb_with_p_name(b_list.id) as crzb_name,
                  count(*) as total_sale,
                  sum(if(m_tb_sale.member_id is null, 0, 1)) as month_sale

                from mem_link_crzb as mem_lk_crzb
                join members as m
                on mem_lk_crzb.member_id = m.id and m.is_paid_m=1

                left join (
                select 
                  mem_lk.member_id,
                  mem_lk.linked_at
                from mem_link_crzb as mem_lk
                ) as m_tb_sale
                on mem_lk_crzb.member_id = m_tb_sale.member_id and (m_tb_sale.linked_at >= '${gen_start_month}' and m_tb_sale.linked_at <= '${gen_end_month}')

                left join crzb_list as b_list
                on mem_lk_crzb.crzb_id = b_list.id
                left join crzb_list as z_list
                on b_list.parent_id = z_list.id

                join assign_roles_trans as asn_role_tns
                on z_list.id = asn_role_tns.crzb_id and mem_lk_crzb.member_id = asn_role_tns.linked_member_id

                where mem_lk_crzb.linked_mem_type=1 and z_list.id=${req.params.hod_id} and asn_role_tns.member_id=${req.decoded.data.user_id}
                group by b_list.id
              ) as all_data
              
              where 
                all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
                all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
              
              order by all_data.total_sale desc, all_data.month_sale desc
          
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/region-sale-list/:hod_id', (req, res) => {
  if (!/^[0-9]*$/.test(req.params.hod_id)) {
    return res.status(500).json({
      error: "Invalid Parameters!"
    });
  }

  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select 
        COUNT(*) as tot_rows
        from (
          select 
            asn_mem.user_asn_id as mj_id,
            asn_mem.full_name as mj_name,
            get_crzb_rd_code(z_list.id) as crzb_code,
            get_crzb_with_p_name(z_list.id) as crzb_name
          from assign_roles as asn_role

          join assign_roles_trans as asn_trans
          on asn_role.crzb_id=asn_trans.crzb_id and asn_role.member_id=asn_trans.member_id

          join mem_link_crzb as mem_lk_crzb
          on asn_trans.linked_member_id=mem_lk_crzb.member_id

          join members as m
          on mem_lk_crzb.member_id = m.id and m.is_paid_m=1

          left join crzb_list as b_list
          on mem_lk_crzb.crzb_id = b_list.id
          left join crzb_list as z_list
          on b_list.parent_id = z_list.id

          left join assign_roles_trans as asn_mem_tns
          on z_list.id = asn_mem_tns.crzb_id and mem_lk_crzb.member_id = asn_mem_tns.linked_member_id

          left join members as asn_mem
          on asn_mem_tns.member_id = asn_mem.id

          where asn_role.crzb_id=${req.params.hod_id} and asn_role.member_id=${req.decoded.data.user_id}
          group by z_list.id, asn_mem_tns.member_id
        ) as all_data
        where 
          all_data.mj_id like '%${search}%' or
          all_data.mj_name like '%${search}%' or
          all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
          all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
        `, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select all_data.* from (
                select 
                  asn_mem.user_asn_id as mj_id,
                  asn_mem.full_name as mj_name,
                  get_crzb_rd_code(z_list.id) as crzb_code,
                  get_crzb_with_p_name(z_list.id) as crzb_name,
                  count(*) as total_sale,
                  sum(if(m_tb_sale.member_id is null, 0, 1)) as month_sale
                from assign_roles as asn_role

                join assign_roles_trans as asn_trans
                on asn_role.crzb_id=asn_trans.crzb_id and asn_role.member_id=asn_trans.member_id

                join mem_link_crzb as mem_lk_crzb
                on asn_trans.linked_member_id=mem_lk_crzb.member_id

                left join (
                select 
                  mem_lk.member_id,
                  mem_lk.linked_at
                from mem_link_crzb as mem_lk
                ) as m_tb_sale
                on mem_lk_crzb.member_id = m_tb_sale.member_id and (m_tb_sale.linked_at >= '${gen_start_month}' and m_tb_sale.linked_at <= '${gen_end_month}')

                join members as m
                on mem_lk_crzb.member_id = m.id and m.is_paid_m=1

                left join crzb_list as b_list
                on mem_lk_crzb.crzb_id = b_list.id
                left join crzb_list as z_list
                on b_list.parent_id = z_list.id

                left join assign_roles_trans as asn_mem_tns
                on z_list.id = asn_mem_tns.crzb_id and mem_lk_crzb.member_id = asn_mem_tns.linked_member_id

                left join members as asn_mem
                on asn_mem_tns.member_id = asn_mem.id

                where asn_role.crzb_id=${req.params.hod_id} and asn_role.member_id=${req.decoded.data.user_id}
                group by z_list.id, asn_mem_tns.member_id
              ) as all_data
              
              where 
                all_data.mj_id like '%${search}%' or
                all_data.mj_name like '%${search}%' or
                all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
                all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
              
              order by all_data.total_sale desc, all_data.month_sale desc
          
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/country-sale-list/:hod_id', (req, res) => {
  if (!/^[0-9]*$/.test(req.params.hod_id)) {
    return res.status(500).json({
      error: "Invalid Parameters!"
    });
  }

  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select 
        COUNT(*) as tot_rows
        from (
          select 
            asn_mem.user_asn_id as mj_id,
            asn_mem.full_name as mj_name,
            get_crzb_rd_code(r_list.id) as crzb_code,
            get_crzb_with_p_name(r_list.id) as crzb_name
          from assign_roles as asn_role

          join assign_roles_trans as asn_trans
          on asn_role.crzb_id=asn_trans.crzb_id and asn_role.member_id=asn_trans.member_id

          join mem_link_crzb as mem_lk_crzb
          on asn_trans.linked_member_id=mem_lk_crzb.member_id

          join members as m
          on mem_lk_crzb.member_id = m.id and m.is_paid_m=1

          left join crzb_list as b_list
          on mem_lk_crzb.crzb_id = b_list.id
          left join crzb_list as z_list
          on b_list.parent_id = z_list.id
          left join crzb_list as r_list
          on z_list.parent_id = r_list.id

          left join assign_roles_trans as asn_mem_tns
          on r_list.id = asn_mem_tns.crzb_id and mem_lk_crzb.member_id = asn_mem_tns.linked_member_id

          left join members as asn_mem
          on asn_mem_tns.member_id = asn_mem.id

          where asn_role.crzb_id=${req.params.hod_id} and asn_role.member_id=${req.decoded.data.user_id}
          group by r_list.id, asn_mem_tns.member_id
        ) as all_data
        where 
          all_data.mj_id like '%${search}%' or
          all_data.mj_name like '%${search}%' or
          all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
          all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
        `, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select all_data.* from (
                select 
                  asn_mem.user_asn_id as mj_id,
                  asn_mem.full_name as mj_name,
                  get_crzb_rd_code(r_list.id) as crzb_code,
                  get_crzb_with_p_name(r_list.id) as crzb_name,
                  count(*) as total_sale,
                  sum(if(m_tb_sale.member_id is null, 0, 1)) as month_sale
                from assign_roles as asn_role

                join assign_roles_trans as asn_trans
                on asn_role.crzb_id=asn_trans.crzb_id and asn_role.member_id=asn_trans.member_id

                join mem_link_crzb as mem_lk_crzb
                on asn_trans.linked_member_id=mem_lk_crzb.member_id

                left join (
                select 
                  mem_lk.member_id,
                  mem_lk.linked_at
                from mem_link_crzb as mem_lk
                ) as m_tb_sale
                on mem_lk_crzb.member_id = m_tb_sale.member_id and (m_tb_sale.linked_at >= '${gen_start_month}' and m_tb_sale.linked_at <= '${gen_end_month}')

                join members as m
                on mem_lk_crzb.member_id = m.id and m.is_paid_m=1

                left join crzb_list as b_list
                on mem_lk_crzb.crzb_id = b_list.id
                left join crzb_list as z_list
                on b_list.parent_id = z_list.id
                left join crzb_list as r_list
                on z_list.parent_id = r_list.id

                left join assign_roles_trans as asn_mem_tns
                on r_list.id = asn_mem_tns.crzb_id and mem_lk_crzb.member_id = asn_mem_tns.linked_member_id

                left join members as asn_mem
                on asn_mem_tns.member_id = asn_mem.id

                where asn_role.crzb_id=${req.params.hod_id} and asn_role.member_id=${req.decoded.data.user_id}
                group by r_list.id, asn_mem_tns.member_id
              ) as all_data
              
              where 
                all_data.mj_id like '%${search}%' or
                all_data.mj_name like '%${search}%' or
                all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
                all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
              
              order by all_data.total_sale desc, all_data.month_sale desc
          
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/country-comm-list/:hod_id', (req, res) => {
  if (!/^[0-9]*$/.test(req.params.hod_id)) {
    return res.status(500).json({
      error: "Invalid Parameters!"
    });
  }

  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select 
        COUNT(*) as tot_rows
        from (
          SELECT 
            asn_mem.user_asn_id as mj_id,
            asn_mem.full_name as mj_name,
            get_crzb_rd_code(r_l.id) as crzb_code,
            get_crzb_with_p_name(r_l.id) as crzb_name
          FROM assign_roles_trans as asn_trans

          join mem_link_crzb as mem_lk_crzb
          on asn_trans.linked_member_id = mem_lk_crzb.member_id

          join crzb_list as b_l
          on mem_lk_crzb.crzb_id = b_l.id
          join crzb_list as z_l
          on b_l.parent_id = z_l.id
          join crzb_list as r_l
          on z_l.parent_id = r_l.id

          left join assign_roles_trans as asn_mem_trn
          on mem_lk_crzb.member_id = asn_mem_trn.linked_member_id and asn_mem_trn.crzb_id = r_l.id

          left join members as asn_mem
          on asn_mem_trn.member_id= asn_mem.id

          where asn_trans.member_id=${req.decoded.data.user_id} and asn_trans.crzb_id=${req.params.hod_id}
          group by asn_mem.id, r_l.id
        ) as all_data
        where 
          all_data.mj_id like '%${search}%' or
          all_data.mj_name like '%${search}%' or
          all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
          all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
        `, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select all_data.* from (
                SELECT 
                  asn_mem.user_asn_id as mj_id,
                  asn_mem.full_name as mj_name,
                  get_crzb_rd_code(r_l.id) as crzb_code,
                  get_crzb_with_p_name(r_l.id) as crzb_name,
                  sum(asn_trans.amount) as total_comm,
                  sum(if(m_tb_sale.member_id is null, 0, asn_trans.amount)) as month_comm
                FROM assign_roles_trans as asn_trans

                join mem_link_crzb as mem_lk_crzb
                on asn_trans.linked_member_id = mem_lk_crzb.member_id

                left join (
                select 
                  mem_lk.member_id,
                  mem_lk.linked_at
                from mem_link_crzb as mem_lk
                ) as m_tb_sale
                on mem_lk_crzb.member_id = m_tb_sale.member_id and (m_tb_sale.linked_at >= '${gen_start_month}' and m_tb_sale.linked_at <= '${gen_end_month}')

                join crzb_list as b_l
                on mem_lk_crzb.crzb_id = b_l.id
                join crzb_list as z_l
                on b_l.parent_id = z_l.id
                join crzb_list as r_l
                on z_l.parent_id = r_l.id

                left join assign_roles_trans as asn_mem_trn
                on mem_lk_crzb.member_id = asn_mem_trn.linked_member_id and asn_mem_trn.crzb_id = r_l.id

                left join members as asn_mem
                on asn_mem_trn.member_id= asn_mem.id

                where asn_trans.member_id=${req.decoded.data.user_id} and asn_trans.crzb_id=${req.params.hod_id}
                group by asn_mem.id, r_l.id
              ) as all_data
              
              where 
                all_data.mj_id like '%${search}%' or
                all_data.mj_name like '%${search}%' or
                all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
                all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
              
              order by all_data.total_comm desc, all_data.month_comm desc
          
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/region-comm-list/:hod_id', (req, res) => {
  if (!/^[0-9]*$/.test(req.params.hod_id)) {
    return res.status(500).json({
      error: "Invalid Parameters!"
    });
  }

  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select 
        COUNT(*) as tot_rows
        from (
          SELECT 
            asn_mem.user_asn_id as mj_id,
            asn_mem.full_name as mj_name,
            get_crzb_rd_code(z_l.id) as crzb_code,
            get_crzb_with_p_name(z_l.id) as crzb_name
          FROM assign_roles_trans as asn_trans

          join mem_link_crzb as mem_lk_crzb
          on asn_trans.linked_member_id = mem_lk_crzb.member_id

          join crzb_list as b_l
          on mem_lk_crzb.crzb_id = b_l.id
          join crzb_list as z_l
          on b_l.parent_id = z_l.id

          left join assign_roles_trans as asn_mem_trn
          on mem_lk_crzb.member_id = asn_mem_trn.linked_member_id and asn_mem_trn.crzb_id = z_l.id

          left join members as asn_mem
          on asn_mem_trn.member_id= asn_mem.id

          where asn_trans.member_id=${req.decoded.data.user_id} and asn_trans.crzb_id=${req.params.hod_id}
          group by asn_mem.id, z_l.id
        ) as all_data
        where 
          all_data.mj_id like '%${search}%' or
          all_data.mj_name like '%${search}%' or
          all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
          all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
        `, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select all_data.* from (
                SELECT 
                  asn_mem.user_asn_id as mj_id,
                  asn_mem.full_name as mj_name,
                  get_crzb_rd_code(z_l.id) as crzb_code,
                  get_crzb_with_p_name(z_l.id) as crzb_name,
                  sum(asn_trans.amount) as total_comm,
                  sum(if(m_tb_sale.member_id is null, 0, asn_trans.amount)) as month_comm
                FROM assign_roles_trans as asn_trans

                join mem_link_crzb as mem_lk_crzb
                on asn_trans.linked_member_id = mem_lk_crzb.member_id

                left join (
                select 
                  mem_lk.member_id,
                  mem_lk.linked_at
                from mem_link_crzb as mem_lk
                ) as m_tb_sale
                on mem_lk_crzb.member_id = m_tb_sale.member_id and (m_tb_sale.linked_at >= '${gen_start_month}' and m_tb_sale.linked_at <= '${gen_end_month}')

                join crzb_list as b_l
                on mem_lk_crzb.crzb_id = b_l.id
                join crzb_list as z_l
                on b_l.parent_id = z_l.id

                left join assign_roles_trans as asn_mem_trn
                on mem_lk_crzb.member_id = asn_mem_trn.linked_member_id and asn_mem_trn.crzb_id = z_l.id

                left join members as asn_mem
                on asn_mem_trn.member_id= asn_mem.id

                where asn_trans.member_id=${req.decoded.data.user_id} and asn_trans.crzb_id=${req.params.hod_id}
                group by asn_mem.id, z_l.id
              ) as all_data
              
              where 
                all_data.mj_id like '%${search}%' or
                all_data.mj_name like '%${search}%' or
                all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
                all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
              
              order by all_data.total_comm desc, all_data.month_comm desc
          
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/zonal-comm-list/:hod_id', (req, res) => {
  if (!/^[0-9]*$/.test(req.params.hod_id)) {
    return res.status(500).json({
      error: "Invalid Parameters!"
    });
  }

  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select 
        COUNT(*) as tot_rows
        from (
          SELECT 
            get_crzb_rd_code(mem_lk_crzb.crzb_id) as crzb_code,
            get_crzb_with_p_name(mem_lk_crzb.crzb_id) as crzb_name
          FROM assign_roles_trans as asn_trans

          join mem_link_crzb as mem_lk_crzb
          on asn_trans.linked_member_id = mem_lk_crzb.member_id

          where asn_trans.member_id=${req.decoded.data.user_id} and asn_trans.crzb_id=${req.params.hod_id}
          group by mem_lk_crzb.crzb_id
        ) as all_data
        where 
          all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
          all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
        `, function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          }

          let tot_rows = result[0].tot_rows;
          let date = moment();
          let gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
              gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
          connection.query(`select all_data.* from (
                SELECT 
                  get_crzb_rd_code(mem_lk_crzb.crzb_id) as crzb_code,
                  get_crzb_with_p_name(mem_lk_crzb.crzb_id) as crzb_name,
                  sum(asn_trans.amount) as total_comm,
                  sum(if(m_tb_sale.member_id is null, 0, asn_trans.amount)) as month_comm
                FROM assign_roles_trans as asn_trans

                join mem_link_crzb as mem_lk_crzb
                on asn_trans.linked_member_id = mem_lk_crzb.member_id

                left join (
                select 
                  mem_lk.member_id,
                  mem_lk.linked_at
                from mem_link_crzb as mem_lk
                ) as m_tb_sale
                on mem_lk_crzb.member_id = m_tb_sale.member_id and (m_tb_sale.linked_at >= '${gen_start_month}' and m_tb_sale.linked_at <= '${gen_end_month}')

                where asn_trans.member_id=${req.decoded.data.user_id} and asn_trans.crzb_id=${req.params.hod_id}
                group by mem_lk_crzb.crzb_id
              ) as all_data
              
              where
                all_data.crzb_code collate utf8mb4_general_ci like '%${search}%' or 
                all_data.crzb_name collate utf8mb4_general_ci like '%${search}%'
              
              order by all_data.total_comm desc, all_data.month_comm desc
          
              LIMIT ${limit}
              OFFSET ${offset}`, function (error, results) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/sale-count/:hod_id', (req, res) => {
  if (!/^[0-9]*$/.test(req.params.hod_id)) {
    return res.status(500).json({
      error: "Invalid HOD ID!"
    });
  }

  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let data = {
        total_sale: 0,
        yearly_sale: 0,
        monthly_sale: 0,
        type: null,
        name: null
      };
      let date = moment();
      let gen_start_year = date.clone().startOf('year').format('YYYY-MM-DD HH:mm:ss'),
          gen_end_year = date.clone().endOf('year').format('YYYY-MM-DD HH:mm:ss'),
          gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
          gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query(`select 
              crzb_l.id as crzb_id,
              crzb_l.name as crzb_name,
              crzb_l.type as crzb_type,
              (
              select count(*) 
                  from assign_roles_trans as trans
                  where trans.crzb_id=${req.params.hod_id} and trans.member_id=${req.decoded.data.user_id}
              ) as total_sale,
              (
              select count(*) 
                  from assign_roles_trans as trans
                  join mem_link_crzb as mem_lk
                  on trans.linked_member_id=mem_lk.member_id and (mem_lk.linked_at >= '${gen_start_year}' and mem_lk.linked_at <= '${gen_end_year}')
                  where trans.crzb_id=${req.params.hod_id} and trans.member_id=${req.decoded.data.user_id}
              ) as yearly_sale,
              (
              select count(*) 
                  from assign_roles_trans as trans
                  join mem_link_crzb as mem_lk
                  on trans.linked_member_id=mem_lk.member_id and (mem_lk.linked_at >= '${gen_start_month}' and mem_lk.linked_at <= '${gen_end_month}')
                  where trans.crzb_id=${req.params.hod_id} and trans.member_id=${req.decoded.data.user_id}
              ) as monthly_sale
          from assign_roles as asn_role
          left join crzb_list as crzb_l 
          on asn_role.crzb_id=crzb_l.id
          where asn_role.crzb_id=${req.params.hod_id} and asn_role.member_id=${req.decoded.data.user_id}`, function (error, result) {
          if (error) {
            err_hdl(error);
          } else {
            data['total_sale'] = result[0].total_sale;
            data['yearly_sale'] = result[0].yearly_sale;
            data['monthly_sale'] = result[0].monthly_sale;
            data['type'] = result[0].crzb_type;
            data['name'] = result[0].crzb_name;
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data
          });
        }
      });
    }
  });
});
router.get('/commission-count/:hod_id', (req, res) => {
  if (!/^[0-9]*$/.test(req.params.hod_id)) {
    return res.status(500).json({
      error: "Invalid HOD ID!"
    });
  }

  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let data = {
        total_comm: 0,
        yearly_comm: 0,
        monthly_comm: 0,
        type: null,
        name: null
      };
      let date = moment();
      let gen_start_year = date.clone().startOf('year').format('YYYY-MM-DD HH:mm:ss'),
          gen_end_year = date.clone().endOf('year').format('YYYY-MM-DD HH:mm:ss'),
          gen_start_month = date.clone().startOf('month').format('YYYY-MM-DD HH:mm:ss'),
          gen_end_month = date.clone().endOf('month').format('YYYY-MM-DD HH:mm:ss');
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query(`select 
            crzb_l.id as crzb_id,
            crzb_l.name as crzb_name,
            crzb_l.type as crzb_type,
            sum(trans.amount) as total,
            ifnull(get_crzb_mem_comm_monthly(trans.crzb_id, trans.member_id, '${gen_start_year}', '${gen_end_year}'), 0) as yearly,
            ifnull(get_crzb_mem_comm_monthly(trans.crzb_id, trans.member_id, '${gen_start_month}', '${gen_end_month}'), 0) as monthly
              
          from assign_roles_trans as trans
          
          left join crzb_list as crzb_l 
          on trans.crzb_id=crzb_l.id
          
          where
          trans.member_id = ${req.decoded.data.user_id}
          and
          trans.crzb_id = ${req.params.hod_id}`, function (error, result) {
          if (error) {
            err_hdl(error);
          } else {
            data['total_comm'] = result[0].total;
            data['yearly_comm'] = result[0].yearly;
            data['monthly_comm'] = result[0].monthly;
            data['type'] = result[0].crzb_type;
            data['name'] = result[0].crzb_name;
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data
          });
        }
      });
    }
  });
});
router.get('/top5-childs-sale/:hod_id', (req, res) => {
  if (!/^[0-9]*$/.test(req.params.hod_id)) {
    return res.status(500).json({
      error: "Invalid HOD ID!"
    });
  }

  db.getConnection(function (error, connection) {
    if (error) {
      res.status(500).json({
        error
      });
    } else {
      let data = [];
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query(`select 
              if(
              crzb_lvl_1.l1_p=asn_role.crzb_id, 
                crzb_lvl_1.l1_name, 
                if(
                  crzb_lvl_1.l2_p=asn_role.crzb_id,
                  crzb_lvl_1.l2_name,
                  if(
                    crzb_lvl_1.l3_p=asn_role.crzb_id,
                    crzb_lvl_1.l3_name,
                    ''
                    )
                  )
              ) as name,
              count(crzb_lvl_1.l1_id) as sale
            from assign_roles as asn_role
          
            left join assign_roles_trans as asnr_tr 
            on asn_role.member_id = asnr_tr.member_id and asn_role.crzb_id = asnr_tr.crzb_id
          
            right join mem_link_crzb as mem_lk_crzb
            on asnr_tr.linked_member_id = mem_lk_crzb.member_id
          
            left join (
              select 
                crzb_1.id as l1_id,
                crzb_1.name as l1_name,
                crzb_1.parent_id as l1_p,
                crzb_2.id as l2_id,
                crzb_2.name as l2_name,
                crzb_2.parent_id as l2_p,
                crzb_3.id as l3_id,
                crzb_3.name as l3_name,
                crzb_3.parent_id as l3_p,
                crzb_4.id as l4_id,
                crzb_4.name as l4_name,
                crzb_4.parent_id as l4_p
              from crzb_list as crzb_1
                
                left join crzb_list as crzb_2
                on crzb_1.parent_id = crzb_2.id
                
                left join crzb_list as crzb_3
                on crzb_2.parent_id = crzb_3.id
                
                left join crzb_list as crzb_4
                on crzb_3.parent_id = crzb_4.id
            ) crzb_lvl_1
            on mem_lk_crzb.crzb_id = crzb_lvl_1.l1_id
          
            where asn_role.member_id=${req.decoded.data.user_id} and asn_role.crzb_id=${req.params.hod_id}
          
            group by if(
                    crzb_lvl_1.l1_p=asn_role.crzb_id, 
                      crzb_lvl_1.l1_id, 
                      if(
                        crzb_lvl_1.l2_p=asn_role.crzb_id,
                          crzb_lvl_1.l2_id,
                          if(
                            crzb_lvl_1.l3_p=asn_role.crzb_id,
                              crzb_lvl_1.l3_id,
                              ''
                          )
                        )
                      )
            order by sale desc
            limit 5`, function (error, result) {
          if (error) {
            err_hdl(error);
          } else {
            data = result;
          }

          resolve();
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data
          });
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/index.js":
/*!******************************!*\
  !*** ./server/apis/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const cors = __webpack_require__(/*! cors */ "cors");

const config = __webpack_require__(/*! ../config.js */ "./server/config.js");

const jwt = __webpack_require__(/*! jsonwebtoken */ "jsonwebtoken");

let corsOptions = {
  optionsSuccessStatus: 200,
  origin: config.dev ? 'http://127.0.0.1:3000' : ['https://mj-supreme.com', 'https://www.mj-supreme.com']
};
router.use("/verify-token", cors(corsOptions), __webpack_require__(/*! ./verify-tokens.js */ "./server/apis/verify-tokens.js"));
router.use("/web", cors(corsOptions), __webpack_require__(/*! ./web.js */ "./server/apis/web.js"));
router.use((req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, config.secret, function (err, decoded) {
      if (err) {
        return res.status(403).json({
          status: false,
          message: err.message
        });
      } else {
        req.decoded = decoded; // res.json(decoded)

        next();
      }
    });
  } else {
    return res.status(403).json({
      status: false,
      message: 'No token provided.'
    });
  }
});
router.use("/hierarchy", cors(corsOptions), __webpack_require__(/*! ./hierarchy.js */ "./server/apis/hierarchy.js"));
router.use("/product", cors(corsOptions), __webpack_require__(/*! ./products.js */ "./server/apis/products.js"));
router.use("/member", cors(corsOptions), __webpack_require__(/*! ./members.js */ "./server/apis/members.js"));
router.use("/moderator", cors(corsOptions), __webpack_require__(/*! ./moderators.js */ "./server/apis/moderators.js"));
router.use("/admin", cors(corsOptions), __webpack_require__(/*! ./admin.js */ "./server/apis/admin.js"));
router.use("/transaction", cors(corsOptions), __webpack_require__(/*! ./transactions.js */ "./server/apis/transactions.js"));
router.use("/bank-detail", cors(corsOptions), __webpack_require__(/*! ./bank-details.js */ "./server/apis/bank-details.js"));
router.use("/commission", cors(corsOptions), __webpack_require__(/*! ./commissions.js */ "./server/apis/commissions.js"));
router.use("/profile", cors(corsOptions), __webpack_require__(/*! ./profiles.js */ "./server/apis/profiles.js"));
router.use("/receipt", cors(corsOptions), __webpack_require__(/*! ./receipts.js */ "./server/apis/receipts.js"));
router.use("/notification", cors(corsOptions), __webpack_require__(/*! ./notifications.js */ "./server/apis/notifications.js"));
router.use("/report", cors(corsOptions), __webpack_require__(/*! ./reports.js */ "./server/apis/reports.js"));
router.use("/reward", cors(corsOptions), __webpack_require__(/*! ./rewards.js */ "./server/apis/rewards.js"));
router.use("/partner", cors(corsOptions), __webpack_require__(/*! ./partners.js */ "./server/apis/partners.js"));
router.use("/startup", cors(corsOptions), __webpack_require__(/*! ./startup_check.js */ "./server/apis/startup_check.js"));
router.use("/gen_excel", cors(corsOptions), __webpack_require__(/*! ./gen_excels.js */ "./server/apis/gen_excels.js"));
router.use("/email", cors(corsOptions), __webpack_require__(/*! ./emails.js */ "./server/apis/emails.js"));
router.use("/c_subsidiary", cors(corsOptions), __webpack_require__(/*! ./c_subsidiary.js */ "./server/apis/c_subsidiary.js"));
router.use("/voucher", cors(corsOptions), __webpack_require__(/*! ./vouchers.js */ "./server/apis/vouchers.js"));
router.use("/crzb-list", cors(corsOptions), __webpack_require__(/*! ./crzb_list.js */ "./server/apis/crzb_list.js")); // router.use("/crct-list", cors(corsOptions), require('./crct_list.js'))

router.use("/assign-role", cors(corsOptions), __webpack_require__(/*! ./assign-role.js */ "./server/apis/assign-role.js"));
router.use("/assign-role-trans", cors(corsOptions), __webpack_require__(/*! ./assign-role-trans.js */ "./server/apis/assign-role-trans.js"));
router.use("/company-hierarchy", cors(corsOptions), __webpack_require__(/*! ./company-hierarchy.js */ "./server/apis/company-hierarchy.js"));
router.use("/hod", cors(corsOptions), __webpack_require__(/*! ./hod.js */ "./server/apis/hod.js"));
router.use("/franchise", cors(corsOptions), __webpack_require__(/*! ./franchise.js */ "./server/apis/franchise.js"));
router.use("/assign-role-fr", cors(corsOptions), __webpack_require__(/*! ./assign-role-fr.js */ "./server/apis/assign-role-fr.js"));
router.use("/assign-role-trans-fr", cors(corsOptions), __webpack_require__(/*! ./assign-role-trans-fr.js */ "./server/apis/assign-role-trans-fr.js"));
router.use("/campaign", cors(corsOptions), __webpack_require__(/*! ./campaign.js */ "./server/apis/campaign.js"));
router.use("/nomination", cors(corsOptions), __webpack_require__(/*! ./nomination.js */ "./server/apis/nomination.js"));
router.use("/lucky-draw", cors(corsOptions), __webpack_require__(/*! ./lucky-draw.js */ "./server/apis/lucky-draw.js"));
router.all("*", function (req, res) {
  res.status(403).json({
    error: 'Invalid Request!'
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/lucky-draw.js":
/*!***********************************!*\
  !*** ./server/apis/lucky-draw.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const _ = __webpack_require__(/*! lodash */ "lodash");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 0 || req.decoded.data.type === 2) {
    return next();
  } else {
    return res.status(500).json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/today-winners', (req, res) => {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let start_of_day = moment().startOf('d').format('YYYY-MM-DD HH:mm:ss');
      connection.query(`select 
            ld_w.group as ld_id,
            ld_w.created_at as win_at,
            m.user_asn_id,
            m.full_name,
            prd.name as prd_name
          from ld_winners as ld_w
          join members as m
          on ld_w.member_id = m.id
          join products as prd
          on ld_w.prd_id = prd.id

          where ld_w.created_at >= '${start_of_day}'
          
          order by ld_w.created_at desc
          
          limit 10`, function (error, result) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            result
          });
        }
      });
    }
  });
});
router.use(function (req, res, next) {
  if (req.decoded.data.type === 2) {
    return next();
  } else {
    return res.status(500).json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/ld-list/:prd_type', function (req, res) {
  if (!/^1$|^2$/.test(req.params.prd_type)) {
    return res.status(500).json({
      error: "Invalid Parameters!"
    });
  }

  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 5,
          load_grp = 5,
          groups_data = [];
      let date = moment("2019-02-01"),
          start_at = date.startOf('d').format('YYYY-MM-DD HH:mm:ss');

      if (req.query.offset && /^[0-9]*$/.test(req.query.offset)) {
        offset = parseInt(req.query.offset) % 5 === 0 ? parseInt(req.query.offset) : 0;
      }

      connection.query(`SELECT 
          count(*) as tot_rows
        FROM hierarchy_m as hir 
        join members as mem
        on hir.member_id = mem.id
        join members as ref_mem
        on mem.ref_user_asn_id = ref_mem.user_asn_id
        join user_product_details as usr_prd
        on mem.id = usr_prd.member_id
        where 
          hir.created_at >= '${start_at}' and
          usr_prd.product_id = ${req.params.prd_type}
        order by hir.created_at`, async function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = result.length ? result[0].tot_rows : 0;
          let proc_rows = offset;
          let grp_inc = offset / limit;
          let err;

          while (proc_rows < tot_rows) {
            if (groups_data.length >= load_grp) break;
            proc_rows += limit; // proc_rows = proc_rows > tot_rows ? tot_rows : proc_rows

            grp_inc++;
            let exist_qrp = false;
            await new Promise(resolve => {
              connection.query(`SELECT \`group\` FROM ld_winners where prd_id=${req.params.prd_type} and \`group\`=${grp_inc} order by id desc limit 1`, function (error, result) {
                if (error) {
                  err = error;
                } else {
                  exist_qrp = result.length > 0;
                }

                return resolve();
              });
            });
            if (err) break;

            if (exist_qrp) {
              continue;
            }

            await new Promise(resolve => {
              connection.query(`SELECT 
                    ref_mem.full_name,
                    ref_mem.user_asn_id,
                    mem.user_asn_id as lnk_mem_id,
                    hir.created_at as paid_at
                  FROM hierarchy_m as hir 
                  join members as mem
                  on hir.member_id = mem.id
                  join members as ref_mem
                  on mem.ref_user_asn_id = ref_mem.user_asn_id
                  join user_product_details as usr_prd
                  on mem.id = usr_prd.member_id
                  where 
                    hir.created_at >= '${start_at}' and
                    usr_prd.product_id = ${req.params.prd_type}
                  order by hir.created_at
                  limit ${limit}
                  offset ${proc_rows - limit}`, function (error, result) {
                if (error) {
                  err = error;
                } else {
                  groups_data.push({
                    group: grp_inc,
                    data: result
                  });
                }

                return resolve();
              });
            });
            if (err) break;
          }

          connection.release();

          if (err) {
            return res.status(500).json({
              error
            });
          } else {
            res.json({
              data: groups_data,
              tot_rows,
              last_offset: proc_rows
            });
          }
        }
      });
    }
  });
});
router.get('/winner-list', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`select 
          count(*) as tot_rows 
        from ld_winners as ld_w
        join members as m
        on ld_w.member_id = m.id
        join products as prd
        on ld_w.prd_id = prd.id
        ${search !== '' ? 'where (ld_w.group like ? or prd.name like ? or m.user_asn_id like ? or m.full_name like ?)' : ''}`, [`%${search}%`, `%${search}%`, `%${search}%`, `%${search}%`], function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = result.length ? result[0].tot_rows : 0;

          if (tot_rows < 1) {
            connection.release();
            return res.json({
              data: [],
              tot_rows: 0
            });
          } else {
            connection.query(`select 
                  ld_w.id,
                  ld_w.group,
                  ld_w.created_at,
                  m.user_asn_id,
                  m.full_name,
                  prd.name as prd_name
                from ld_winners as ld_w
                join members as m
                on ld_w.member_id = m.id
                join products as prd
                on ld_w.prd_id = prd.id
                
                ${search !== '' ? 'where (ld_w.group like ? or prd.name like ? or m.user_asn_id like ? or m.full_name like ?)' : ''}
                
                order by ld_w.created_at desc
                
                limit ${limit}
                offset ${offset}`, [`%${search}%`, `%${search}%`, `%${search}%`, `%${search}%`], function (error, result) {
              connection.release();

              if (error) {
                return res.status(500).json({
                  error
                });
              } else {
                res.json({
                  data: result,
                  tot_rows
                });
              }
            });
          }
        }
      });
    }
  });
});
router.get('/ld-detail/:id', (req, res) => {
  if (!req.params.id || !/^[0-9]*$/.test(req.params.id)) {
    return res.status(500).json({
      error: "Invalid Parameters!"
    });
  } else {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT * from ld_winners where id=?`, req.params.id, function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            if (result.length < 1) {
              connection.release();
              return res.status(500).json({
                error: "Invalid ID!"
              });
            } else {
              let winner_mem_id = result[0].member_id;
              let limit = 5,
                  gourp = parseInt(result[0].group),
                  offset = (gourp - 1) * limit;
              let date = moment("2019-02-01"),
                  start_at = date.startOf('d').format('YYYY-MM-DD HH:mm:ss');
              connection.query(`SELECT 
                    ref_mem.id,
                    ref_mem.full_name,
                    ref_mem.user_asn_id,
                    mem.user_asn_id as lnk_mem_id,
                    hir.created_at
                  FROM hierarchy_m as hir 
                  join members as mem
                  on hir.member_id = mem.id
                  join members as ref_mem
                  on mem.ref_user_asn_id = ref_mem.user_asn_id
                  join user_product_details as usr_prd
                  on mem.id = usr_prd.member_id
                  where 
                    hir.created_at >= '${start_at}' and 
                    usr_prd.product_id = ${result[0].prd_id}
                  order by hir.created_at
                  limit ${limit}
                  offset ${offset}`, function (error, result) {
                connection.release();

                if (error) {
                  return res.status(500).json({
                    error
                  });
                } else {
                  res.json({
                    data: result,
                    winner: {
                      mj_id: _.find(result, {
                        id: winner_mem_id
                      }).user_asn_id,
                      grp_id: gourp
                    }
                  });
                }
              });
            }
          }
        });
      }
    });
  }
});
router.post('/spin/:prd_type/:grp_pg', (req, res) => {
  if (!/^1$|^2$/.test(req.params.prd_type) && !/^[0-9]*$/.test(req.params.grp_pg)) {
    return res.status(500).json({
      error: "Invalid Parameters!"
    });
  }

  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let limit = 5,
          offset = (parseInt(req.params.grp_pg) - 1) * limit;
      let ld_date = moment("2019-02-01"),
          start_at = ld_date.startOf('d').format('YYYY-MM-DD HH:mm:ss'),
          last_win_start_date = moment().subtract(1, 'd').format('YYYY-MM-DD HH:mm:ss'); // check members in group

      connection.query(`SELECT 
          ref_mem.id,
          ref_mem.full_name,
          ref_mem.user_asn_id
        FROM hierarchy_m as hir 
        join members as mem
        on hir.member_id = mem.id
        join members as ref_mem
        on mem.ref_user_asn_id = ref_mem.user_asn_id
        join user_product_details as usr_prd
        on mem.id = usr_prd.member_id
        where 
          hir.created_at >= '${start_at}' and 
          usr_prd.product_id = ${req.params.prd_type}
        order by hir.created_at
        limit ${limit}
        offset ${offset}`, async function (error, results) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (results.length < 5) {
            connection.release();
            return res.json({
              status: false,
              message: "Invalid Spin!"
            });
          } else {
            let err; // check gourp already spin or not

            let ald_spin = false;
            await new Promise(resolve => {
              connection.query(`SELECT \`group\`
                  FROM ld_winners 
                  where 
                    \`group\`=${req.params.grp_pg} and
                    prd_id=${req.params.prd_type}
                  limit 1`, function (error, result) {
                if (error) {
                  err = error;
                } else {
                  if (result.length > 0) {
                    ald_spin = true;
                  }
                }

                return resolve();
              });
            });

            if (err) {
              connection.release();
              return res.status(500).json({
                err
              });
            } else if (ald_spin) {
              connection.release();
              return res.json({
                status: false,
                message: "This group is already spinned!"
              });
            } // check group member already win in last day


            let mem_ids = _.map(_.uniqBy(results, 'id'), o => {
              return o.id;
            });

            let last_win_mem_ids = [];
            await new Promise(resolve => {
              connection.query(`SELECT member_id
                  FROM ld_winners 
                  where 
                    member_id in(${mem_ids.join(', ')}) and
                    created_at >= '${last_win_start_date}'`, function (error, result) {
                if (error) {
                  err = error;
                } else {
                  if (result.length > 0) {
                    last_win_mem_ids = _.map(result, o => {
                      return o.member_id;
                    });
                  }
                }

                return resolve();
              });
            });

            if (err) {
              connection.release();
              return res.status(500).json({
                err
              });
            } // all validation done select winner


            let mem_get_ls = _.reject(mem_ids, id => {
              return _.indexOf(last_win_mem_ids, id) > -1;
            });

            let sel_mem_ids = mem_get_ls.length > 0 ? mem_get_ls : mem_ids;

            let random_ind = _.random(0, sel_mem_ids.length - 1);

            let winner_mem_id = sel_mem_ids[random_ind];
            connection.query(`insert into ld_winners set ?`, {
              member_id: winner_mem_id,
              group: req.params.grp_pg,
              prd_id: req.params.prd_type
            }, function (error) {
              connection.release();

              if (error) {
                return res.status(500).json({
                  error
                });
              } else {
                res.json({
                  status: true
                });
              }
            });
          }
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/members.js":
/*!********************************!*\
  !*** ./server/apis/members.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const _ = __webpack_require__(/*! lodash */ "lodash");

const moment = __webpack_require__(/*! moment */ "moment");

const {
  DateTime
} = __webpack_require__(/*! luxon */ "luxon");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.get("/", function (req, res) {
  let offset = 0,
      limit = 10,
      search = "",
      filter_qry = "";

  if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
    limit = req.query.limit;
  }

  if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
    offset = (parseInt(req.query.page) - 1) * limit;
  }

  if (req.query.search) {
    search = req.query.search;
  }

  if (/^[0-9]$|^(unpaid|paid)$|^suspend$/.test(req.query.filter)) {
    filter_qry = `${search !== "" ? 'AND' : ''}`;

    if (/^[0-9]$/.test(req.query.filter)) {
      filter_qry += ` u_var.level='${req.query.filter}'`;
    } else if (/^(unpaid|paid)$/.test(req.query.filter)) {
      filter_qry += ` m.is_paid_m='${req.query.filter == 'paid' ? 1 : 0}'`;
    } else if (/^suspend$/.test(req.query.filter)) {
      filter_qry += ` m.active_sts=0`;
    }
  }

  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query(`SELECT COUNT(*) as total_rows 
        FROM members as m
        LEFT JOIN info_var_m as u_var
        ON m.id = u_var.member_id
        LEFT JOIN mem_link_crzb as mem_l_crzb
        ON m.id=mem_l_crzb.member_id
        LEFT JOIN crzb_list as crzb_l
        ON mem_l_crzb.crzb_id=crzb_l.id
        ${search !== '' || filter_qry !== '' ? 'WHERE' : ''}
        ${search !== '' ? '(m.user_asn_id LIKE ? OR m.email LIKE ? OR m.full_name LIKE ? OR crzb_l.name LIKE ?)' : ''}
        ${filter_qry !== '' ? filter_qry : ''}
        `, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results, fields) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let rows_count = results[0].total_rows;

          if (rows_count > 0) {
            connection.query(`SELECT 
                  m.id, 
                  m.user_asn_id, 
                  m.email, 
                  m.full_name,
                  m.active_sts, 
                  m.is_paid_m, 
                  crzb_l.name as crzb_name,
                  u_var.level,
                  COUNT(ur.id) as tot_rcp_up 
                FROM members as m
                LEFT JOIN mem_link_crzb as mem_l_crzb
                ON m.id=mem_l_crzb.member_id
                LEFT JOIN crzb_list as crzb_l
                ON mem_l_crzb.crzb_id=crzb_l.id
                LEFT JOIN user_receipts as ur
                ON m.id=ur.ref_id AND ur.type=0 
                LEFT JOIN info_var_m as u_var
                ON m.id = u_var.member_id
                ${search !== '' || filter_qry !== '' ? 'WHERE' : ''}
                ${search !== '' ? '(m.user_asn_id LIKE ? OR m.email LIKE ? OR m.full_name LIKE ? OR crzb_l.name LIKE ?)' : ''}
                ${filter_qry !== '' ? filter_qry : ''}
                GROUP BY m.id
                ORDER BY m.id DESC
                LIMIT ${limit}
                OFFSET ${offset}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results, fields) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                res.json({
                  data: results,
                  total_rows: rows_count
                });
              }
            });
          } else {
            connection.release();
            res.json({
              data: [],
              total_rows: rows_count
            });
          }
        }
      });
    }
  });
});
router.get('/member_info/:id', function (req, res, next) {
  if (req.decoded.data.type > 0) {
    if (/^[0-9]*$/.test(req.params.id)) {
      db.getConnection(function (err, connection) {
        if (err) {
          res.status(500).json({
            error: err
          });
        } else {
          connection.query(`SELECT 
              m.id,
              m.active_sts,
              m.address,
              m.cnic_num,
              m.contact_num,
              m.dob,
              m.email,
              m.full_name,
              m.ref_user_asn_id,
              m.user_asn_id,
              prd.id as product_id,
              prd.name as product_name,
              u_var.package_act_date,
              u_var.level,
              u_var.wallet,
              u_var.direct_ref_count as direct_ref,
              u_var.in_direct_ref_count as indirect_ref,
              u_bank.bank_name,
              u_bank.branch_code,
              u_bank.account_number,
              u_bank.account_title,
              u_bank.iban_number,
              u_bank.address as bk_address,
              crzb_l.name as crzb_name
            FROM members as m

            LEFT JOIN mem_link_crzb as mem_l_crzb
            ON m.id=mem_l_crzb.member_id
            LEFT JOIN (
              select
                ls_b.id,
                concat(ls_b.name, ", ", ls_z.name, ", ", ls_r.name, ", ", ls_c.name) as name
              from crzb_list as ls_b
                
              join crzb_list as ls_z
              on ls_b.parent_id = ls_z.id
              join crzb_list as ls_r
              on ls_z.parent_id = ls_r.id
              join crzb_list as ls_c
              on ls_r.parent_id = ls_c.id
                
            ) as crzb_l
            ON mem_l_crzb.crzb_id=crzb_l.id
            
            LEFT JOIN user_product_details as u_prd
            ON m.id = u_prd.member_id
            LEFT JOIN products as prd
            ON u_prd.product_id = prd.id
            LEFT JOIN info_var_m as u_var
            ON m.id = u_var.member_id
            LEFT JOIN user_bank_details as u_bank
            ON m.id = u_bank.member_id
            WHERE m.id=?`, req.params.id, function (err, results) {
            connection.release();

            if (err) {
              res.status(500).json({
                error: err
              });
            } else {
              let result = results.length > 0 ? results[0] : {};
              res.json({
                result
              });
            }
          });
        }
      });
    } else {
      res.json({
        status: false,
        message: "Invalid id!"
      });
    }
  } else {
    res.json({
      status: false,
      message: "Permission denied!"
    });
  }
});
router.get("/get_terms_sts", (req, res) => {
  if (req.decoded.data.user_id) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error: err
        });
      } else {
        connection.query("SELECT accept_sts FROM terms_accept WHERE member_id=?", req.decoded.data.user_id, function (error, results) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            let accept_sts = 0;

            if (results.length > 0) {
              accept_sts = results[0].accept_sts;
            }

            res.json({
              accept_sts
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No user found!"
    });
  }
});
router.post("/terms_accept", (req, res) => {
  if (req.decoded.data.user_id) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error: err
        });
      } else {
        connection.query("SELECT accept_sts FROM terms_accept WHERE member_id=?", req.decoded.data.user_id, function (error, results) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let query = 0;

            if (results.length > 0) {
              query = `UPDATE terms_accept SET accept_sts=1 WHERE member_id=${req.decoded.data.user_id}`;
            } else {
              query = `INSERT INTO terms_accept SET member_id=${req.decoded.data.user_id}, accept_sts=1`;
            }

            connection.query(query, function (error, result) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                res.json({
                  status: true
                });
              }
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No user found!"
    });
  }
});
router.get('/get_level_info', function (req, res) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error: err
        });
      } else {
        connection.query("SELECT h_childs_count, direct_ref_count FROM info_var_m WHERE member_id=?", req.decoded.data.user_id, function (error, results) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (results.length > 0) {
              res.json({
                child_count: results[0].h_childs_count,
                direct_ref: results[0].direct_ref_count
              });
            } else {
              res.json({
                status: false,
                message: "You are not active member!"
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No user found!"
    });
  }
});
router.get('/get_direct_ref_c', function (req, res) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error: err
        });
      } else {
        connection.query("SELECT (SUM(direct_ref_count)+SUM(in_direct_ref_count)) as ref_count FROM info_var_m WHERE member_id=?", req.decoded.data.user_id, function (error, results) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (results.length > 0) {
              res.json({
                ref_count: results[0].ref_count
              });
            } else {
              res.json({
                status: false,
                message: "You are not active member!"
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No user found!"
    });
  }
});
router.get('/wallet/:id', function (req, res, next) {
  if (/^[0-9]*$/.test(req.params.id)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let options = {
          sql: `
        SELECT wallet
        FROM info_var_m
        WHERE member_id=?
        `
        };
        connection.query(options, [req.params.id], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            let wallet = 0;

            if (results.length > 0) {
              wallet = results[0].wallet;
            }

            res.json({
              data: wallet
            });
          }
        });
      }
    });
  } else {
    next();
  }
});
router.get('/user_info/:id', function (req, res, next) {
  if (/^[0-9]*$/.test(req.params.id)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let options = {
          sql: `
        SELECT direct_ref_count, in_direct_ref_count, level, package_act_date, wallet
        FROM info_var_m
        WHERE member_id=?
        `
        };
        connection.query(options, [req.params.id], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            let data = {};

            if (results.length > 0) {
              data = results[0];
            }

            res.json({
              data
            });
          }
        });
      }
    });
  } else {
    next();
  }
});
router.get('/get_referrals/:id', function (req, res, next) {
  if (/^[0-9]*$/.test(req.params.id)) {
    let grab_months = {
      1: {},
      2: {},
      3: {},
      4: {},
      5: {},
      6: {},
      7: {},
      8: {},
      9: {},
      10: {},
      11: {},
      12: {}
    };
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          error: err
        });
      } else {
        let throw_error = null;
        let user_asn_id = null;
        let mem_id = req.params.id;
        await new Promise(resolve => {
          connection.query('SELECT id, user_asn_id FROM members WHERE id=?', mem_id, function (error, results) {
            if (error) {
              throw_error = error;
            } else {
              if (results.length > 0 && results[0].user_asn_id !== null) {
                user_asn_id = results[0].user_asn_id;
              }
            }

            resolve();
          });
        });

        if (user_asn_id) {
          let date = moment().set('M', 0).set('Y', moment().get('Y') - 1).endOf('Y');

          for (month in grab_months) {
            await new Promise(resolve => {
              date.add(1, 'month');
              let start = date.clone().startOf('month').format("YYYY-MM-DD HH-mm-ss");
              let end = date.clone().endOf('month').format("YYYY-MM-DD HH-mm-ss");
              connection.query('SELECT COUNT(*) as count FROM members WHERE ref_user_asn_id = ? AND is_paid_m = 1 AND created_at >= ? AND created_at <= ?', [user_asn_id, start, end], function (error, results) {
                if (error) {
                  throw_error = error;
                  resolve();
                } else {
                  grab_months[month]['paid'] = results[0].count;
                  connection.query('SELECT COUNT(*) as count FROM members WHERE ref_user_asn_id = ? AND is_paid_m = 0 AND created_at >= ? AND created_at <= ?', [user_asn_id, start, end], function (error, results) {
                    if (error) {
                      throw_error = error;
                      resolve();
                    } else {
                      grab_months[month]['un_paid'] = results[0].count;
                      resolve();
                    }
                  });
                }
              });
            });

            if (throw_error) {
              break;
            }
          }
        }

        connection.release();

        if (throw_error) {
          res.status(500).json({
            error: throw_error
          });
        } else {
          res.json({
            data: grab_months
          });
        }
      }
    });
  } else {
    next();
  }
});
router.get("/user_id/:id", function (req, res, next) {
  if (/^[0-9]*$/.test(req.params.id)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let options = {
          sql: `
        SELECT user_asn_id
        FROM members
        WHERE id=?
        `
        };
        connection.query(options, [req.params.id], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (results.length > 0 && results[0].user_asn_id !== null) {
              res.json({
                user_asn_id: results[0].user_asn_id
              });
            } else {
              res.status(404).json({
                message: "Not Found Id!"
              });
            }
          }
        });
      }
    });
  } else {
    next();
  }
});
router.get("/:id", function (req, res, next) {
  if (/^[0-9]*$/.test(req.params.id)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let options = {
          sql: `
          SELECT m.*, upd.product_id, crzb_data.*
          FROM members AS m

          LEFT JOIN user_product_details as upd
          ON m.id=upd.member_id
          LEFT JOIN mem_link_crzb as mem_l_crzb
          ON m.id=mem_l_crzb.member_id
          left join (
            select
              ls_b.id as crzb_id,
              concat(ls_b.name, ", ", ls_z.name, ", ", ls_r.name, ", ", ls_c.name) as crzb_name,
              concat(ls_z.name, ", ", ls_r.name, ", ", ls_c.name) as crct_name,
              ls_b.parent_id as crct_id
              from crzb_list as ls_b
                
              join crzb_list as ls_z
              on ls_b.parent_id = ls_z.id
              join crzb_list as ls_r
              on ls_z.parent_id = ls_r.id
              join crzb_list as ls_c
              on ls_r.parent_id = ls_c.id
              
          ) as crzb_data
          on mem_l_crzb.crzb_id = crzb_data.crzb_id

          WHERE m.id=?
        `
        };
        connection.query(options, [req.params.id], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              data: results
            });
          }
        });
      }
    });
  } else {
    next();
  }
});
router.post("/add_referral", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query('SELECT COUNT(*) as count FROM members WHERE id=? AND user_asn_id=?', [req.decoded.data.user_id, req.body.member_data.ref_user_asn_id], function (error, results) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            if (results[0].count > 0) {
              connection.query(`SELECT reg_amount FROM products WHERE id=?`, req.body.ext_data.product_id, async function (error, result) {
                if (error) {
                  err_hdl(error);
                  resolve();
                } else {
                  if (result.length > 0) {
                    let prd_reg_amount = result[0].reg_amount;
                    let throw_error;
                    let promotion_id;
                    await new Promise(promResolve => {
                      let curr_date = moment(DateTime.local().setZone("UTC+5").toString()).format("YYYY-MM-DD HH-mm-ss");
                      connection.query(`SELECT id, disc_percent FROM disc_promotions WHERE prd_id = ${req.body.ext_data.product_id} AND (start_prom_dt<='${curr_date}' AND end_prom_dt>='${curr_date}') limit 1`, function (error, result) {
                        if (error) {
                          throw_error = error;
                          return promResolve();
                        } else {
                          if (result.length > 0) {
                            promotion_id = result[0].id;
                            prd_reg_amount = parseInt(prd_reg_amount) - parseInt(parseInt(prd_reg_amount) * result[0].disc_percent / 100);
                          }

                          return promResolve();
                        }
                      });
                    });

                    if (throw_error) {
                      err_hdl(throw_error);
                      resolve();
                    } // deduct amount from wallet


                    connection.query('SELECT wallet, pending FROM `info_var_m` WHERE member_id=?', req.decoded.data.user_id, function (error, results) {
                      if (error) {
                        err_hdl(error);
                        resolve();
                      } else {
                        let amount_wp = 0;
                        let wallet = 0,
                            pending = 0;

                        if (results.length) {
                          wallet = results[0].wallet ? parseInt(results[0].wallet) : 0;
                          pending = results[0].pending ? parseInt(results[0].pending) : 0;
                        }

                        amount_wp = wallet + pending;

                        if (amount_wp >= prd_reg_amount) {
                          wallet -= prd_reg_amount;

                          if (wallet < 0) {
                            pending -= -wallet;
                            wallet = 0;
                          }

                          let set_w_params = {
                            wallet,
                            pending
                          };
                          connection.query('UPDATE info_var_m SET ? WHERE member_id=?', [set_w_params, req.decoded.data.user_id], function (error, results) {
                            if (error) {
                              err_hdl(error);
                              resolve();
                            } else {
                              // grab last user asn id
                              connection.query('SELECT user_asn_id FROM `members` ORDER BY user_asn_id DESC LIMIT 1', function (error, results) {
                                if (error) {
                                  err_hdl(error);
                                  resolve();
                                } else {
                                  req.body.member_data['is_paid_m'] = 1;
                                  req.body.member_data['active_sts'] = 1; // id increament with last id

                                  let new_inc = (parseInt(results[0].user_asn_id) + 1).toString();
                                  new_inc = new_inc.length < 9 ? ("000000000" + new_inc).substr(-9, 9) : new_inc;
                                  req.body.member_data['user_asn_id'] = new_inc; // insert member for new data

                                  connection.query('INSERT INTO members SET ?', req.body.member_data, async function (error, results) {
                                    if (error) {
                                      err_hdl(error);
                                      resolve();
                                    } else {
                                      let mem_id = results.insertId;

                                      if (promotion_id) {
                                        let throw_error;
                                        await new Promise(promResolve => {
                                          connection.query(`INSERT INTO mem_in_prom SET ?`, {
                                            member_id: mem_id,
                                            disc_prom_id: promotion_id
                                          }, function (error) {
                                            if (error) {
                                              throw_error = error;
                                            }

                                            return promResolve();
                                          });
                                        });

                                        if (throw_error) {
                                          err_hdl(throw_error);
                                          resolve();
                                        }
                                      }

                                      connection.query('INSERT INTO `user_product_details` SET ?', {
                                        product_id: req.body.ext_data.product_id,
                                        member_id: mem_id
                                      }, function (error, results) {
                                        if (error) {
                                          err_hdl(error);
                                          resolve();
                                        } else {
                                          connection.query('INSERT INTO `mem_link_crzb` SET ?', {
                                            member_id: mem_id,
                                            crzb_id: req.body.ext_data.crzb_id,
                                            linked_mem_type: 1
                                          }, async function (error, results) {
                                            if (error) {
                                              err_hdl(error);
                                              resolve();
                                            } else {
                                              connection.query('INSERT INTO `transactions_m` SET ?', {
                                                member_id: req.decoded.data.user_id,
                                                remarks: "Create New Referral Fees Deduct Amount In Your Wallet - User ID " + req.body.member_data['user_asn_id'],
                                                credit: prd_reg_amount
                                              }, function (error, results) {
                                                if (error) {
                                                  err_hdl(error);
                                                  resolve();
                                                } else {
                                                  connection.query('INSERT INTO `notifications` SET ?', {
                                                    from_type: 1,
                                                    to_type: 0,
                                                    from_id: 1,
                                                    to_id: req.decoded.data.user_id,
                                                    message: "Create New Referral Fees Deduct Amount In Your Wallet - User ID " + req.body.member_data['user_asn_id'],
                                                    notify_type: 0
                                                  }, function (error, results) {
                                                    if (error) {
                                                      err_hdl(error);
                                                      resolve();
                                                    } else {
                                                      after_paid_member(connection, mem_id, req.body.member_data['user_asn_id'], function (err) {
                                                        if (err) {
                                                          err_hdl(err);
                                                        }

                                                        resolve();
                                                      });
                                                    }
                                                  });
                                                }
                                              });
                                            }
                                          });
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            }
                          });
                        } else {
                          err_hdl(`You have not Rs. ${prd_reg_amount}/- in your account!`);
                          resolve();
                        }
                      }
                    });
                  } else {
                    err_hdl("Invalid Product Selected!");
                    resolve();
                  }
                }
              });
            } else {
              err_hdl("Invalid User!");
              resolve();
            }
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/mjIdCheck', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('SELECT user_asn_id FROM `members` where binary `user_asn_id`=?', [req.body.id], function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            count: results.length
          });
        }
      });
    }
  });
});
router.post('/add', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query('INSERT INTO `members` SET ?', req.body.member_data, async function (error, results, fields) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            let mem_id = results.insertId;
            let throw_error;
            let curr_date = moment(DateTime.local().setZone("UTC+5").toString()).format("YYYY-MM-DD HH-mm-ss");
            await new Promise(promResolve => {
              connection.query(`SELECT id FROM disc_promotions WHERE prd_id = ${req.body.ext_data.prd_id} AND (start_prom_dt<='${curr_date}' AND end_prom_dt>='${curr_date}') limit 1`, function (error, result) {
                if (error) {
                  throw_error = error;
                  return promResolve();
                } else {
                  if (!result.length) {
                    return promResolve();
                  } else {
                    let prom_id = result[0].id;
                    connection.query(`INSERT INTO mem_in_prom SET ?`, {
                      member_id: mem_id,
                      disc_prom_id: prom_id
                    }, function (error) {
                      if (error) {
                        throw_error = error;
                      }

                      return promResolve();
                    });
                  }
                }
              });
            });

            if (throw_error) {
              err_hdl(throw_error);
              resolve();
            }

            connection.query('INSERT INTO `user_product_details` SET ?', {
              product_id: req.body.ext_data.prd_id,
              member_id: mem_id
            }, function (error, results, fields) {
              if (error) {
                err_hdl(error);
                resolve();
              } else {
                connection.query('INSERT INTO `mem_link_crzb` SET ?', {
                  member_id: mem_id,
                  crzb_id: req.body.ext_data.crzb_id,
                  linked_mem_type: 1
                }, async function (error, results, fields) {
                  if (error) {
                    err_hdl(error);
                    resolve();
                  } else {
                    if (_.get(req.body.member_data, 'is_paid_m', 0) === 1) {
                      after_paid_member(connection, mem_id, req.body.member_data.user_asn_id, function (err) {
                        if (err) {
                          err_hdl(err);
                        }

                        return resolve();
                      });
                    } else {
                      resolve();
                    }
                  }
                });
              }
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/update', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_hdl) {
        connection.query('UPDATE `members` SET ? WHERE id=?', [req.body.member_data, req.body.update_id], function (error, results, fields) {
          if (error) {
            err_hdl(error);
            resolve();
          } else {
            connection.query('SELECT member_id FROM user_product_details WHERE member_id=?', [req.body.update_id], function (error, results, fields) {
              if (error) {
                err_hdl(error);
                resolve();
              } else {
                let query = 'UPDATE `user_product_details` SET ? WHERE member_id=?';
                let params = [{
                  product_id: req.body.ext_data.product_id
                }, req.body.update_id];

                if (results.length < 1) {
                  query = 'INSERT INTO `user_product_details` SET ?';
                  params = [{
                    product_id: req.body.ext_data.product_id,
                    member_id: req.body.update_id
                  }];
                }

                connection.query(query, params, function (error, results, fields) {
                  if (error) {
                    err_hdl(error);
                    resolve();
                  } else {
                    connection.query(`SELECT link_mem.member_id, m.is_paid_m FROM members as m
                      LEFT JOIN mem_link_crzb as link_mem
                      ON m.id = link_mem.member_id
                      WHERE m.id=?`, [req.body.update_id], function (error, results, fields) {
                      if (error) {
                        err_hdl(error);
                        resolve();
                      } else {
                        let query = 'UPDATE `mem_link_crzb` SET ? WHERE member_id=?';
                        let params = [{
                          crzb_id: req.body.ext_data.crzb_id
                        }, req.body.update_id];

                        if (results.length && results[0].member_id == null) {
                          query = 'INSERT INTO `mem_link_crzb` SET ?';
                          params = [{
                            crzb_id: req.body.ext_data.crzb_id,
                            member_id: req.body.update_id,
                            linked_mem_type: results[0].is_paid_m == 0 ? 1 : 0
                          }];
                        }

                        connection.query(query, params, async function (error, results, fields) {
                          if (error) {
                            err_hdl(error);
                            resolve();
                          } else {
                            if (_.get(req.body.member_data, 'is_paid_m', 0) === 1) {
                              after_paid_member(connection, req.body.update_id, req.body.member_data.user_asn_id, function (err) {
                                if (err) {
                                  err_hdl(err);
                                }

                                resolve();
                              });
                            } else {
                              resolve();
                            }
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/pay_user', function (req, res) {
  if (/^[0-9]*$/.test(req.body.id)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let mem_id = req.body.id;
        db_util.connectTrans(connection, function (resolve, err_hdl) {
          connection.query('SELECT user_asn_id FROM `members` ORDER BY user_asn_id DESC LIMIT 1', function (error, result) {
            if (error) {
              err_hdl(error);
              resolve();
            } else {
              let params_set = {
                is_paid_m: 1 // if not find it first id assign

              };

              if (result[0].user_asn_id === null) {
                params_set['user_asn_id'] = '000010001';
              } else {
                // id increament with last id
                let new_inc = (parseInt(result[0].user_asn_id) + 1).toString();
                new_inc = new_inc.length < 9 ? ("000000000" + new_inc).substr(-9, 9) : new_inc;
                params_set['user_asn_id'] = new_inc;
              } // first change status is_paid_m=1 and assign id


              connection.query('UPDATE `members` SET ? WHERE id=?', [params_set, mem_id], function (error, result) {
                if (error) {
                  err_hdl(error);
                  return resolve();
                } else {
                  after_paid_member(connection, mem_id, params_set['user_asn_id'], function (err) {
                    if (err) {
                      err_hdl(err);
                    }

                    return resolve();
                  });
                }
              });
            }
          });
        }, function (error) {
          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              status: true
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
module.exports = router;

async function after_paid_member(connection, mem_id, mem_asn_id, cb) {
  let throw_error = null; // initial product amount member which was registered in it

  let add_to_c_wallet = 0;
  let prd_id = null;
  await new Promise(resolve => {
    connection.query(`SELECT prd.reg_amount, prd.id 
      FROM user_product_details as u_prd_det
      LEFT JOIN products as prd
      ON u_prd_det.product_id = prd.id
      WHERE u_prd_det.member_id=${mem_id}`, function (error, result) {
      if (error) {
        throw_error = error;
        return resolve();
      } else {
        let prd_det = result[0];
        prd_id = prd_det.id;
        add_to_c_wallet = prd_det.reg_amount;
        connection.query(`select 
                disc_promotions.disc_percent
              from mem_in_prom
              join disc_promotions
              on mem_in_prom.disc_prom_id = disc_promotions.id and disc_promotions.prd_id = ${prd_det.id}
              where mem_in_prom.member_id=${mem_id}`, function (error, result) {
          if (error) {
            throw_error = error;
          } else {
            if (result.length > 0) {
              add_to_c_wallet = parseInt(prd_det.reg_amount) - parseInt(parseInt(prd_det.reg_amount) * result[0].disc_percent / 100);
            }
          }

          return resolve();
        });
      }
    });
  });

  if (throw_error) {
    return cb(throw_error);
  } // apply fees - transaction into company wallet


  await new Promise(resolve => {
    connection.query('INSERT INTO `transactions_comp` SET ?', {
      remarks: "Activation Fees From User ID " + mem_asn_id,
      debit: add_to_c_wallet
    }, function (error, results, fields) {
      if (error) {
        throw_error = error;
        return resolve();
      } else {
        connection.query('INSERT INTO `notifications` SET ?', {
          from_type: 0,
          to_type: 1,
          from_id: mem_id,
          to_id: 1,
          message: "Activation Fees From User ID " + mem_asn_id,
          notify_type: 0
        }, function (error, results, fields) {
          if (error) {
            throw_error = error;
          }

          return resolve();
        });
      }
    });
  });

  if (throw_error) {
    return cb(throw_error);
  } // insert member variable


  await new Promise(resolve => {
    connection.query('INSERT INTO `info_var_m` SET ?', {
      member_id: mem_id
    }, async function (error, results, fields) {
      if (error) {
        throw_error = error;
      }

      return resolve();
    });
  });

  if (throw_error) {
    return cb(throw_error);
  } // count and add in hierarchy and assign parent id here


  await new Promise(resolve => {
    connection.query('SELECT COUNT(*) AS total_rows FROM `hierarchy_m`', function (error, results, fields) {
      if (error) {
        throw_error = error;
        return resolve();
      } else {
        let h_params = {
          id: results[0].total_rows + 1,
          member_id: mem_id,
          parent_id: Math.ceil(results[0].total_rows / 4)
        };
        let grab_parent_ids = [h_params.parent_id]; // insert hierarchy tree member

        connection.query('INSERT INTO `hierarchy_m` SET ?', h_params, async function (error, results, fields) {
          if (error) {
            throw_error = error;
            return resolve();
          } else {
            // here is parents level increament
            for (parent_id of grab_parent_ids) {
              let exist_row = true;
              await new Promise(resolve2 => {
                if (parent_id > 0) {
                  // select hierarchy id and grab row
                  connection.query('SELECT member_id, parent_id FROM `hierarchy_m` WHERE id=?', parent_id, function (error, results, fields) {
                    if (error) {
                      throw_error = error;
                      return resolve2();
                    } else {
                      // found the parent id and next parent id grab
                      if (results.length > 0) {
                        if (results[0].parent_id > 0) {
                          grab_parent_ids.push(results[0].parent_id);
                        } // select parent selected info variables


                        connection.query('SELECT * FROM `info_var_m` WHERE member_id=?', results[0].member_id, function (error, results, fields) {
                          if (error) {
                            throw_error = error;
                            return resolve2();
                          } else {
                            let old_level = results[0].level;
                            let i_mem_id = results[0].member_id; // update the info variables

                            let set_info_params = {
                              h_childs_count: parseInt(results[0].h_childs_count) + 1,
                              level: get_level(parseInt(results[0].h_childs_count) + 1)
                            };
                            connection.query('UPDATE `info_var_m` SET ? WHERE member_id=?', [set_info_params, i_mem_id], function (error, results, fields) {
                              if (error) {
                                throw_error = error;
                                return resolve2();
                              } else {
                                if (old_level < set_info_params.level) {
                                  connection.query('INSERT INTO `notifications` SET ?', {
                                    from_type: 1,
                                    to_type: 0,
                                    from_id: 1,
                                    to_id: i_mem_id,
                                    from_txt: 'Admin',
                                    message: "Successfully Upgrade Your Level Up -> " + set_info_params.level,
                                    notify_type: 0
                                  }, function (error, results, fields) {
                                    if (error) {
                                      throw_error = error;
                                    }

                                    return resolve2();
                                  });
                                } else {
                                  return resolve2();
                                }
                              }
                            });
                          }
                        });
                      } else {
                        exist_row = false;
                        return resolve2();
                      }
                    }
                  });
                } else {
                  exist_row = false;
                  return resolve2();
                }
              });
              if (throw_error || exist_row === false) break;
            }

            return resolve();
          }
        });
      }
    });
  });

  if (throw_error) {
    return cb(throw_error);
  } // now finance goes here


  await new Promise(resolve => {
    // select ref user and apply comissions and set wallet and direct or indirect count increament
    connection.query('SELECT user_asn_id, ref_user_asn_id FROM `members` WHERE id=?', mem_id, async function (error, results, fields) {
      if (error) {
        throw_error = error;
        return resolve();
      } else {
        if (results[0].ref_user_asn_id == null) {
          results[0].ref_user_asn_id = '000000022';
          await new Promise(resolve2 => {
            connection.query(`UPDATE members SET ? WHERE id=?`, [{
              ref_user_asn_id: '000000022'
            }, mem_id], function (error, result) {
              if (error) {
                throw_error = error;
              }

              return resolve2();
            });
          });

          if (throw_error) {
            return resolve();
          }
        }

        if (results[0].ref_user_asn_id !== null) {
          let grab_ref_usr_ids = [results[0].ref_user_asn_id];
          let direct_inc = 0;

          for (ref_usr_asn_id of grab_ref_usr_ids) {
            await new Promise(resolve2 => {
              connection.query(`SELECT m.id, m.full_name, m.ref_user_asn_id, m.active_sts, iv.direct_ref_count, iv.in_direct_ref_count, iv.wallet, iv.level
                FROM \`members\` as m
                LEFT JOIN info_var_m as iv
                ON m.id = iv.member_id
                WHERE user_asn_id=?`, ref_usr_asn_id, async function (error, results, fields) {
                if (error) {
                  throw_error = error;
                  return resolve2();
                } else {
                  if (results[0].ref_user_asn_id !== null) {
                    grab_ref_usr_ids.push(results[0].ref_user_asn_id);
                  }

                  if (results[0].active_sts !== 1) {
                    return resolve2();
                  }

                  direct_inc++;
                  let ref_mem_id = results[0].id; // campaign process goes here -- start

                  if (direct_inc <= 9) {
                    await new Promise(resolveCamp => {
                      let curr_date = DateTime.local().setZone("UTC+5").toFormat("yyyy-LL-dd HH:mm:ss");
                      connection.query(`select id from campaigns where start_date <= '${curr_date}' and end_date >= '${curr_date}' limit 1`, function (error, result) {
                        if (error) {
                          throw_error = error;
                          return resolveCamp();
                        } else {
                          if (result.length > 0) {
                            let camp_id = result[0].id;
                            connection.query(`select id, total_ref from mem_in_campaign where member_id=${ref_mem_id} and campaign_id=${camp_id}`, async function (error, result) {
                              if (error) {
                                throw_error = error;
                                return resolveCamp();
                              } else {
                                let mem_in_camp_id,
                                    mem_tot_ref = 1;

                                if (result.length > 0) {
                                  mem_in_camp_id = result[0].id;
                                  mem_tot_ref = parseInt(result[0].total_ref) + 1;
                                  connection.query(`update mem_in_campaign set ? where id=${mem_in_camp_id}`, {
                                    total_ref: mem_tot_ref
                                  }, function (error) {
                                    if (error) {
                                      throw_error = error;
                                    }

                                    return resolveCamp();
                                  });
                                } else {
                                  connection.query(`insert into mem_in_campaign set ?`, {
                                    member_id: ref_mem_id,
                                    campaign_id: camp_id,
                                    total_ref: mem_tot_ref
                                  }, function (error) {
                                    if (error) {
                                      throw_error = error;
                                    }

                                    return resolveCamp();
                                  });
                                }
                              }
                            });
                          } else {
                            return resolveCamp();
                          }
                        }
                      });
                    });

                    if (throw_error) {
                      return resolve2();
                    }
                  } // campaign process goes here -- end


                  let set_param = {};
                  let commission_amount = 0;

                  if (direct_inc === 1) {
                    set_param['direct_ref_count'] = parseInt(results[0].direct_ref_count) + 1;
                    commission_amount = prd_id && prd_id == 2 ? 1500 : 1000;
                  } else if (direct_inc === 2) {
                    set_param['in_direct_ref_count'] = parseInt(results[0].in_direct_ref_count) + 1;
                    commission_amount = 300;
                  } else if (direct_inc <= 9) {
                    set_param['in_direct_ref_count'] = parseInt(results[0].in_direct_ref_count) + 1;
                    commission_amount = 200;
                  } //  else {
                  //   set_param['in_direct_ref_count'] = parseInt(results[0].in_direct_ref_count) + 1
                  // }
                  // Check ref user has 4 direct members or active this account this month


                  if (direct_inc > 1) {
                    await new Promise(in_res => {
                      connection.query(`select 
                            h_m.created_at as join_member,
                            m_info.direct_ref_count as direct_ref
                          from hierarchy_m as h_m 
                          join info_var_m as m_info
                          on h_m.member_id = m_info.member_id
                          where h_m.member_id=${ref_mem_id}`, function (error, result) {
                        if (error) {
                          throw_error = error;
                        } else {
                          if (result.length > 0 && result[0]) {
                            let now = moment(DateTime.local().setZone("UTC+5").toString());
                            let old = moment(result[0].join_member);
                            let dur = Math.floor(moment.duration(now.diff(old)).asMonths());

                            if (dur > 1 && parseInt(result[0].direct_ref) < 4) {
                              commission_amount = 0;
                            }
                          }
                        }

                        return in_res();
                      });
                    });

                    if (throw_error) {
                      return resolve2();
                    }
                  }

                  if (commission_amount > 0) {
                    set_param['wallet'] = parseInt(results[0].wallet) + commission_amount;
                  }

                  if (_.isEmpty(set_param)) {
                    return resolve2();
                  } else {
                    connection.query('UPDATE `info_var_m` SET ? WHERE member_id=?', [set_param, ref_mem_id], function (error, results, fields) {
                      if (error) {
                        throw_error = error;
                        return resolve2();
                      } else {
                        // notify query add direct or indirect
                        connection.query('INSERT INTO `notifications` SET ?', {
                          from_type: 1,
                          to_type: 0,
                          from_id: 1,
                          to_id: ref_mem_id,
                          from_txt: 'Admin',
                          message: `Add Your ${direct_inc === 1 ? 'Direct' : 'In-Direct'} Refferral in Hierarchy - User ID ${mem_asn_id}`,
                          notify_type: 0
                        }, function (error, results, fields) {
                          if (error) {
                            throw_error = error;
                            return resolve2();
                          } else {
                            if (commission_amount > 0) {
                              add_to_c_wallet = add_to_c_wallet - commission_amount; // apply commission - transaction

                              connection.query('INSERT INTO `transactions_m` SET ?', {
                                member_id: ref_mem_id,
                                remarks: "Issued Commission From User ID " + mem_asn_id,
                                debit: commission_amount
                              }, function (error, results, fields) {
                                if (error) {
                                  throw_error = error;
                                  return resolve2();
                                } else {
                                  // notify query issued commission to user
                                  connection.query('INSERT INTO `notifications` SET ?', {
                                    from_type: 1,
                                    to_type: 0,
                                    from_id: 1,
                                    to_id: ref_mem_id,
                                    from_txt: 'Admin',
                                    message: "Issued Commission From User ID " + mem_asn_id + " Amount Rs." + commission_amount + "/-",
                                    notify_type: 0
                                  }, function (error, results, fields) {
                                    if (error) {
                                      throw_error = error;
                                      return resolve2();
                                    } else {
                                      // transaction insert in company
                                      connection.query('INSERT INTO `transactions_comp` SET ?', {
                                        remarks: "Issued Commission To User ID " + ref_usr_asn_id,
                                        credit: commission_amount
                                      }, function (error, results, fields) {
                                        if (error) {
                                          throw_error = error;
                                          return resolve2();
                                        } else {
                                          // notify admin issued commission to user
                                          connection.query('INSERT INTO `notifications` SET ?', {
                                            from_type: 0,
                                            to_type: 1,
                                            from_id: ref_mem_id,
                                            to_id: 1,
                                            message: "Issued Commission To User ID " + ref_usr_asn_id + " Amount Rs." + commission_amount + "/-",
                                            notify_type: 0
                                          }, function (error, results, fields) {
                                            if (error) {
                                              throw_error = error;
                                            }

                                            return resolve2();
                                          });
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            } else {
                              return resolve2();
                            }
                          }
                        });
                      }
                    });
                  }
                }
              });
            });
            if (throw_error) break;
          }

          return resolve();
        } else {
          return resolve();
        }
      }
    });
  });

  if (throw_error) {
    return cb(throw_error);
  } // Country Region Zone and Branch commission goes here


  await new Promise(resolve => {
    // select ref user and apply comissions and set wallet and direct or indirect count increament
    connection.query('SELECT crzb_id FROM mem_link_crzb WHERE member_id=?', mem_id, async function (error, results, fields) {
      if (error) {
        throw_error = error;
        return resolve();
      } else {
        if (results.length > 0 && results[0].crzb_id !== null) {
          let grab_crzb_ids = [results[0].crzb_id];

          for (crzb_id of grab_crzb_ids) {
            await new Promise(resolve2 => {
              connection.query(`SELECT 
                  crzb_l.id, 
                  crzb_l.parent_id, 
                  crzb_l.type as crzb_type,
                  asn_role.id as asn_role_id, 
                  asn_role.member_id,
                  iv_mem.wallet,
                  m.user_asn_id
                FROM crzb_list as crzb_l
                LEFT JOIN assign_roles as asn_role
                ON crzb_l.id = asn_role.crzb_id AND asn_role.role_status=1
                LEFT JOIN info_var_m as iv_mem
                ON asn_role.member_id = iv_mem.member_id
                LEFT JOIN members as m
                ON asn_role.member_id = m.id
                WHERE crzb_l.id=?`, crzb_id, async function (error, results) {
                if (error) {
                  throw_error = error;
                  return resolve2();
                } else {
                  if (results[0].parent_id > 0 && results[0].parent_id !== null) {
                    grab_crzb_ids.push(results[0].parent_id);
                  }

                  let asn_role_id = results[0].asn_role_id;
                  let asn_role_mem_id = results[0].member_id;
                  let asn_role_mem_wallet = parseInt(results[0].wallet);
                  let asn_role_mem_asn_id = results[0].user_asn_id;
                  let crzb_type = results[0].crzb_type;

                  if (asn_role_mem_id !== null) {
                    let set_param = {};
                    let commission_amount = 0;

                    if (crzb_type === 2) {
                      // Zonal commission
                      commission_amount = 300;
                      await new Promise(resolve3 => {
                        let curr_date = moment(DateTime.local().setZone("UTC+5").toString());
                        let start_of_m = curr_date.startOf('m');
                        let end_of_m = curr_date.endOf('m');
                        connection.query(`select 
                              count(id) as tot_sale_m
                            from assign_roles_trans as asn_r_tr 
                            where 
                              asn_r_tr.member_id=${asn_role_mem_id} and 
                              asn_r_tr.crzb_id=${crzb_id} and 
                              (asn_r_tr.created_at >= '${start_of_m}' and asn_r_tr.created_at <= '${end_of_m}')`, async function (error, results, fields) {
                          if (error) {
                            throw_error = error;
                            return resolve3();
                          } else {
                            if (results.length > 0 && results[0].tot_sale_m > 0) {
                              let m_sales = results[0].tot_sale_m;
                              let g_sales = m_sales > 0 ? m_sales - 1 : 0;
                              let rot_sale_max = 30;
                              let even_sales = Math.floor(g_sales / rot_sale_max) % 2; // means after 30sales commission 1300 and after 60sales return to 300

                              commission_amount = even_sales === 1 ? commission_amount + 1000 : commission_amount;
                            }

                            return resolve3();
                          }
                        });
                      });

                      if (throw_error) {
                        return resolve2();
                      }
                    } else if (crzb_type === 1) {
                      // Sales Coordinator/Regional commission
                      commission_amount = 150;
                    } else if (crzb_type === 0) {
                      // Country Manager commission
                      commission_amount = 100;
                    }

                    set_param['wallet'] = asn_role_mem_wallet + commission_amount;
                    add_to_c_wallet = add_to_c_wallet - commission_amount;
                    let crz_names = ['Country', 'Sales Coordinator', 'Zone'];
                    connection.query('UPDATE `info_var_m` SET ? WHERE member_id=?', [set_param, asn_role_mem_id], function (error, results) {
                      if (error) {
                        throw_error = error;
                        return resolve2();
                      } else {
                        // save commission - transaction in assign role transaction
                        connection.query('INSERT INTO `assign_roles_trans` SET ?', {
                          member_id: asn_role_mem_id,
                          crzb_id,
                          asn_role_id,
                          linked_member_id: mem_id,
                          amount: commission_amount
                        }, function (error, results, fields) {
                          if (error) {
                            throw_error = error;
                            return resolve2();
                          } else {
                            // apply commission - transaction
                            connection.query('INSERT INTO `transactions_m` SET ?', {
                              member_id: asn_role_mem_id,
                              remarks: `Issued Commission For ${crz_names[crzb_type]} From User ID ${mem_asn_id}`,
                              debit: commission_amount
                            }, function (error, results, fields) {
                              if (error) {
                                throw_error = error;
                                return resolve2();
                              } else {
                                // notify query issued commission to user
                                connection.query('INSERT INTO `notifications` SET ?', {
                                  from_type: 1,
                                  to_type: 0,
                                  from_id: 1,
                                  to_id: asn_role_mem_id,
                                  from_txt: 'Admin',
                                  message: `Issued Commission For ${crz_names[crzb_type]} From User ID ${mem_asn_id} Amount Rs.${commission_amount}/-`,
                                  notify_type: 0
                                }, function (error, results, fields) {
                                  if (error) {
                                    throw_error = error;
                                    return resolve2();
                                  } else {
                                    // transaction insert in company
                                    connection.query('INSERT INTO `transactions_comp` SET ?', {
                                      remarks: `Issued Commission For ${crz_names[crzb_type]} To User ID ${asn_role_mem_asn_id}`,
                                      credit: commission_amount
                                    }, function (error, results, fields) {
                                      if (error) {
                                        throw_error = error;
                                        return resolve2();
                                      } else {
                                        // notify admin issued commission to user
                                        connection.query('INSERT INTO `notifications` SET ?', {
                                          from_type: 0,
                                          to_type: 1,
                                          from_id: asn_role_mem_id,
                                          to_id: 1,
                                          message: `Issued Commission For ${crz_names[crzb_type]} To User ID ${asn_role_mem_asn_id} Amount Rs.${commission_amount}/-`,
                                          notify_type: 0
                                        }, function (error, results, fields) {
                                          if (error) {
                                            throw_error = error;
                                          }

                                          return resolve2();
                                        });
                                      }
                                    });
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });
                  } else {
                    return resolve2();
                  }
                }
              });
            });
            if (throw_error) break;
          }

          return resolve();
        } else {
          return resolve();
        }
      }
    });
  });

  if (throw_error) {
    return cb(throw_error);
  } // add amount to company wallet after all commission issued


  await new Promise(resolve => {
    connection.query('SELECT wallet FROM company_var WHERE id=1', function (error, results) {
      if (error) {
        throw_error = error;
        return resolve();
      } else {
        let is_add = add_to_c_wallet > 0 ? true : false;
        let g_wallet = parseInt(results[0].wallet) + add_to_c_wallet;
        connection.query('UPDATE company_var SET wallet=? WHERE id=1', g_wallet, function (error, results) {
          if (error) {
            throw_error = error;
            return resolve();
          } else {
            if (add_to_c_wallet > 0 || add_to_c_wallet < 0) {
              // notify admin after pay member and wallet amount add or deduct
              connection.query('INSERT INTO `notifications` SET ?', {
                from_type: 0,
                to_type: 1,
                from_id: mem_id,
                to_id: 1,
                message: `${is_add ? "Add" : "Deduct"} Amount Rs.${add_to_c_wallet}/- From Wallet After Paid Member`,
                notify_type: 0
              }, function (error, results, fields) {
                if (error) {
                  throw_error = error;
                }

                return resolve();
              });
            } else {
              return resolve();
            }
          }
        });
      }
    });
  });

  if (throw_error) {
    return cb(throw_error);
  } // notify paid member after paid


  await new Promise(resolve => {
    connection.query('INSERT INTO `notifications` SET ?', {
      from_type: 1,
      to_type: 0,
      from_id: 1,
      to_id: mem_id,
      from_txt: "Admin",
      message: `Your Payment Has Been Approved.`,
      notify_type: 0
    }, function (error, results, fields) {
      if (error) {
        throw_error = error;
      }

      return resolve();
    });
  });
  return cb(throw_error);
}

function get_level(childs) {
  let level = 0,
      l_rows = 1,
      c_rows = 1;

  while (childs > c_rows) {
    level++;
    l_rows = l_rows * 4;
    c_rows += l_rows;
  }

  return level;
}

/***/ }),

/***/ "./server/apis/moderators.js":
/*!***********************************!*\
  !*** ./server/apis/moderators.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get("/", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query('SELECT `id`,`email`,`full_name`,`active_sts` FROM `moderators`', function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data: results
          });
        }
      });
    }
  });
});
router.get("/:id", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query('SELECT * FROM moderators WHERE id=?', [req.params.id], function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data: results
          });
        }
      });
    }
  });
});
router.post('/add', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('INSERT INTO `moderators` SET ?', req.body, function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/update', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('UPDATE `moderators` SET ? WHERE id=?', [req.body.data, req.body.update_id], function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/nomination.js":
/*!***********************************!*\
  !*** ./server/apis/nomination.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const _ = __webpack_require__(/*! lodash */ "lodash");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 0) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/list', (req, res) => {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query(`SELECT id, name, blood_rel, cnic_numb, contact_numb FROM nominations WHERE member_id=${req.decoded.data.user_id}`, (error, result) => {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            result
          });
        }
      });
    }
  });
});
router.post("/add", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      db_util.connectTrans(connection, async function (resolve, err_cb) {
        let upd_rows = [],
            ins_rows = [];

        _.each(req.body.rows, o => {
          if (o.hasOwnProperty('id')) {
            upd_rows.push({
              id: o.id,
              set: {
                name: o.name,
                blood_rel: o.blood_rel,
                cnic_numb: o.cnic_numb,
                contact_numb: o.contact_numb
              }
            });
          } else {
            ins_rows.push([req.decoded.data.user_id, o.name, o.blood_rel, o.cnic_numb, o.contact_numb]);
          }
        });

        let thr_err;

        if (ins_rows.length > 0) {
          await new Promise(resInner => {
            connection.query(`INSERT INTO nominations (member_id, name, blood_rel, cnic_numb, contact_numb) VALUES ?`, [ins_rows], async function (error, result) {
              if (error) {
                thr_err = error;
              }

              resInner();
            });
          });

          if (thr_err) {
            err_cb(thr_err);
            return resolve();
          }
        }

        if (upd_rows.length > 0) {
          for (let row of upd_rows) {
            await new Promise(resInner => {
              connection.query(`UPDATE nominations SET ? WHERE id=?`, [row.set, row.id], function (error) {
                if (error) {
                  thr_err = error;
                }

                resInner();
              });
            });
            if (thr_err) break;
          }

          if (thr_err) {
            err_cb(thr_err);
            return resolve();
          }
        }

        return resolve();
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/notifications.js":
/*!**************************************!*\
  !*** ./server/apis/notifications.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get('/', function (req, res) {
  if (req.decoded.data.user_id) {
    let type = req.decoded.data.type === 2 || req.decoded.data.type === 1 ? 1 : 0;
    let id = type === 1 ? 1 : req.decoded.data.user_id;
    let offset = 0,
        limit = 10,
        search = "",
        filter_qry = "";

    if (/^5$|^10$|^20$|^50$|^100$/.test(req.query.limit)) {
      limit = req.query.limit;
    }

    if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
      offset = (parseInt(req.query.page) - 1) * limit;
    }

    if (req.query.search) {
      search = req.query.search;
    }

    if (/^all$|^finance$|^rewards$|^general$/.test(req.query.filter)) {
      if (req.query.filter === 'finance') {
        filter_qry = `(
          message LIKE 'add amount%' OR 
          message LIKE 'deduct amount%' OR 
          message LIKE 'Issued Commission%' OR 
          message LIKE '%Withdrawal Amount%' OR 
          message LIKE '%Withdrawal Request%' OR 
          message LIKE '%Funds Transfered%' OR 
          message LIKE 'Funds Received%' OR 
          message LIKE '%New Referral Fees Deduct%'
          )`;
      } else if (req.query.filter === 'rewards') {
        filter_qry = `(
          message LIKE '%Reward%'
          )`;
      } else if (req.query.filter === 'general') {
        filter_qry = `(
          message LIKE 'activation fees%' OR 
          message LIKE 'add your direct refferral%' OR 
          message LIKE 'add your in-direct refferral%' OR 
          message LIKE '%Upgrade Your Level%' OR 
          message LIKE 'Your Payment%' OR 
          message LIKE 'New member%'
          )`;
      }
    }

    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query('SELECT COUNT(*) as total FROM notifications WHERE to_id=? AND to_type=? AND seen=0 AND hide=0', [id, type], function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let un_read = result[0].total;
            connection.query(`
              SELECT COUNT(*) as total_rows 
              FROM notifications 
              WHERE to_id=? AND to_type=? AND hide=0
              ${search !== '' ? ' AND (from_txt LIKE ? OR message LIKE ? OR created_at LIKE ?)' : ''}
              ${filter_qry !== "" ? ' AND ' + filter_qry : ''}
              `, [id, type, '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, result) {
              if (error) {
                connection.release();
                res.status(500).json({
                  error
                });
              } else {
                let total_rows = result[0].total_rows;
                connection.query(`
                  SELECT id, from_id, from_txt, from_type, message as msg, seen as \`read\`, created_at as date, notify_type as type, ref_id 
                  FROM notifications 
                  WHERE to_id=? AND to_type=? AND hide=0
                  ${search !== '' ? ' AND (from_txt LIKE ? OR message LIKE ? OR created_at LIKE ?)' : ''}
                  ${filter_qry !== "" ? ' AND ' + filter_qry : ''}
                  ORDER BY id DESC LIMIT ${limit}
                  OFFSET ${offset}
                  `, [id, type, '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, result) {
                  connection.release();

                  if (error) {
                    res.status(500).json({
                      error
                    });
                  } else {
                    res.json({
                      result,
                      un_read,
                      total_rows
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid User!"
    });
  }
});
router.get('/top_5', function (req, res) {
  if (req.decoded.data.user_id) {
    let type = req.decoded.data.type === 2 || req.decoded.data.type === 1 ? 1 : 0;
    let id = type === 1 ? 1 : req.decoded.data.user_id;
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query('SELECT COUNT(*) as total FROM notifications WHERE to_id=? AND to_type=? AND seen=0 AND hide=0', [id, type], function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let un_read = result[0].total;
            connection.query(`
            SELECT id, from_id, from_txt, from_type, message as msg, seen as \`read\`, created_at as date, notify_type as type, ref_id 
            FROM notifications 
            WHERE to_id=? AND to_type=? AND hide=0
            ORDER BY id DESC LIMIT 5
            `, [id, type], function (error, result) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                res.json({
                  result,
                  un_read
                });
              }
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid User!"
    });
  }
});
router.get('/member_info/:id', function (req, res) {
  if (/^[0-9]*$/.test(req.params.id)) {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`
        SELECT is_paid_m as paid, full_name as name, email, contact_num as cont_num
        FROM members 
        WHERE id=?
        `, req.params.id, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              data: result[0]
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get('/cm_info/:trans_id', function (req, res) {
  if (/^[0-9]*$/.test(req.params.trans_id)) {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`
        SELECT status
        FROM commissions 
        WHERE trans_id=?
        `, req.params.trans_id, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              data: result[0]
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get("/claim_info/:clm_id", function (req, res) {
  if (/^[0-9]*$/.test(req.params.clm_id)) {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`
          SELECT reward_selected, type, level, status, cancel_reason
          FROM claim_rewards 
          WHERE id=?
          `, req.params.clm_id, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              data: result[0]
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.post('/read_it', function (req, res) {
  if (/^[0-9]*$/.test(req.body.id)) {
    let seen_sts = 1;

    if (/^[0|1]$/.test(req.body.sts)) {
      seen_sts = req.body.sts;
    }

    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`
        UPDATE notifications
        SET ?
        WHERE id=?
        `, [{
          seen: seen_sts
        }, req.body.id], function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              status: true
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.post("/multi_rd", function (req, res) {
  if (req.body.id !== "" && /^[0|1]$/.test(req.body.read_sts)) {
    let ids = req.body.id.split("|");
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let throw_error = null;

        for (id of ids) {
          await new Promise(resolve => {
            connection.query(`
            UPDATE notifications
            SET ?
            WHERE id=?
            `, [{
              seen: req.body.read_sts
            }, id], function (error, result) {
              if (error) {
                throw_error = error;
              }

              resolve();
            });
          });
          if (throw_error) break;
        }

        connection.release();

        if (throw_error) {
          res.status(500).json({
            error: throw_error
          });
        } else {
          res.json({
            status: true
          });
        }
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.post("/user_remove", function (req, res) {
  if (req.body.id !== "") {
    let ids = req.body.id.split("|");
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let throw_error = null;

        for (id of ids) {
          await new Promise(resolve => {
            connection.query(`
            UPDATE notifications
            SET ?
            WHERE id=?
            `, [{
              hide: 1
            }, id], function (error, result) {
              if (error) {
                throw_error = error;
              }

              resolve();
            });
          });
          if (throw_error) break;
        }

        connection.release();

        if (throw_error) {
          res.status(500).json({
            error: throw_error
          });
        } else {
          res.json({
            status: true
          });
        }
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
module.exports = router;

/***/ }),

/***/ "./server/apis/partners.js":
/*!*********************************!*\
  !*** ./server/apis/partners.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const fs = __webpack_require__(/*! fs */ "fs");

const multer = __webpack_require__(/*! multer */ "multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/../uploads/partners_logo');
  },
  filename: function (req, file, cb) {
    let newFile = Date.now() + typeGet(file.mimetype);
    req.body.logo = newFile;
    cb(null, newFile);
  }
});
const upload = multer({
  storage
});

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get('/list', function (req, res) {
  if (req.decoded.data.type === 2) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let offset = 0,
            limit = 10,
            search = "";

        if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
          limit = req.query.limit;
        }

        if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
          offset = (parseInt(req.query.page) - 1) * limit;
        }

        if (req.query.search) {
          search = req.query.search;
        }

        connection.query(`SELECT COUNT(*) as total_rows 
                    FROM partners
                    ${search !== '' ? 'WHERE' : ''}
                    ${search !== '' ? '(id LIKE ? OR email LIKE ? OR full_name LIKE ? OR city LIKE ? OR created_at LIKE ?)' : ''}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results, fields) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let rows_count = results[0].total_rows;
            connection.query(`SELECT id, email, full_name, city, created_at, active FROM \`partners\`
                                ${search !== '' ? 'WHERE' : ''}
                                ${search !== '' ? '(id LIKE ? OR email LIKE ? OR full_name LIKE ? OR city LIKE ? OR created_at LIKE ?)' : ''}
                                ORDER BY id DESC
                                LIMIT ${limit}
                                OFFSET ${offset}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results, fields) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                res.json({
                  data: results,
                  total_rows: rows_count
                });
              }
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
router.get('/full_info/:id', function (req, res) {
  if (req.decoded.data.type === 2 && /^[0-9]*$/.test(req.params.id)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query('SELECT * FROM `partners` WHERE `id`=?', [req.params.id], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              result: results[0]
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
router.get('/logo/:file_name', function (req, res) {
  if (req.decoded.data.type === 2 && req.params.file_name !== '') {
    if (fs.existsSync(__dirname + "/../uploads/partners_logo/" + req.params.file_name)) {
      not_found = false;
      let file = fs.readFileSync(__dirname + "/../uploads/partners_logo/" + req.params.file_name);
      return res.send(file);
    } else {
      res.status(404).json({
        message: 'Not found!'
      });
    }
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
router.post("/check_email", function (req, res) {
  if (req.decoded.data.type === 2) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query('SELECT email FROM `partners` where binary `email`=?', [req.body.email], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              count: results.length
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
router.post('/add', upload.single('logo'), function (req, res) {
  if (req.decoded.data.type === 2) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query('INSERT INTO partners SET ?', req.body, function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              status: true
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
router.post('/update', upload.single('logo'), function (req, res) {
  if (req.decoded.data.type === 2) {
    const updated_id = req.body.update_id;
    delete req.body.update_id;

    if (!req.body.logo && req.body.logo_remove != 'null') {
      if (fs.existsSync(__dirname + "/../uploads/partners_logo/" + req.body.logo_remove)) {
        fs.unlinkSync(__dirname + "/../uploads/partners_logo/" + req.body.logo_remove);
      }

      req.body.logo = null;
      delete req.body.logo_remove;
    } else if (req.body.logo && req.body.logo_remove) {
      if (fs.existsSync(__dirname + "/../uploads/partners_logo/" + req.body.logo_remove)) {
        fs.unlinkSync(__dirname + "/../uploads/partners_logo/" + req.body.logo_remove);
      }

      delete req.body.logo_remove;
    }

    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query('UPDATE partners SET ? WHERE id=?', [req.body, updated_id], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              status: true
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
router.post('/active_change', function (req, res) {
  if (req.decoded.data.type === 2) {
    const updated_id = req.body.update_id;
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query('UPDATE partners SET ? WHERE id=?', [{
          active: req.body.sts
        }, updated_id], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              status: true
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
module.exports = router;

function typeGet(mimetype) {
  let type = "";

  if (mimetype === "image/png") {
    type = ".png";
  }

  if (mimetype === "image/jpeg") {
    type = ".jpg";
  }

  return type;
}
/* WEBPACK VAR INJECTION */}.call(this, "server\\apis"))

/***/ }),

/***/ "./server/apis/products.js":
/*!*********************************!*\
  !*** ./server/apis/products.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get("/", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('SELECT * FROM `products`', function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            data: results
          });
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/profiles.js":
/*!*********************************!*\
  !*** ./server/apis/profiles.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const fs = __webpack_require__(/*! fs */ "fs");

const jwt = __webpack_require__(/*! jsonwebtoken */ "jsonwebtoken");

const config = __webpack_require__(/*! ../config */ "./server/config.js");

const trans_email = __webpack_require__(/*! ../e-conf.js */ "./server/e-conf.js");

const multer = __webpack_require__(/*! multer */ "multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/../uploads/profile');
  },
  filename: function (req, file, cb) {
    let newFile = Date.now() + typeGet(file.mimetype);
    req.body.file_name = newFile;
    cb(null, newFile);
  }
});
const upload = multer({
  storage
});

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_utils = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.get("/name", function (req, res) {
  if (req.decoded.data.user_id) {
    let query = '';

    if (req.decoded.data.type === 0) {
      query = "SELECT full_name FROM members WHERE id=?";
    } else if (req.decoded.data.type === 1) {
      query = "SELECT full_name FROM moderators WHERE id=?";
    } else {
      query = "SELECT full_name FROM admins WHERE id=?";
    }

    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(query, req.decoded.data.user_id, function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              name: results[0].full_name
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Found!"
    });
  }
});
router.get("/file/:id", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      let opt = {
        sql: `SELECT * FROM u_images WHERE user_id=? AND user_type=?`
      };
      connection.query(opt, [req.decoded.data.user_id, req.decoded.data.type], function (error, results, fields) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          let not_found = true;

          if (results.length > 0) {
            if (fs.existsSync(__dirname + "/../uploads/profile/" + results[0].file_name)) {
              not_found = false;
              let file = fs.readFileSync(__dirname + "/../uploads/profile/" + results[0].file_name);
              return res.send(file);
            }
          }

          if (not_found) {
            res.status(404).json({
              message: 'Not found!'
            });
          }
        }
      });
    }
  });
});
router.get("/", function (req, res) {
  if (req.decoded.data.user_id) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error
        });
      } else {
        let query = '';

        if (req.decoded.data.type === 0) {
          query = `
            SELECT 
              m.user_asn_id, 
              m.email, 
              m.full_name, 
              m.contact_num, 
              m.cnic_num, 
              m.dob, 
              m.address, 
              m.ref_user_asn_id, 
              m.active_sts,
              crzb_data.*
            FROM members as m
            LEFT JOIN mem_link_crzb as mem_l_crzb
            ON m.id=mem_l_crzb.member_id
            
            left join (
              select
                ls_b.id as crzb_id,
                concat(ls_b.name, ", ", ls_z.name, ", ", ls_r.name, ", ", ls_c.name) as crzb_name,
                concat(ls_z.name, ", ", ls_r.name, ", ", ls_c.name) as crct_name,
                ls_b.parent_id as crct_id
                from crzb_list as ls_b
                  
                join crzb_list as ls_z
                on ls_b.parent_id = ls_z.id
                join crzb_list as ls_r
                on ls_z.parent_id = ls_r.id
                join crzb_list as ls_c
                on ls_r.parent_id = ls_c.id
                
            ) as crzb_data
            on mem_l_crzb.crzb_id = crzb_data.crzb_id
            
            WHERE m.id=?`;
        } else if (req.decoded.data.type === 1) {
          query = "SELECT email, full_name, contact_num, cnic_num, address, active_sts FROM moderators WHERE id=?";
        } else {
          query = "SELECT email, full_name, contact_num, cnic_num, address FROM admins WHERE id=?";
        }

        connection.query(query, req.decoded.data.user_id, function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              data: results[0]
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Found!"
    });
  }
});
router.get("/get_comp_prg", function (req, res) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT m.full_name, m.dob, m.cnic_num, m.email, m.contact_num, m.address, m.user_asn_id, m_bk.bank_name, m_bk.branch_code, m_bk.account_title, m_bk.account_number
                    FROM members as m
                    LEFT JOIN user_bank_details as m_bk
                    ON m.id=m_bk.member_id
                    WHERE m.id=?`, req.decoded.data.user_id, function (err, result) {
          connection.release();

          if (err) {
            res.status(500).json({
              err
            });
          } else {
            if (result.length > 0) {
              let com_fields = 0;

              for (field in result[0]) {
                if (result[0][field] !== null && result[0][field] !== "") {
                  com_fields++;
                }
              }

              let progress = Math.round(com_fields / Object.keys(result[0]).length * 100);
              res.json({
                progress
              });
            } else {
              res.json({
                status: false,
                message: "No user found!"
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Data!"
    });
  }
});
router.get("/load_fin_var", (req, res) => {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`select wallet, pending, available, paid_tax from info_var_m where member_id=${req.decoded.data.user_id}`, (error, result) => {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              result: result[0]
            });
          }
        });
      }
    });
  } else {
    res.status(500).json({
      error: "Invalid Request!"
    });
  }
});
router.get("/may_i_wallet_req", function (req, res) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let query = `SELECT m.*, m_bk.bank_name, m_bk.branch_code, m_bk.account_title, m_bk.account_number
                FROM members as m
                LEFT JOIN user_bank_details as m_bk
                ON m.id=m_bk.member_id
                WHERE m.id=?`;
        connection.query(query, req.decoded.data.user_id, function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            let profile_comp = []; // member detail

            if (results[0].active_sts !== 1) {
              profile_comp.push({
                message: "Your account has been suspended."
              });
            }

            if (results[0].address === "" || results[0].address === null) {
              profile_comp.push({
                message: "Provide address."
              });
            }

            if (results[0].cnic_num === "" || results[0].cnic_num === null) {
              profile_comp.push({
                message: "Provide CNIC."
              });
            }

            if (results[0].contact_num === "" || results[0].contact_num === null) {
              profile_comp.push({
                message: "Provide contact number."
              });
            }

            if (results[0].email === "" || results[0].email === null) {
              profile_comp.push({
                message: "Provide email address."
              });
            }

            if (results[0].full_name === "" || results[0].full_name === null) {
              profile_comp.push({
                message: "Provide full name."
              });
            }

            if (results[0].is_paid_m !== 1) {
              profile_comp.push({
                message: "You are unpaid member."
              });
            } //bank detail


            if (results[0].bank_name === "" || results[0].bank_name === null) {
              profile_comp.push({
                message: "Provide bank name."
              });
            }

            if (results[0].branch_code === "" || results[0].branch_code === null) {
              profile_comp.push({
                message: "Provide bank branch code."
              });
            }

            if (results[0].account_title === "" || results[0].account_title === null) {
              profile_comp.push({
                message: "Provide bank account title."
              });
            }

            if (results[0].account_number === "" || results[0].account_number === null) {
              profile_comp.push({
                message: "Provide bank account number."
              });
            } // here is response


            if (profile_comp.length > 0) {
              res.json({
                status: false,
                errors: profile_comp
              });
            } else {
              res.json({
                status: true
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Data!"
    });
  }
});
router.get("/get_prd_detail", function (req, res) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT p_d.product_id, p.name, p.reg_amount, iv_m.package_act_date, p_d.created_at as sel_prd_date
            FROM user_product_details as p_d
            LEFT JOIN products as p
            ON p_d.product_id = p.id
            LEFT JOIN info_var_m as iv_m
            ON p_d.member_id = iv_m.member_id
            WHERE p_d.member_id=?`, req.decoded.data.user_id, function (err, result) {
          connection.release();

          if (err) {
            res.status(500).json({
              err
            });
          } else {
            if (result.length > 0) {
              res.json({
                data: result[0]
              });
            } else {
              res.json({
                status: false,
                message: "No user data found!"
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Data!"
    });
  }
});
router.post("/update", function (req, res) {
  if (req.decoded.data.user_id) {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          error
        });
      } else {
        let str_err = null;
        db_utils.connectTrans(connection, async function (resolve, err_hdl) {
          let query = '',
              params = {
            "address": req.body.data.address,
            "cnic_num": req.body.data.cnic_num,
            "contact_num": req.body.data.cont_num,
            "email": req.body.data.email,
            "full_name": req.body.data.full_name,
            "password": req.body.data.password
          },
              secure_form = {},
              send_email = null;

          if (req.decoded.data.type === 0) {
            query = "UPDATE members SET ? WHERE id=?";
            params["dob"] = req.body.data.dob;
            delete params['password']; // if any pincode and verified pin than this work

            if (req.body.data.secure === true) {
              delete params['contact_num'];
              delete params['email'];
            }

            let throw_error = null;
            await new Promise(inner_resolve => {
              connection.query(`SELECT is_paid_m, email, contact_num FROM members WHERE id=?`, req.decoded.data.user_id, function (error, result) {
                if (error) {
                  throw_error = error;
                  return inner_resolve();
                } else {
                  if (req.body.data.secure === true) {
                    if (req.body.data.cont_num !== result[0].contact_num) {
                      secure_form['contact_num'] = req.body.data.cont_num;
                    }

                    if (req.body.data.email !== result[0].email) {
                      secure_form['email'] = req.body.data.email;
                    }

                    if (result[0].email && result[0].email !== null && result[0].email !== '') {
                      send_email = result[0].email;
                    }
                  }

                  if (req.body.data.secure !== true && result[0].email !== params['email']) {
                    params["email_v_sts"] = 0;
                  }

                  if (result[0].is_paid_m === 0) {
                    params["ref_user_asn_id"] = req.body.data.ref_code;
                  }

                  return inner_resolve();
                }
              });
            });

            if (throw_error) {
              err_hdl(throw_error);
              resolve();
            }
          } else if (req.decoded.data.type === 1) {
            query = "UPDATE moderators SET ? WHERE id=?";
          } else {
            query = "UPDATE admins SET ? WHERE id=?";
          }

          connection.query(query, [params, req.decoded.data.user_id], async function (error, results, fields) {
            if (error) {
              err_hdl(error);
              resolve();
            } else {
              if (req.decoded.data.type === 0) {
                let throw_error = null;
                await new Promise(inner_resolve => {
                  connection.query(`SELECT mem_lk.member_id, m.is_paid_m
                      FROM mem_link_crzb as mem_lk
                      right join members as m
                      on mem_lk.member_id = m.id
                      WHERE m.id=?`, req.decoded.data.user_id, function (error, results, fields) {
                    if (error) {
                      throw_error = error;
                      inner_resolve();
                    } else {
                      let query = 'UPDATE `mem_link_crzb` SET ? WHERE member_id=?';
                      let params = [{
                        crzb_id: req.body.ext_data.crzb_id
                      }, req.decoded.data.user_id];

                      if (results.length && results[0].member_id == null) {
                        query = 'INSERT INTO `mem_link_crzb` SET ?';
                        params = [{
                          crzb_id: req.body.ext_data.crzb_id,
                          member_id: req.decoded.data.user_id,
                          linked_mem_type: results[0].is_paid_m == 0 ? 1 : 0
                        }];
                      }

                      connection.query(query, params, function (error, results, fields) {
                        if (error) {
                          throw_error = error;
                        }

                        inner_resolve();
                      });
                    }
                  });
                });

                if (throw_error) {
                  err_hdl(throw_error);
                  resolve();
                }
              }

              if (req.body.data.secure === true && send_email !== null) {
                let token = jwt.sign({
                  data: {
                    user_id: req.decoded.data.user_id,
                    form_data: secure_form,
                    type: 3
                  }
                }, config.secret, {
                  expiresIn: "1 day"
                });
                connection.query(`INSERT INTO tokens SET ?`, {
                  type: 3,
                  member_id: req.decoded.data.user_id,
                  token: token
                }, function (error, results) {
                  if (error) {
                    err_hdl(error);
                    resolve();
                  } else {
                    res.render("verify-token", {
                      host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                      name: "Member",
                      token: token
                    }, function (errPug, html) {
                      if (errPug) {
                        str_err = "Error render in pug file!";
                        err_hdl(true);
                        resolve();
                      } else {
                        trans_email.sendMail({
                          from: '"MJ Supreme" <info@mj-supreme.com>',
                          to: send_email,
                          subject: 'Verification Token',
                          html: html
                        }, function (err, info) {
                          if (err) {
                            str_err = "Error in sending an email!";
                            err_hdl(true);
                            resolve();
                          } else {
                            resolve();
                          }
                        });
                      }
                    });
                  }
                });
              } else {
                resolve();
              }
            }
          });
        }, function (error) {
          if (str_err) {
            res.status(500).json({
              message: str_err
            });
          } else if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              status: true
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Found!"
    });
  }
});
router.post("/image_upload", upload.single('profile'), function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query('SELECT * FROM u_images WHERE user_id=? AND user_type=?', [req.decoded.data.user_id, req.decoded.data.type], function (error, results, fields) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let up_opt = {
            sql: `INSERT INTO u_images SET ?`
          };
          let params = [{
            user_id: req.decoded.data.user_id,
            user_type: req.decoded.data.type,
            file_name: req.body.file_name
          }];

          if (results.length > 0) {
            up_opt = {
              sql: `UPDATE u_images SET ? WHERE id=?`
            };
            params = [{
              file_name: req.body.file_name
            }, results[0].id];
            fs.unlink(__dirname + '/../uploads/profile/' + results[0].file_name, err => {
              if (err) {
                console.log(err);
              }
            });
          }

          connection.query(up_opt, params, function (error, results, fields) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                status: true
              });
            }
          });
        }
      });
    }
  });
});
router.get('/is_pin', function (req, res) {
  if (req.decoded.data.type === 0) {
    let user_id = req.decoded.data.user_id;
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT pin, active_sts, last_pin
                    FROM pincodes
                    WHERE member_id=?`, user_id, function (error, results) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            let is_pin = false,
                is_pin_act = false,
                last_pin = false;

            if (results.length > 0) {
              is_pin = true;

              if (results[0].active_sts === 1) {
                is_pin_act = true;
              }

              if (results[0].last_pin && results[0].last_pin !== null) {
                last_pin = true;
              }
            }

            res.json({
              is_pin,
              is_pin_act,
              last_pin
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid User Type!"
    });
  }
});
router.post('/verify_pin', function (req, res) {
  if (req.decoded.data.type === 0) {
    let user_id = req.decoded.data.user_id;
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT active_sts
                    FROM pincodes
                    WHERE member_id=? AND BINARY pin=?`, [user_id, req.body.pin], function (error, results) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (results.length > 0) {
              if (results[0].active_sts === 1) {
                res.json({
                  status: true
                });
              } else {
                res.json({
                  status: false,
                  message: 'Your pincode is not verify. Verify your pin code using an email!'
                });
              }
            } else {
              res.json({
                status: false,
                message: 'Invalid PinCode!'
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid User Type!"
    });
  }
});
router.post('/add_pincode', function (req, res) {
  if (req.decoded.data.type === 0) {
    let user_id = req.decoded.data.user_id;
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT m.email, pin.pin, m.full_name 
                    FROM members as m
                    LEFT JOIN pincodes as pin
                    ON m.id = pin.member_id
                    WHERE m.id=? AND BINARY m.password=?`, [user_id, req.body.password], function (error, results) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            if (results.length > 0) {
              if (results[0].pin !== null) {
                connection.release();
                res.json({
                  status: false,
                  message: "You have already add pin code!"
                });
              } else {
                if (results[0].email && results[0].email !== null && results[0].email !== '') {
                  let token = null,
                      full_name = results[0].full_name,
                      send_email = results[0].email;
                  db_utils.connectTrans(connection, function (resolve, err_cb) {
                    // this is in query promise handler
                    connection.query(`INSERT INTO pincodes SET ?`, {
                      member_id: user_id,
                      pin: req.body.pin
                    }, function (error, results) {
                      if (error) {
                        err_cb(error);
                        resolve();
                      } else {
                        token = jwt.sign({
                          data: {
                            user_id,
                            new_pin: req.body.pin,
                            type: 2
                          }
                        }, config.secret, {
                          expiresIn: "1 day"
                        });
                        connection.query(`INSERT INTO tokens SET ?`, {
                          type: 2,
                          member_id: user_id,
                          token: token
                        }, function (error, results) {
                          if (error) {
                            err_cb(error);
                          }

                          resolve();
                        });
                      }
                    });
                  }, function (error) {
                    // this is finalize response handler
                    if (error) {
                      res.status(500).json({
                        error
                      });
                    } else {
                      //sending an email in here
                      if (token !== null) {
                        res.render("verify-token", {
                          host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                          name: full_name,
                          token: token
                        }, function (errPug, html) {
                          if (errPug) {
                            res.json({
                              status: false,
                              message: "Error render in pug file!"
                            });
                          } else {
                            trans_email.sendMail({
                              from: '"MJ Supreme" <info@mj-supreme.com>',
                              to: send_email,
                              subject: 'Verification Token',
                              html: html
                            }, function (err, info) {
                              if (err) {
                                res.json({
                                  status: false,
                                  message: "Error in sending an email!"
                                });
                              } else {
                                res.json({
                                  status: true
                                });
                              }
                            });
                          }
                        });
                      } else {
                        res.json({
                          status: false,
                          message: "Token generated error!"
                        });
                      }
                    }
                  });
                } else {
                  connection.release();
                  res.json({
                    status: false,
                    message: "Please first enter your email!"
                  });
                }
              }
            } else {
              connection.release();
              res.json({
                status: false,
                message: "Invalid Password!"
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid User Type!"
    });
  }
});
router.post('/update_pincode', function (req, res) {
  if (req.decoded.data.type === 0) {
    let user_id = req.decoded.data.user_id;
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT pin.id, pin.pin, m.email, m.full_name  
                    FROM members as m
                    LEFT JOIN pincodes as pin
                    ON m.id = pin.member_id
                    WHERE m.id=? AND BINARY m.password=?`, [user_id, req.body.password], function (error, results) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            if (results.length > 0) {
              if (results[0].pin == req.body.old_pin) {
                let token = null,
                    full_name = results[0].full_name,
                    send_email = results[0].email;
                db_utils.connectTrans(connection, function (resolve, err_cb) {
                  // this is in query promise handler
                  connection.query(`UPDATE pincodes SET ? WHERE id=?`, [{
                    pin: req.body.pin,
                    last_pin: results[0].pin,
                    active_sts: 0
                  }, results[0].id], function (error, results) {
                    if (error) {
                      err_cb(error);
                      resolve();
                    } else {
                      token = jwt.sign({
                        data: {
                          user_id,
                          new_pin: req.body.pin,
                          type: 2
                        }
                      }, config.secret, {
                        expiresIn: "1 day"
                      });
                      connection.query(`INSERT INTO tokens SET ?`, {
                        type: 2,
                        member_id: user_id,
                        token: token
                      }, function (error, results) {
                        if (error) {
                          err_cb(error);
                        }

                        resolve();
                      });
                    }
                  });
                }, function (error) {
                  // this is finalize response handler
                  if (error) {
                    res.status(500).json({
                      error
                    });
                  } else {
                    //sending an email in here
                    if (token !== null) {
                      res.render("verify-token", {
                        host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                        name: full_name,
                        token: token
                      }, function (errPug, html) {
                        if (errPug) {
                          res.json({
                            status: false,
                            message: "Error render in pug file!"
                          });
                        } else {
                          trans_email.sendMail({
                            from: '"MJ Supreme" <info@mj-supreme.com>',
                            to: send_email,
                            subject: 'Verification Token',
                            html: html
                          }, function (err, info) {
                            if (err) {
                              res.json({
                                status: false,
                                message: "Error in sending an email!"
                              });
                            } else {
                              res.json({
                                status: true
                              });
                            }
                          });
                        }
                      });
                    } else {
                      res.json({
                        status: false,
                        message: "Token generated error!"
                      });
                    }
                  }
                });
              } else {
                connection.release();
                res.json({
                  status: false,
                  message: "Invalid old pin code!"
                });
              }
            } else {
              connection.release();
              res.json({
                status: false,
                message: "Invalid Password!"
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid User Type!"
    });
  }
});
router.post('/update_password', function (req, res) {
  if (req.decoded.data.user_id) {
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          error
        });
      } else {
        let table = 'members',
            params = {
          "password": req.body.pass
        },
            secure_form = {},
            send_email = null,
            throw_error = null,
            str_error = null;

        if (req.decoded.data.type === 1) {
          table = 'moderators';
        } else if (req.decoded.data.type === 2) {
          table = 'admins';
        }

        await new Promise(resolve => {
          connection.query(`SELECT email FROM ${table} WHERE id=? AND BINARY password=?`, [req.decoded.data.user_id, req.body.cur_pass], async function (error, result) {
            if (error) {
              throw_error = error;
              return resolve();
            } else {
              if (result.length > 0) {
                if (req.body.secure === true) {
                  secure_form['password'] = req.body.pass;

                  if (result[0].email && result[0].email !== null && result[0].email !== '') {
                    send_email = result[0].email;
                    await new Promise(resolve2 => {
                      let token = jwt.sign({
                        data: {
                          user_id: req.decoded.data.user_id,
                          form_data: secure_form,
                          type: 4
                        }
                      }, config.secret, {
                        expiresIn: "1 day"
                      });
                      connection.query(`INSERT INTO tokens SET ?`, {
                        type: 4,
                        member_id: req.decoded.data.user_id,
                        token: token
                      }, function (error, results) {
                        if (error) {
                          throw_error = error;
                          return resolve2();
                        } else {
                          res.render("verify-token", {
                            host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                            name: "Member",
                            token: token
                          }, function (errPug, html) {
                            if (errPug) {
                              str_error = "Error render in pug file!";
                              return resolve2();
                            } else {
                              trans_email.sendMail({
                                from: '"MJ Supreme" <info@mj-supreme.com>',
                                to: send_email,
                                subject: 'Verification Token',
                                html: html
                              }, function (err, info) {
                                if (err) {
                                  throw_error = err;
                                }

                                return resolve2();
                              });
                            }
                          });
                        }
                      });
                    });
                  }

                  return resolve();
                } else {
                  connection.query(`UPDATE ${table} SET ? WHERE id=?`, [params, req.decoded.data.user_id], function (error, result) {
                    if (error) {
                      throw_error = error;
                    }

                    return resolve();
                  });
                }
              } else {
                str_error = "Current password is not match!";
                return resolve();
              }
            }
          });
        });

        if (throw_error) {
          connection.release();
          return res.status(500).json({
            throw_error
          });
        }

        if (str_error) {
          connection.release();
          return res.json({
            status: false,
            message: str_error
          });
        }

        res.json({
          status: true
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Found!"
    });
  }
});
router.get('/get-process-detail', function (req, res) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT 
            m.email, 
            m.full_name, 
            m.contact_num, 
            m.cnic_num, 
            m.dob, 
            m.address, 
            m.is_paid_m, 
            bk.id as bk_id, 
            bk.bank_name, 
            bk.branch_code, 
            bk.account_title, 
            bk.account_number, 
            prd.reg_amount as prd_reg_amount
          FROM members as m
          LEFT JOIN user_bank_details as bk
          ON m.id = bk.member_id
          LEFT JOIN user_product_details as prd_det
          ON m.id = prd_det.member_id
          LEFT JOIN products as prd
          ON prd_det.product_id = prd.id
          WHERE m.id=${req.decoded.data.user_id}`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              result: result[0]
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Found!"
    });
  }
});
module.exports = router;

function typeGet(mimetype) {
  let type = "";

  if (mimetype === "image/png") {
    type = ".png";
  }

  if (mimetype === "image/jpeg") {
    type = ".jpg";
  }

  return type;
}
/* WEBPACK VAR INJECTION */}.call(this, "server\\apis"))

/***/ }),

/***/ "./server/apis/receipts.js":
/*!*********************************!*\
  !*** ./server/apis/receipts.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const fs = __webpack_require__(/*! fs */ "fs");

const multer = __webpack_require__(/*! multer */ "multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/../uploads/receipts');
  },
  filename: function (req, file, cb) {
    let file_id = Date.now();
    let newFile = file_id + typeGet(file.mimetype);
    req.body.file_id = file_id;
    req.body.file_name = newFile;
    cb(null, newFile);
  }
});
const upload = multer({
  storage
});

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get('/get_invoices', function (req, res) {
  if (req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error
        });
      } else {
        let offset = 0,
            limit = 10;

        if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
          limit = req.query.limit;
        }

        if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
          offset = (parseInt(req.query.page) - 1) * limit;
        }

        connection.query(`SELECT COUNT(*) as tot_rows
                    FROM user_receipts
                    WHERE member_id=?`, req.decoded.data.user_id, function (error, results) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let tot_rows = results[0].tot_rows;
            connection.query(`SELECT u_rpt.id as file_id, u_rpt.file_name, u_rpt.created_at as date, u_rpt.ref_id as id, u_rpt.type
                                FROM user_receipts as u_rpt
                                WHERE u_rpt.member_id=?
                                ORDER BY u_rpt.created_at DESC
                                LIMIT ${limit}
                                OFFSET ${offset}`, req.decoded.data.user_id, function (error, results, fields) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                res.json({
                  result: results,
                  tot_rows
                });
              }
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid User!'
    });
  }
});
router.get('/dn_file/:file_id/:type', function (req, res) {
  if (/^[0-9]*$/.test(req.params.file_id) && /^[0|1]$/.test(req.params.type)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error
        });
      } else {
        connection.query(`SELECT file_name FROM user_receipts WHERE id=? AND type=?
                    ${req.decoded.data.type === 0 ? ' AND member_id="' + req.decoded.data.user_id + '"' : ''}`, [req.params.file_id, req.params.type], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            if (results.length > 0) {
              if (fs.existsSync(__dirname + "/../uploads/receipts/" + results[0].file_name)) {
                let file = fs.readFileSync(__dirname + "/../uploads/receipts/" + results[0].file_name);
                res.send(file);
              } else {
                res.status(404).json({
                  message: 'Not found!'
                });
              }
            } else {
              res.status(404).json({
                message: 'Not found!'
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
router.get('/get_list', function (req, res) {
  if (/^[0-9]*$/.test(req.query.ref_id) && /^[0|1]$/.test(req.query.type)) {
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.query('SELECT id, file_name, created_at as date FROM user_receipts WHERE ref_id=? AND type=?', [req.query.ref_id, req.query.type], function (error, results) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              results
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
router.post('/delete', function (req, res) {
  if (/^[0-9]*$/.test(req.body.id) && req.decoded.data.type !== 0) {
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.query('SELECT file_name FROM user_receipts WHERE id=?', req.body.id, function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let file_name = result[0].file_name;
            connection.query('DELETE FROM user_receipts WHERE id=?', req.body.id, function (error, results) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                if (fs.existsSync(__dirname + "/../uploads/receipts/" + file_name)) {
                  fs.unlinkSync(__dirname + "/../uploads/receipts/" + file_name);
                }

                res.json({
                  status: true
                });
              }
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
router.post('/upload', upload.single('receipt'), function (req, res) {
  if (/^[0-9]*$/.test(req.body.mem_id) && /^[0-9]*$/.test(req.body.ref_id) && /^[0|1]$/.test(req.body.type)) {
    let ref_id = req.body.ref_id,
        mem_id = req.body.mem_id,
        type = req.body.type;
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.query('INSERT INTO user_receipts SET ?', {
          id: req.body.file_id,
          member_id: mem_id,
          file_name: req.body.file_name,
          type,
          ref_id
        }, function (error, results) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              status: true
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: 'Invalid parameters!'
    });
  }
});
module.exports = router;

function typeGet(mimetype) {
  let type = "";

  if (mimetype === "image/png") {
    type = ".png";
  }

  if (mimetype === "image/jpeg") {
    type = ".jpg";
  }

  if (mimetype === "application/pdf") {
    type = ".pdf";
  }

  if (mimetype === "application/msword") {
    type = ".doc";
  }

  if (mimetype === "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
    type = ".docx";
  }

  if (mimetype === "application/rtf") {
    type = ".rtf";
  }

  if (mimetype === "text/plain") {
    type = ".txt";
  }

  if (mimetype === "image/photoshop" || mimetype === "image/x-photoshop" || mimetype === "image/psd" || mimetype === "application/photoshop" || mimetype === "application/psd" || mimetype === "zz-application/zz-winassoc-psd") {
    type = ".psd";
  }

  return type;
}
/* WEBPACK VAR INJECTION */}.call(this, "server\\apis"))

/***/ }),

/***/ "./server/apis/reports.js":
/*!********************************!*\
  !*** ./server/apis/reports.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const createCsvWriter = __webpack_require__(/*! csv-writer */ "csv-writer").createObjectCsvWriter;

const _ = __webpack_require__(/*! lodash */ "lodash");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get('/member/:start/:end', function (req, res) {
  let start_at = moment(new Date(req.params.start)).startOf('d').format('YYYY-MM-DD HH:mm:ss'),
      end_at = moment(new Date(req.params.end)).endOf('d').format('YYYY-MM-DD HH:mm:ss');
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      connection.query(`SELECT m.created_at, m.user_asn_id, m.full_name, m.ref_user_asn_id, m.email, m.cnic_num, m.contact_num, m.dob, m.address, get_crzbf_name_type(ifnull(mem_lk_fr.franchise_id, mem_lk_crzb.crzb_id), if(mem_lk_fr.franchise_id is null, 3, 4)) as fr_name, prd.name as prd_name, iv.level, iv.wallet
                FROM members as m
                LEFT JOIN info_var_m as iv
                ON m.id = iv.member_id
                left join user_product_details as u_prd
                on m.id = u_prd.member_id
                left join products as prd
                on u_prd.product_id = prd.id
                left join mem_link_franchise as mem_lk_fr
                on m.id = mem_lk_fr.member_id
                left join mem_link_crzb as mem_lk_crzb
                on m.id = mem_lk_crzb.member_id
                WHERE m.created_at >= '${start_at}' AND m.created_at <= '${end_at}'`, function (err, result) {
        connection.release();

        if (err) {
          res.status(500).json({
            err
          });
        } else {
          let file_name = new Date().getTime() + '.csv';
          const csvWriter = createCsvWriter({
            path: __dirname + '/../uploads/reports/' + file_name,
            header: [{
              id: 'created_at',
              title: 'Date'
            }, {
              id: 'user_asn_id',
              title: 'MJ ID'
            }, {
              id: 'full_name',
              title: 'Client Name'
            }, {
              id: 'ref_user_asn_id',
              title: 'Refferal ID'
            }, {
              id: 'email',
              title: 'Email ID'
            }, {
              id: 'cnic_num',
              title: 'CNIC #'
            }, {
              id: 'contact_num',
              title: 'Contact #'
            }, {
              id: 'dob',
              title: 'D.O.B'
            }, {
              id: 'address',
              title: 'Address'
            }, {
              id: 'fr_name',
              title: 'Franchise Address'
            }, {
              id: 'prd_name',
              title: 'Selected Product'
            }, {
              id: 'level',
              title: 'Level'
            }, {
              id: 'wallet',
              title: 'Wallet'
            }]
          });
          let new_res = [];

          for (row of result) {
            row['created_at'] = moment(new Date(row['created_at'])).format("DD-MMM-YYYY");

            if (row['dob']) {
              row['dob'] = moment(new Date(row['dob'])).format("DD-MMM-YYYY");
            }

            new_res.push(row);
          }

          csvWriter.writeRecords(new_res).then(() => {
            let file = __dirname + "/../uploads/reports/" + file_name;
            res.download(file);
          }).catch(err => {
            res.json({
              status: false,
              message: "Download error."
            });
          });
        }
      });
    }
  });
});
router.get('/finance/:start/:end', function (req, res) {
  let start_at = moment(new Date(req.params.start)).startOf('d').format('YYYY-MM-DD HH:mm:ss'),
      end_at = moment(new Date(req.params.end)).endOf('d').format('YYYY-MM-DD HH:mm:ss');
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let throw_err = null,
          trans_tot_rows = 0,
          coms_tot_rows = 0;
      await new Promise(resolve => {
        connection.query(`SELECT COUNT(*) as tot_rows 
                    FROM transactions_comp 
                    WHERE created_at >= '${start_at}' AND created_at <= '${end_at}'`, function (err, result) {
          if (err) {
            throw_err = err;
          } else {
            trans_tot_rows = result[0].tot_rows;
          }

          return resolve();
        });
      });

      if (throw_err) {
        connection.release();
        return res.status(500).json({
          throw_err
        });
      }

      await new Promise(resolve => {
        connection.query(`SELECT COUNT(*) as tot_rows 
                    FROM commissions 
                    WHERE created_at >= '${start_at}' AND created_at <= '${end_at}'`, function (err, result) {
          if (err) {
            throw_err = err;
          } else {
            coms_tot_rows = result[0].tot_rows;
          }

          return resolve();
        });
      });

      if (throw_err) {
        connection.release();
        return res.status(500).json({
          throw_err
        });
      }

      let max_rows = coms_tot_rows > trans_tot_rows ? coms_tot_rows : trans_tot_rows,
          limit = 200,
          limit_inc = 0,
          loop_ind = 0,
          grab_offsets = [0],
          data = [];

      for (offset of grab_offsets) {
        loop_ind++;
        limit_inc += limit; // next offset grab

        if (max_rows > limit_inc) {
          grab_offsets.push(loop_ind * limit);
        }

        if (offset < coms_tot_rows) {
          await new Promise(resolve => {
            connection.query(`SELECT trans_id as id, remarks, 
                            '0' as debit,
                            (CASE 
                                WHEN status=1 THEN amount
                                WHEN status=2 THEN amount
                                ELSE 0
                            END) as credit,
                            (CASE 
                                WHEN status=1 THEN 'Paid'
                                WHEN status=2 THEN 'Canceled'
                                ELSE 'UnPaid'
                            END) as status,
                            created_at, 'Paid Commission' as p_r
                            FROM commissions 
                            WHERE created_at >= '${start_at}' AND created_at <= '${end_at}'
                            LIMIT ${limit}
                            OFFSET ${offset}`, function (err, result) {
              if (err) {
                throw_err = err;
              } else {
                data.push(result);
              }

              return resolve();
            });
          });
        }

        if (throw_err) {
          break;
        }

        if (offset < trans_tot_rows) {
          await new Promise(resolve => {
            connection.query(`SELECT *, 'Finance Details' as p_r
                            FROM transactions_comp 
                            WHERE created_at >= '${start_at}' AND created_at <= '${end_at}'
                            LIMIT ${limit}
                            OFFSET ${offset}`, function (err, result) {
              if (err) {
                throw_err = err;
              } else {
                data.push(result);
              }

              return resolve();
            });
          });
        }

        if (throw_err) {
          break;
        }
      }

      if (throw_err) {
        connection.release();
        return res.status(500).json({
          throw_err
        });
      } else {
        connection.release();
        data = _.sortBy(_.flatten(data), ['created_at']);
        let tot_cr = 0,
            tot_dr = 0;

        _.map(data, o => {
          tot_cr += parseInt(o.credit);
          tot_dr += parseInt(o.debit);
          o['created_at'] = moment(new Date(o['created_at'])).format("DD-MMM-YYYY hh:mm:ss");
          return o;
        });

        let file_name = new Date().getTime() + '.csv';
        const csvWriter = createCsvWriter({
          path: __dirname + '/../uploads/reports/' + file_name,
          header: [{
            id: 'created_at',
            title: 'Date'
          }, {
            id: 'id',
            title: 'ID'
          }, {
            id: 'p_r',
            title: 'P/R'
          }, {
            id: 'remarks',
            title: 'Description'
          }, {
            id: 'status',
            title: 'Status'
          }, {
            id: 'debit',
            title: 'Debit (DR)'
          }, {
            id: 'credit',
            title: 'Credit (CR)'
          }, {
            id: 'balance',
            title: ''
          }]
        });
        data.push({
          remarks: 'Total',
          debit: tot_dr,
          credit: tot_cr,
          balance: tot_dr - tot_cr
        });
        csvWriter.writeRecords(data).then(() => {
          let file = __dirname + "/../uploads/reports/" + file_name;
          res.download(file);
        }).catch(err => {
          res.json({
            status: false,
            message: "Download error."
          });
        });
      }
    }
  });
});
router.get('/exp_vch_report/:start/:end/:type', function (req, res) {
  if (req.decoded.data.type === 2) {
    const start_at = moment(new Date(req.params.start)).startOf('d').format('YYYY-MM-DD HH:mm:ss'),
          end_at = moment(new Date(req.params.end)).endOf('d').format('YYYY-MM-DD HH:mm:ss'),
          type = req.params.type;
    db.getConnection(async function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        let throw_err = null,
            max_rows = 0;
        await new Promise(resolve => {
          connection.query(`SELECT COUNT(*) as tot_rows 
                            FROM transactions_comp 
                            WHERE (created_at >= '${start_at}' AND created_at <= '${end_at}') AND type=${type}`, function (err, result) {
            if (err) {
              throw_err = err;
            } else {
              max_rows = result[0].tot_rows;
            }

            return resolve();
          });
        });

        if (throw_err) {
          connection.release();
          return res.status(500).json({
            throw_err
          });
        }

        let limit = 200,
            limit_inc = 0,
            loop_ind = 0,
            grab_offsets = [0],
            data = [];

        for (offset of grab_offsets) {
          loop_ind++;
          limit_inc += limit; // next offset grab

          if (max_rows > limit_inc) {
            grab_offsets.push(loop_ind * limit);
          }

          if (offset < max_rows) {
            await new Promise(resolve => {
              connection.query(`SELECT *
                            FROM transactions_comp 
                            WHERE (created_at >= '${start_at}' AND created_at <= '${end_at}') AND type=${type}
                            LIMIT ${limit}
                            OFFSET ${offset}`, function (err, result) {
                if (err) {
                  throw_err = err;
                } else {
                  data.push(result);
                }

                return resolve();
              });
            });
          }

          if (throw_err) {
            break;
          }
        }

        if (throw_err) {
          connection.release();
          return res.status(500).json({
            throw_err
          });
        } else {
          connection.release();
          data = _.sortBy(_.flatten(data), ['created_at']);
          let tot_cr = 0,
              tot_dr = 0;

          _.map(data, o => {
            tot_cr += parseInt(o.credit);
            tot_dr += parseInt(o.debit);
            o['created_at'] = moment(new Date(o['created_at'])).format("DD-MMM-YYYY hh:mm:ss");
            delete o['type'];
            return o;
          });

          let file_name = new Date().getTime() + '.csv';
          const csvWriter = createCsvWriter({
            path: __dirname + '/../uploads/reports/' + file_name,
            header: [{
              id: 'created_at',
              title: 'Date'
            }, {
              id: 'id',
              title: 'ID'
            }, {
              id: 'remarks',
              title: 'Description'
            }, {
              id: 'debit',
              title: 'Debit (DR)'
            }, {
              id: 'credit',
              title: 'Credit (CR)'
            }, {
              id: 'balance',
              title: ''
            }]
          });
          data.push({
            remarks: 'Total',
            debit: tot_dr,
            credit: tot_cr,
            balance: tot_dr - tot_cr
          });
          csvWriter.writeRecords(data).then(() => {
            let file = __dirname + "/../uploads/reports/" + file_name;
            res.download(file);
          }).catch(err => {
            res.json({
              status: false,
              message: "Download error."
            });
          });
        }
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission denied!"
    });
  }
});
module.exports = router;
/* WEBPACK VAR INJECTION */}.call(this, "server\\apis"))

/***/ }),

/***/ "./server/apis/rewards.js":
/*!********************************!*\
  !*** ./server/apis/rewards.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get("/list_req", function (req, res) {
  if (req.decoded.data.type > 0) {
    let offset = 0,
        limit = 10,
        search = "",
        filter_qry = "";

    if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
      limit = req.query.limit;
    }

    if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
      offset = (parseInt(req.query.page) - 1) * limit;
    }

    if (req.query.search) {
      search = req.query.search;
    }

    if (/^(auto)$|^[0-9]$/.test(req.query.filter)) {
      if (/^(auto)$/.test(req.query.filter)) {
        filter_qry = `AND clm_rwds.type=0`;
      } else {
        filter_qry = `AND clm_rwds.level=${req.query.filter}`;
      }
    }

    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT COUNT(*) as total_rows 
                    FROM claim_rewards as clm_rwds
                    LEFT JOIN members as m
                    ON clm_rwds.member_id = m.id
                    WHERE clm_rwds.status=0
                    ${filter_qry !== '' ? filter_qry : ''}
                    ${search !== '' ? 'AND (m.user_asn_id LIKE ? OR m.email LIKE ?)' : ''}
                    `, ['%' + search + '%', '%' + search + '%'], function (error, results, fields) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let rows_count = results[0].total_rows;

            if (rows_count > 0) {
              connection.query(`SELECT
                                    clm_rwds.id,
                                    clm_rwds.type,
                                    clm_rwds.level,
                                    clm_rwds.reward_selected,
                                    m.user_asn_id, 
                                    m.email
                                    FROM claim_rewards as clm_rwds 
                                    LEFT JOIN members as m
                                    ON clm_rwds.member_id=m.id
                                    WHERE clm_rwds.status=0
                                    ${filter_qry !== '' ? filter_qry : ''}
                                    ${search !== '' ? 'AND (m.user_asn_id LIKE ? OR m.email LIKE ?)' : ''}
                                    ORDER BY clm_rwds.id DESC
                                    LIMIT ${limit}
                                    OFFSET ${offset}`, ['%' + search + '%', '%' + search + '%'], function (error, results, fields) {
                connection.release();

                if (error) {
                  res.status(500).json({
                    error
                  });
                } else {
                  res.json({
                    data: results,
                    total_rows: rows_count
                  });
                }
              });
            } else {
              connection.release();
              res.json({
                data: [],
                total_rows: rows_count
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
router.get("/list_comp", function (req, res) {
  if (req.decoded.data.type > 0) {
    let offset = 0,
        limit = 10,
        search = "",
        filter_qry = "";

    if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
      limit = req.query.limit;
    }

    if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
      offset = (parseInt(req.query.page) - 1) * limit;
    }

    if (req.query.search) {
      search = req.query.search;
    }

    if (/^(auto)$|^[0-9]$/.test(req.query.filter)) {
      if (/^(auto)$/.test(req.query.filter)) {
        filter_qry = `AND clm_rwds.type=0`;
      } else {
        filter_qry = `AND clm_rwds.level=${req.query.filter}`;
      }
    }

    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT COUNT(*) as total_rows 
                    FROM claim_rewards as clm_rwds
                    LEFT JOIN members as m
                    ON clm_rwds.member_id = m.id
                    WHERE clm_rwds.status=1
                    ${filter_qry !== '' ? filter_qry : ''}
                    ${search !== '' ? 'AND (m.user_asn_id LIKE ? OR m.email LIKE ?)' : ''}
                    `, ['%' + search + '%', '%' + search + '%'], function (error, results, fields) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let rows_count = results[0].total_rows;

            if (rows_count > 0) {
              connection.query(`SELECT
                                    clm_rwds.id,
                                    clm_rwds.type,
                                    clm_rwds.level,
                                    clm_rwds.reward_selected,
                                    m.user_asn_id, 
                                    m.email
                                    FROM claim_rewards as clm_rwds 
                                    LEFT JOIN members as m
                                    ON clm_rwds.member_id=m.id
                                    WHERE clm_rwds.status=1
                                    ${filter_qry !== '' ? filter_qry : ''}
                                    ${search !== '' ? 'AND (m.user_asn_id LIKE ? OR m.email LIKE ?)' : ''}
                                    ORDER BY clm_rwds.id DESC
                                    LIMIT ${limit}
                                    OFFSET ${offset}`, ['%' + search + '%', '%' + search + '%'], function (error, results, fields) {
                connection.release();

                if (error) {
                  res.status(500).json({
                    error
                  });
                } else {
                  res.json({
                    data: results,
                    total_rows: rows_count
                  });
                }
              });
            } else {
              connection.release();
              res.json({
                data: [],
                total_rows: rows_count
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
router.get("/auto_rwds", function (req, res, next) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT * FROM claim_rewards WHERE member_id=? AND type=0 AND status<>2`, req.decoded.data.user_id, function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              results
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Found!"
    });
  }
});
router.get("/self_rwds", function (req, res, next) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT * FROM claim_rewards WHERE member_id=? AND type=1 AND status<>2`, req.decoded.data.user_id, function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              results
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Found!"
    });
  }
});
router.post('/claim', function (req, res) {
  if (req.decoded.data.user_id && req.decoded.data.type === 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.beginTransaction(async function (err) {
          if (err) {
            connection.release();
            res.status(500).json({
              err
            });
          } else {
            let throw_error = null;
            await new Promise(resolve => {
              let params = {
                member_id: req.decoded.data.user_id,
                level: req.body.level
              };

              if (req.body.is_self && req.body.is_self === 1) {
                params['type'] = 1;
                params['reward_selected'] = 0;
              } else {
                params['reward_selected'] = req.body.reward_selected;
              }

              connection.query(`INSERT INTO claim_rewards SET ?`, params, function (error, results, fields) {
                if (error) {
                  throw_error = error;
                  return resolve();
                } else {
                  let insert_clm_id = results.insertId;
                  connection.query(`SELECT user_asn_id, email FROM members WHERE id=${req.decoded.data.user_id}`, function (error, result) {
                    if (error) {
                      throw_error = error;
                      return resolve();
                    } else {
                      connection.query('INSERT INTO `notifications` SET ?', {
                        from_type: 0,
                        from_txt: result[0].email,
                        to_type: 1,
                        from_id: req.decoded.data.user_id,
                        to_id: 1,
                        message: `${params.type ? 'Self' : 'Auto'} Reward Request From User ID ${result[0].user_asn_id} Reward Level -> ${req.body.level == 0 ? 'You' : req.body.level}`,
                        notify_type: 3,
                        ref_id: insert_clm_id
                      }, function (error, results, fields) {
                        if (error) {
                          throw_error = error;
                        }

                        return resolve();
                      });
                    }
                  });
                }
              });
            });

            if (throw_error) {
              return connection.rollback(function () {
                connection.release();
                res.status(500).json({
                  throw_error
                });
              });
            } else {
              connection.commit(function (err) {
                if (err) {
                  return connection.rollback(function () {
                    connection.release();
                    res.status(500).json({
                      err
                    });
                  });
                } else {
                  connection.release();
                  res.json({
                    status: true
                  });
                }
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "No User Found!"
    });
  }
});
router.post("/sts_change", function (req, res) {
  if (req.decoded.data.type > 0) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.beginTransaction(async function (err) {
          if (err) {
            connection.release();
            res.status(500).json({
              err
            });
          } else {
            let throw_error = null;
            await new Promise(resolve => {
              connection.query('SELECT member_id, level, type FROM claim_rewards WHERE id=?', req.body.clm_id, function (error, results) {
                if (error) {
                  throw_error = error;
                  return resolve();
                } else {
                  let rwd_type = results[0].type;
                  let member_id = results[0].member_id;
                  let notify_msg = `Your ${rwd_type == 1 ? 'Self' : 'Auto'} Reward Request has been ${req.body.sts === 1 ? 'Accepted' : 'Canceled'} Reward Level -> ${results[0].level == 0 ? 'You' : results[0].level}.`,
                      params = {
                    status: req.body.sts
                  };

                  if (req.body.sts === 2) {
                    notify_msg += " Reason is: " + req.body.reason;
                    params['cancel_reason'] = req.body.reason;
                  } else {
                    params['approved_at'] = moment().format('YYYY-MM-DD HH-mm-ss');
                  }

                  connection.query(`UPDATE claim_rewards SET ? WHERE id=?`, [params, req.body.clm_id], function (error, results, fields) {
                    if (error) {
                      throw_error = error;
                      return resolve();
                    } else {
                      connection.query('INSERT INTO `notifications` SET ?', {
                        from_type: 1,
                        from_txt: "Admin",
                        to_type: 0,
                        from_id: 1,
                        to_id: member_id,
                        message: notify_msg,
                        notify_type: 0
                      }, function (error, results, fields) {
                        if (error) {
                          throw_error = error;
                        }

                        return resolve();
                      });
                    }
                  });
                }
              });
            });

            if (throw_error) {
              return connection.rollback(function () {
                connection.release();
                res.status(500).json({
                  throw_error
                });
              });
            } else {
              connection.commit(function (err) {
                if (err) {
                  return connection.rollback(function () {
                    connection.release();
                    res.status(500).json({
                      err
                    });
                  });
                } else {
                  connection.release();
                  res.json({
                    status: true
                  });
                }
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Permission Denied!"
    });
  }
});
module.exports = router;

/***/ }),

/***/ "./server/apis/startup_check.js":
/*!**************************************!*\
  !*** ./server/apis/startup_check.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get('/email-info', function (req, res) {
  if (req.decoded.data.type === 0) {
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        let last_1_day = moment().subtract(1, 'd').startOf('d').format('YYYY-MM-DD HH-mm-ss');
        connection.query(`SELECT m.email_v_sts, t.created_at
                    FROM members as m
                    LEFT JOIN tokens as t
                    ON m.id=t.member_id AND t.created_at>'${last_1_day}'
                    WHERE m.id=${req.decoded.data.user_id}`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              status: true,
              email_v_sts: result[0].email_v_sts,
              last_email: result[0].created_at !== null ? true : false
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Request!"
    });
  }
});
module.exports = router;

/***/ }),

/***/ "./server/apis/transactions.js":
/*!*************************************!*\
  !*** ./server/apis/transactions.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

router.get("/:id", function (req, res, next) {
  if (/^[0-9]*$/.test(req.params.id)) {
    let offset = 0,
        limit = 10,
        search = "";

    if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
      limit = req.query.limit;
    }

    if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
      offset = (parseInt(req.query.page) - 1) * limit;
    }

    if (req.query.search) {
      search = req.query.search;
    }

    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          error
        });
      } else {
        connection.query(`
                SELECT SUM(debit) - SUM(credit) as tot_balance
                FROM transactions_m
                WHERE member_id=? 
                `, [req.params.id], function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            let tot_balance = result[0].tot_balance;
            connection.query(`SELECT COUNT(*) as tot_rows 
                                FROM transactions_m
                                WHERE member_id=? 
                                ${search !== '' ? 'AND (id LIKE ? OR remarks LIKE ? OR debit LIKE ? OR credit LIKE ? OR created_at LIKE ?)' : ''}
                                `, [req.params.id, '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, result) {
              if (error) {
                connection.release();
                res.status(500).json({
                  error
                });
              } else {
                let tot_rows = result[0].tot_rows;
                let opt = {
                  sql: `SELECT trans.id, trans.remarks as description, trans.debit, trans.credit, trans.created_at as date
                                        FROM transactions_m as trans
                                        LEFT JOIN commissions as cm
                                        ON trans.id=cm.trans_id
                                        WHERE trans.member_id=? 
                                        ${search !== '' ? 'AND (trans.id LIKE ? OR trans.remarks LIKE ? OR trans.debit LIKE ? OR trans.credit LIKE ? OR trans.created_at LIKE ?)' : ''}
                                        ORDER BY trans.id DESC
                                        LIMIT ${limit}
                                        OFFSET ${offset}`
                };
                connection.query(opt, [req.params.id, '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, main_result, fields) {
                  if (error) {
                    connection.release();
                    res.status(500).json({
                      error
                    });
                  } else {
                    if (main_result.length > 0) {
                      connection.query(`SELECT SUM(debit) - SUM(credit) as rows_balance, 
                                                            (SELECT SUM(debit) - SUM(credit) FROM transactions_m where member_id=? AND id < ${main_result[main_result.length - 1].id}) as last_balance
                                                        FROM transactions_m
                                                        WHERE member_id=?
                                                        AND (id >= ${main_result[main_result.length - 1].id} AND id <= ${main_result[0].id})`, [req.params.id, req.params.id], function (error, result) {
                        connection.release();

                        if (error) {
                          res.status(500).json({
                            error
                          });
                        } else {
                          let rows_balance = result[0].rows_balance !== null ? result[0].rows_balance : 0;
                          let last_balance = result[0].last_balance !== null ? result[0].last_balance : 0;
                          res.json({
                            data: main_result,
                            tot_rows,
                            tot_balance,
                            last_balance,
                            rows_balance
                          });
                        }
                      });
                    } else {
                      res.json({
                        data: main_result,
                        tot_rows,
                        tot_balance,
                        last_balance: 0,
                        rows_balance: 0
                      });
                    }
                  }
                });
              }
            });
          }
        });
      }
    });
  } else {
    next();
  }
});
module.exports = router;

/***/ }),

/***/ "./server/apis/verify-tokens.js":
/*!**************************************!*\
  !*** ./server/apis/verify-tokens.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const jwt = __webpack_require__(/*! jsonwebtoken */ "jsonwebtoken");

const config = __webpack_require__(/*! ../config */ "./server/config.js");

const _ = __webpack_require__(/*! lodash */ "lodash");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_utils = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.get('/check/:token', function (req, res) {
  if (req.params.token !== "") {
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.query(`SELECT id, type, member_id, token_data, created_at
            FROM tokens
            WHERE binary token=? AND status<1`, req.params.token, function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            if (result.length > 0) {
              let last_1_day = parseInt(moment().subtract(1, 'd').startOf('d').format('x'));
              let last_3_hours = parseInt(moment().subtract(3, 'h').format('x'));
              let result_date = parseInt(moment(result[0].created_at).format('x'));

              if (result[0].type === 0 && result_date > last_1_day) {
                let token_id = result[0].id;
                let mem_id = result[0].member_id;
                db_utils.connectTrans(connection, function (resolve, err_cb) {
                  // this is in query promise handler
                  connection.query(`UPDATE members SET ? WHERE id=${mem_id}`, {
                    email_v_sts: 1
                  }, function (error, result) {
                    if (error) {
                      err_cb(error);
                      resolve();
                    } else {
                      connection.query(`UPDATE tokens SET ? WHERE id=${token_id}`, {
                        status: 1
                      }, function (error, result) {
                        if (error) {
                          err_cb(error);
                        }

                        resolve();
                      });
                    }
                  });
                }, function (error) {
                  // this is finalize response handler
                  if (error) {
                    res.status(500).json({
                      error
                    });
                  } else {
                    res.json({
                      status: true,
                      type: 0
                    });
                  }
                });
              } else if (result[0].type === 1 && result_date > last_3_hours) {
                connection.release();
                res.json({
                  status: true,
                  type: 1
                });
              } else if (result[0].type === 2 && result_date > last_1_day) {
                let token_id = result[0].id;
                let mem_id = result[0].member_id;
                db_utils.connectTrans(connection, function (resolve, err_cb) {
                  // this is in query promise handler
                  connection.query(`UPDATE members SET ? WHERE id=${mem_id}`, {
                    email_v_sts: 1
                  }, function (error, result) {
                    if (error) {
                      err_cb(error);
                      resolve();
                    } else {
                      connection.query(`UPDATE tokens SET ? WHERE id=${token_id}`, {
                        status: 1
                      }, function (error, result) {
                        if (error) {
                          err_cb(error);
                          resolve();
                        } else {
                          connection.query(`UPDATE pincodes SET ? WHERE member_id=${mem_id}`, {
                            active_sts: 1
                          }, function (error, result) {
                            if (error) {
                              err_cb(error);
                            }

                            resolve();
                          });
                        }
                      });
                    }
                  });
                }, function (error) {
                  // this is finalize response handler
                  if (error) {
                    res.status(500).json({
                      error
                    });
                  } else {
                    res.json({
                      status: true,
                      type: 2
                    });
                  }
                });
              } else if ((result[0].type === 3 || result[0].type === 4) && result_date > last_1_day) {
                let token_id = result[0].id,
                    mem_id = result[0].member_id,
                    token_decode_err = null,
                    token_type = null;
                db_utils.connectTrans(connection, function (resolve, err_cb) {
                  // this is in query promise handler
                  connection.query(`UPDATE tokens SET ? WHERE id=${token_id}`, {
                    status: 1
                  }, function (error, result) {
                    if (error) {
                      err_cb(error);
                      resolve();
                    } else {
                      jwt.verify(req.params.token, config.secret, function (err, decoded) {
                        if (err) {
                          token_decode_err = err;
                          resolve();
                        } else {
                          let update_data = {
                            email_v_sts: 1
                          };
                          token_type = decoded.type;
                          update_data = _.merge(update_data, decoded.data.form_data);
                          connection.query(`UPDATE members SET ? WHERE id=${mem_id}`, update_data, function (error, result) {
                            if (error) {
                              err_cb(error);
                              resolve();
                            }

                            resolve();
                          });
                        }
                      });
                    }
                  });
                }, function (error) {
                  // this is finalize response handler
                  if (error) {
                    res.status(500).json({
                      error
                    });
                  } else {
                    if (token_decode_err) {
                      res.json({
                        status: false,
                        type: "Token Error: " + token_decode_err.message
                      });
                    } else {
                      res.json({
                        status: true,
                        type: token_type
                      });
                    }
                  }
                });
              } else if (result[0].type === 5 && result_date > last_1_day) {
                let token_id = result[0].id,
                    mem_id = result[0].member_id,
                    token_data = JSON.parse(result[0].token_data);
                db_utils.connectTrans(connection, function (resolve, err_cb) {
                  // this is in query promise handler
                  connection.query(`UPDATE tokens SET ? WHERE id=${token_id}`, {
                    status: 1
                  }, function (error, result) {
                    if (error) {
                      err_cb(error);
                      resolve();
                    } else {
                      connection.query(`SELECT u_bk.id
                            FROM \`user_bank_details\` as u_bk
                            WHERE u_bk.member_id=${mem_id}`, function (error, result) {
                        if (error) {
                          err_cb(error);
                          resolve();
                        } else {
                          let bk_query = '';
                          let params = [];

                          if (result.length > 0) {
                            bk_query = 'UPDATE user_bank_details SET ? WHERE member_id=?';
                            params = [token_data, mem_id];
                          } else {
                            bk_query = 'INSERT INTO user_bank_details SET ?';
                            token_data['member_id'] = mem_id;
                            params = [token_data];
                          }

                          connection.query(bk_query, params, function (error, results, fields) {
                            if (error) {
                              err_cb(error);
                            }

                            resolve();
                          });
                        }
                      });
                    }
                  });
                }, function (error) {
                  // this is finalize response handler
                  if (error) {
                    res.status(500).json({
                      error
                    });
                  } else {
                    res.json({
                      status: true,
                      type: 5
                    });
                  }
                });
              } else {
                connection.release();
                res.json({
                  status: false,
                  message: "Token Expire!"
                });
              }
            } else {
              connection.release();
              res.json({
                status: false,
                message: "Invalid Token!"
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Request!"
    });
  }
});
router.post('/change-pass', function (req, res) {
  if (req.body.token && req.body.token !== "" && req.body.password && req.body.password !== "") {
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.query(`SELECT id, member_id, created_at
            FROM tokens
            WHERE binary token=? AND status<1 AND type=1`, req.body.token, function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            if (result.length > 0) {
              let last_3_hours = parseInt(moment().subtract(3, 'h').format('x'));
              let result_date = parseInt(moment(result[0].created_at).format('x'));

              if (result_date > last_3_hours) {
                let token_id = result[0].id;
                let mem_id = result[0].member_id;
                let new_pass = req.body.password;
                connection.beginTransaction(async function (error) {
                  if (error) {
                    connection.release();
                    res.status(500).json({
                      error
                    });
                  } else {
                    let throw_error = null;
                    await new Promise(resolve => {
                      connection.query(`UPDATE members SET ? WHERE id=${mem_id}`, {
                        email_v_sts: 1,
                        password: new_pass
                      }, function (error, result) {
                        if (error) {
                          throw_error = error;
                          return resolve();
                        } else {
                          connection.query(`UPDATE tokens SET ? WHERE id=${token_id}`, {
                            status: 1
                          }, function (error, result) {
                            if (error) {
                              throw_error = error;
                            }

                            return resolve();
                          });
                        }
                      });
                    });

                    if (throw_error) {
                      return connection.rollback(function () {
                        connection.release();
                        res.status(500).json({
                          throw_error
                        });
                      });
                    } else {
                      connection.commit(function (err) {
                        if (err) {
                          return connection.rollback(function () {
                            connection.release();
                            res.status(500).json({
                              err
                            });
                          });
                        } else {
                          connection.release();
                          res.json({
                            status: true
                          });
                        }
                      });
                    }
                  }
                });
              } else {
                connection.release();
                res.json({
                  status: false,
                  message: "Token Expired!"
                });
              }
            } else {
              connection.release();
              res.json({
                status: false,
                message: "Invalid Request!"
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get('/unsubscribe/:token', function (req, res) {
  if (req.params.token !== "") {
    db.getConnection(function (error, connection) {
      if (error) {
        res.status(500).json({
          error
        });
      } else {
        connection.query(`SELECT COUNT(*) as \`rows\`
            FROM subscribers
            WHERE binary unsubscribe_id=?`, req.params.token, function (error, result) {
          if (error) {
            connection.release();
            res.status(500).json({
              error
            });
          } else {
            if (result['0'].rows > 0) {
              connection.query(`DELETE FROM subscribers WHERE binary unsubscribe_id=?`, req.params.token, function (error, result) {
                connection.release();

                if (error) {
                  res.status(500).json({
                    error
                  });
                } else {
                  res.json({
                    status: true
                  });
                }
              });
            } else {
              connection.release();
              res.json({
                status: false,
                message: "Invalid Token!"
              });
            }
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Request!"
    });
  }
});
module.exports = router;

/***/ }),

/***/ "./server/apis/vouchers.js":
/*!*********************************!*\
  !*** ./server/apis/vouchers.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const moment = __webpack_require__(/*! moment */ "moment");

const _ = __webpack_require__(/*! lodash */ "lodash");

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const db_util = __webpack_require__(/*! ../func/db-util.js */ "./server/func/db-util.js");

router.use(function (req, res, next) {
  if (req.decoded.data.type === 2) {
    return next();
  } else {
    return res.json({
      status: false,
      message: "Not permission on this request."
    });
  }
});
router.get('/list_voucher', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      let offset = 0,
          limit = 10,
          search = "";

      if (/^10$|^20$|^50$|^100$/.test(req.query.limit)) {
        limit = req.query.limit;
      }

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`SELECT COUNT(*) as tot_rows 
        FROM vouchers
        ${search !== '' ? 'WHERE v_id LIKE ?' : ''}`, ['%' + search + '%'], function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let tot_rows = result[0].tot_rows;
          connection.query(`SELECT id, v_id, v_date 
              FROM vouchers
              ${search !== '' ? 'WHERE v_id LIKE ?' : ''}
              ORDER BY v_id DESC
              LIMIT ${limit}
              OFFSET ${offset}`, ['%' + search + '%'], function (error, result) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: result,
                tot_rows
              });
            }
          });
        }
      });
    }
  });
});
router.get('/load/:id', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      connection.query(`SELECT id, v_id, v_date 
        FROM vouchers
        WHERE id LIKE ?`, [req.params.id], function (error, result) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          if (result.length > 0) {
            let v_data = result[0];
            connection.query(`SELECT id, subs_id, particular, debit, credit 
                FROM v_entries
                WHERE v_id LIKE ?`, [req.params.id], function (error, result) {
              connection.release();

              if (error) {
                res.status(500).json({
                  error
                });
              } else {
                res.json({
                  v_data,
                  v_ent_data: result
                });
              }
            });
          } else {
            res.json({
              status: false,
              message: "Invalid Id!"
            });
          }
        }
      });
    }
  });
});
router.post("/add", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`INSERT INTO vouchers SET ?`, {
          v_date: moment(req.body.v_date).format("YYYY-MM-DD HH-mm-ss")
        }, function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            let v_ins_id = result.insertId;
            let gen_v_id = moment().format('YYYYMMDD');
            gen_v_id = gen_v_id + (v_ins_id.toString().length < 3 ? ("000" + v_ins_id).substr(-3, 3) : v_ins_id);
            connection.query(`UPDATE vouchers SET ? WHERE id=${v_ins_id}`, {
              v_id: gen_v_id
            }, function (error, result) {
              if (error) {
                err_cb(error);
                resolve();
              } else {
                let mapRows = _.map(req.body.rows, o => {
                  return [v_ins_id, o.subs_id, o.particular, o.debit, o.credit];
                });

                connection.query(`INSERT INTO v_entries (v_id, subs_id, particular, debit, credit) VALUES ?`, [mapRows], function (error, result) {
                  if (error) {
                    err_cb(error);
                  }

                  resolve();
                });
              }
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post("/update", function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`UPDATE vouchers SET ? WHERE id=${req.body.upd_id}`, {
          v_date: moment(req.body.v_date).format("YYYY-MM-DD HH-mm-ss"),
          updated_at: moment().format("YYYY-MM-DD HH-mm-ss")
        }, async function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            let thr_err = null;

            for (let upd_row of req.body.rows) {
              if (_.get(upd_row, 'remove', false)) {
                await new Promise(in_res => {
                  connection.query(`DELETE FROM v_entries WHERE id=${upd_row.id}`, function (error, result) {
                    if (error) {
                      thr_err = error;
                    }

                    in_res();
                  });
                });
              } else if (_.get(upd_row, 'id', null)) {
                let upd_db_row = _.cloneDeep(upd_row);

                delete upd_db_row['id'];
                await new Promise(in_res => {
                  connection.query(`UPDATE v_entries SET ? WHERE id=${upd_row.id}`, upd_db_row, function (error, result) {
                    if (error) {
                      thr_err = error;
                    }

                    in_res();
                  });
                });
              } else {
                await new Promise(in_res => {
                  upd_row['v_id'] = req.body.upd_id;
                  connection.query(`INSERT INTO v_entries SET ?`, upd_row, function (error, result) {
                    if (error) {
                      thr_err = error;
                    }

                    in_res();
                  });
                });
              }

              if (thr_err) break;
            }

            if (thr_err) {
              err_cb(thr_err);
            }

            resolve();
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
router.post('/delete', function (req, res) {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        error
      });
    } else {
      db_util.connectTrans(connection, function (resolve, err_cb) {
        connection.query(`DELETE FROM v_entries 
          WHERE v_id='${req.body.del_id}'`, async function (error, result) {
          if (error) {
            err_cb(error);
            resolve();
          } else {
            connection.query(`DELETE FROM vouchers 
                WHERE id='${req.body.del_id}'`, async function (error, result) {
              if (error) {
                err_cb(error);
              }

              resolve();
            });
          }
        });
      }, function (error) {
        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            status: true
          });
        }
      });
    }
  });
});
module.exports = router;

/***/ }),

/***/ "./server/apis/web.js":
/*!****************************!*\
  !*** ./server/apis/web.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {const express = __webpack_require__(/*! express */ "express");

const router = express.Router();

const db = __webpack_require__(/*! ../db.js */ "./server/db.js");

const config = __webpack_require__(/*! ./../config */ "./server/config.js");

const secret = config.secret;

const trans_email = __webpack_require__(/*! ../e-conf.js */ "./server/e-conf.js");

const jwt = __webpack_require__(/*! jsonwebtoken */ "jsonwebtoken");

const _ = __webpack_require__(/*! lodash */ "lodash");

const moment = __webpack_require__(/*! moment */ "moment");

const {
  DateTime
} = __webpack_require__(/*! luxon */ "luxon");

const fs = __webpack_require__(/*! fs */ "fs");

const gm = __webpack_require__(/*! gm */ "gm");

router.get("/pk", function (req, res) {
  let data = JSON.parse(fs.readFileSync(__dirname + '/../files/pk.json', 'utf8'));

  let new_data = _.map(data, o => {
    return o.city;
  });

  new_data.sort();
  res.json({
    cities: new_data
  });
});
router.get('/tot-mem-count', (req, res) => {
  db.getConnection(function (error, connection) {
    if (error) {
      return res.status(500).json({
        error
      });
    } else {
      connection.query(`SELECT COUNT(*) as tot FROM \`members\` WHERE is_paid_m=1`, function (error, result) {
        connection.release();

        if (error) {
          return res.status(500).json({
            error
          });
        } else {
          return res.json({
            mems: result[0].tot
          });
        }
      });
    }
  });
});
router.post('/tokenLogin', (req, res) => {
  const token = req.body.token || req.query.token;

  if (token) {
    jwt.verify(token, secret, function (err, decoded) {
      if (err) {
        return res.json({
          status: false,
          message: err.message
        });
      } else {
        db.getConnection(function (error, connection) {
          if (error) {
            return res.status(500).json({
              error
            });
          } else {
            let table = 'members',
                colm = "`email`, `is_paid_m`";

            if (decoded.data.type === 1) {
              table = 'moderators';
              colm = "`email`";
            } else if (decoded.data.type === 2) {
              table = 'admins';
              colm = "`email`";
            }

            connection.query(`SELECT ${colm} FROM ${table} WHERE id=?`, decoded.data.user_id, function (error, result) {
              connection.release();

              if (error) {
                return res.status(500).json({
                  error
                });
              } else {
                let user = {
                  user_id: decoded.data.user_id,
                  email: result[0].email,
                  type: decoded.data.type
                };

                if (decoded.data.type === 0) {
                  user['is_paid'] = result[0].is_paid_m;
                }

                return res.json({
                  status: true,
                  token: token,
                  user
                });
              }
            });
          }
        });
      }
    });
  } else {
    return res.json({
      status: false,
      message: 'No token provided.'
    });
  }
});
router.use((req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, secret, function (err, decoded) {
      if (err) {
        return res.json({
          status: false,
          message: err.message
        });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.status(403).send({
      status: false,
      message: 'No token provided.'
    });
  }
});
router.get('/ac_crct_ls/:search', (req, res) => {
  if (req.params.search) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`select * from (
            select 
              concat(ls1.name, 
                if(ls2.name is null, "", concat(", ", ls2.name)),
                if(ls3.name is null, "", concat(", ", ls3.name))
              ) as name, ls1.id
              from crzb_list as ls1
              left join crzb_list as ls2
              on ls1.parent_id = ls2.id
              left join crzb_list as ls3
              on ls2.parent_id = ls3.id
              where ls1.type=2 and ls1.active=1
            ) as tbl1
          where tbl1.name like '%${req.params.search}%'
          limit 10`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              result
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get('/ls_branch/:crct_id', (req, res) => {
  if (req.params.crct_id && /^[0-9]*$/.test(req.params.crct_id)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT id, name FROM crzb_list where parent_id=${req.params.crct_id} and active=1;`, function (error, result) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              result
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid Parameters!"
    });
  }
});
router.get('/get_list_winners_auto', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 15,
          date = moment(),
          gen_sofm = date.clone().startOf('month').subtract(_.get(req.query, 'prev_mnth_inc', 0), 'M'),
          startM = gen_sofm.format("YYYY-MM-DD HH-mm-ss"),
          endM = gen_sofm.clone().endOf('month').format("YYYY-MM-DD HH-mm-ss");

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      let throw_error = null;
      let cur_tot = 0;
      let prev_tot = 0;
      await new Promise(resolve => {
        connection.query(`SELECT COUNT(*) as tot_rows
          FROM claim_rewards as clm
          WHERE clm.type=0 AND clm.status=1 AND clm.approved_at < '${startM}'
          `, function (error, result) {
          if (error) {
            throw_error = error;
          } else {
            prev_tot = result[0].tot_rows;
          }

          resolve();
        });
      });

      if (throw_error !== null) {
        connection.release();
        return res.status(500).json({
          error: throw_error
        });
      }

      await new Promise(resolve => {
        connection.query(`SELECT COUNT(*) as tot_rows
          FROM claim_rewards as clm
          WHERE clm.type=0 AND clm.status=1 AND (clm.approved_at >= '${startM}' AND clm.approved_at <= '${endM}')
          `, function (error, result) {
          if (error) {
            throw_error = error;
          } else {
            cur_tot = result[0].tot_rows;
          }

          resolve();
        });
      });

      if (throw_error !== null) {
        connection.release();
        return res.status(500).json({
          error: throw_error
        });
      }

      connection.query(`SELECT clm.reward_selected, clm.level as rwd_level, m.full_name, u_img.file_name, i_var.level
        FROM claim_rewards as clm
        JOIN members as m
        ON clm.member_id=m.id
        JOIN info_var_m as i_var
        ON m.id=i_var.member_id
        LEFT JOIN u_images as u_img
        ON m.id=u_img.user_id AND u_img.user_type=0
        WHERE clm.type=0 AND clm.status=1 AND (clm.approved_at >= '${startM}' AND clm.approved_at <= '${endM}')
        ORDER BY clm.approved_at DESC
        LIMIT ${limit}
        OFFSET ${offset}
        `, function (error, result) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            result,
            cur_tot,
            prev_tot,
            gen_my: gen_sofm.format('MMMM YYYY')
          });
        }
      });
    }
  });
});
router.get('/get_list_winners_self', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 15,
          date = moment(),
          // startM = date.clone().startOf('month').format("YYYY-MM-DD HH-mm-ss"),
      // endM = date.clone().endOf('month').format("YYYY-MM-DD HH-mm-ss")
      gen_sofm = date.clone().startOf('month').subtract(_.get(req.query, 'prev_mnth_inc', 0), 'M'),
          startM = gen_sofm.format("YYYY-MM-DD HH-mm-ss"),
          endM = gen_sofm.clone().endOf('month').format("YYYY-MM-DD HH-mm-ss");

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      let throw_error = null;
      let cur_tot = 0;
      let prev_tot = 0;
      await new Promise(resolve => {
        connection.query(`SELECT COUNT(*) as tot_rows
          FROM claim_rewards as clm
          WHERE clm.type=1 AND clm.status=1 AND clm.approved_at < '${startM}'
          `, function (error, result) {
          if (error) {
            throw_error = error;
          } else {
            prev_tot = result[0].tot_rows;
          }

          resolve();
        });
      });

      if (throw_error !== null) {
        connection.release();
        return res.status(500).json({
          error: throw_error
        });
      }

      await new Promise(resolve => {
        connection.query(`SELECT COUNT(*) as tot_rows
          FROM claim_rewards as clm
          WHERE clm.type=1 AND clm.status=1 AND (clm.approved_at >= '${startM}' AND clm.approved_at <= '${endM}')
          `, function (error, result) {
          if (error) {
            throw_error = error;
          } else {
            cur_tot = result[0].tot_rows;
          }

          resolve();
        });
      });

      if (throw_error !== null) {
        connection.release();
        return res.status(500).json({
          error: throw_error
        });
      }

      connection.query(`SELECT clm.reward_selected, clm.level as rwd_level, m.full_name, u_img.file_name, i_var.level
        FROM claim_rewards as clm
        JOIN members as m
        ON clm.member_id=m.id
        JOIN info_var_m as i_var
        ON m.id=i_var.member_id
        LEFT JOIN u_images as u_img
        ON m.id=u_img.user_id AND u_img.user_type=0
        WHERE clm.type=1 AND clm.status=1 AND (clm.approved_at >= '${startM}' AND clm.approved_at <= '${endM}')
        ORDER BY clm.approved_at DESC
        LIMIT ${limit}
        OFFSET ${offset}
        `, function (error, result) {
        connection.release();

        if (error) {
          res.status(500).json({
            error
          });
        } else {
          res.json({
            result,
            cur_tot,
            prev_tot,
            gen_my: gen_sofm.format('MMMM YYYY')
          });
        }
      });
    }
  });
});
router.get('/get_winners', function (req, res) {
  db.getConnection(async function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let throw_error = null,
          data = {
        auto_rewards: [],
        self_rewards: []
      };
      await new Promise(resolve => {
        connection.query(`SELECT clm.reward_selected, clm.level as rwd_level, m.full_name, u_img.file_name, i_var.level
          FROM claim_rewards as clm
          JOIN members as m
          ON clm.member_id=m.id
          JOIN info_var_m as i_var
          ON m.id=i_var.member_id
          LEFT JOIN u_images as u_img
          ON m.id=u_img.user_id AND u_img.user_type=0
          WHERE clm.type=0 AND clm.status=1
          ORDER BY clm.approved_at DESC
          LIMIT 4
          `, function (error, results, fields) {
          if (error) {
            throw_error = error;
          } else {
            data['auto_rewards'] = results;
          }

          return resolve();
        });
      });

      if (throw_error !== null) {
        connection.release();
        return res.status(500).json({
          err: throw_error
        });
      }

      await new Promise(resolve => {
        connection.query(`SELECT clm.reward_selected, clm.level as rwd_level, m.full_name, u_img.file_name, i_var.level
          FROM claim_rewards as clm
          JOIN members as m
          ON clm.member_id=m.id
          JOIN info_var_m as i_var
          ON m.id=i_var.member_id
          LEFT JOIN u_images as u_img
          ON m.id=u_img.user_id AND u_img.user_type=0
          WHERE clm.type=1 AND clm.status=1
          ORDER BY clm.approved_at DESC
          LIMIT 4
          `, function (error, results, fields) {
          if (error) {
            throw_error = error;
          } else {
            data['self_rewards'] = results;
          }

          return resolve();
        });
      });

      if (throw_error !== null) {
        connection.release();
        res.status(500).json({
          err: throw_error
        });
      } else {
        connection.release();
        res.json({
          data
        });
      }
    }
  });
});
router.get('/list_partner', (req, res) => {
  db.getConnection(function (err, connection) {
    if (err) {
      res.status(500).json({
        err
      });
    } else {
      let offset = 0,
          limit = 12,
          search = "";

      if (req.query.page && /^[0-9]*$/.test(req.query.page)) {
        offset = (parseInt(req.query.page) - 1) * limit;
      }

      if (req.query.search) {
        search = req.query.search;
      }

      connection.query(`SELECT COUNT(*) as total_rows 
        FROM partners
        WHERE active=1
        ${search !== '' ? 'AND (discount LIKE ? OR full_name LIKE ? OR city LIKE ?)' : ''}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results, fields) {
        if (error) {
          connection.release();
          res.status(500).json({
            error
          });
        } else {
          let rows_count = results[0].total_rows;
          connection.query(`SELECT id, full_name, email, discount, cont_num, city, address, logo
              FROM partners
              WHERE active=1
              ${search !== '' ? 'AND (discount LIKE ? OR full_name LIKE ? OR city LIKE ?)' : ''}
              ORDER BY id ASC
              LIMIT ${limit}
              OFFSET ${offset}`, ['%' + search + '%', '%' + search + '%', '%' + search + '%'], function (error, results, fields) {
            connection.release();

            if (error) {
              res.status(500).json({
                error
              });
            } else {
              res.json({
                data: results,
                total_rows: rows_count
              });
            }
          });
        }
      });
    }
  });
});
router.get('/partner_info/:id', function (req, res) {
  if (/^[0-9]*$/.test(req.params.id)) {
    db.getConnection(function (err, connection) {
      if (err) {
        res.status(500).json({
          err
        });
      } else {
        connection.query(`SELECT id, full_name, email, discount, disc_prds, cont_num, city, address, logo 
          FROM \`partners\` 
          WHERE \`id\`=?`, [req.params.id], function (error, results, fields) {
          connection.release();

          if (error) {
            res.status(500).json({
              error
            });
          } else {
            res.json({
              result: results[0]
            });
          }
        });
      }
    });
  } else {
    res.json({
      status: false,
      message: "Invalid id!"
    });
  }
});
router.get('/partner/logo/:file_name', function (req, res) {
  if (req.params.file_name !== '') {
    if (fs.existsSync(__dirname + "/../uploads/partners_logo/" + req.params.file_name)) {
      gm(__dirname + '/../uploads/partners_logo/' + req.params.file_name).gravity('Center').background('transparent').resize(320, 320).extent(320, 320).stream('jpg', function (err, stdout, stderr) {
        if (err) return res.status(404).json({
          message: "Not Found!"
        });
        stdout.pipe(res);
        stdout.on('error', function (err) {
          return res.status(404).json({
            message: "Not Found!"
          });
        });
      });
    } else {
      res.status(404).json({
        message: 'Not found!'
      });
    }
  } else {
    res.status(404).json({
      message: 'Not found!'
    });
  }
});
router.get('/user/img/:file_name', function (req, res) {
  if (req.params.file_name !== '') {
    if (fs.existsSync(__dirname + "/../uploads/profile/" + req.params.file_name)) {
      gm(__dirname + '/../uploads/profile/' + req.params.file_name).gravity('Center').background('transparent').resize(180, 180).extent(180, 180).stream('jpg', function (err, stdout, stderr) {
        if (err) return res.status(404).json({
          message: "Not Found!"
        });
        stdout.pipe(res);
        stdout.on('error', function (err) {
          return res.status(404).json({
            message: "Not Found!"
          });
        });
      });
    } else {
      res.status(404).json({
        message: 'Not found!'
      });
    }
  } else {
    res.status(404).json({
      message: 'Not found!'
    });
  }
});
router.get('/user/thumb/:file_name', function (req, res) {
  if (req.params.file_name !== '') {
    if (fs.existsSync(__dirname + "/../uploads/profile/" + req.params.file_name)) {
      gm(__dirname + '/../uploads/profile/' + req.params.file_name).gravity('Center').background('transparent').resize(50, 50).extent(50, 50).stream('jpg', function (err, stdout, stderr) {
        if (err) return res.status(404).json({
          message: "Not Found!"
        });
        stdout.pipe(res);
        stdout.on('error', function (err) {
          return res.status(404).json({
            message: "Not Found!"
          });
        });
      });
    } else {
      res.status(404).json({
        message: 'Not found!'
      });
    }
  } else {
    res.status(404).json({
      message: 'Not found!'
    });
  }
});
router.post('/check_email', (req, res) => {
  db.getConnection(function (err, connection) {
    if (err) {
      sendDBError(res, err);
    } else {
      connection.query('SELECT email FROM `members` where binary `email`=?', [req.body.email], function (error, results, fields) {
        if (error) {
          connection.release();
          sendDBError(res, error);
        } else {
          if (results.length > 0) {
            connection.release();
            res.json({
              count: results.length
            });
          } else {
            connection.query('SELECT email FROM `moderators` where binary `email`=?', [req.body.email], function (error, results, fields) {
              if (error) {
                connection.release();
                sendDBError(res, error);
              } else {
                if (results.length > 0) {
                  connection.release();
                  res.json({
                    count: results.length
                  });
                } else {
                  connection.query('SELECT email FROM `admins` where binary `email`=?', [req.body.email], function (error, results, fields) {
                    if (error) {
                      connection.release();
                      sendDBError(res, error);
                    } else {
                      connection.release();
                      res.json({
                        count: results.length
                      });
                    }
                  });
                }
              }
            });
          }
        }
      });
    }
  });
});
router.post('/check_ref_id', (req, res) => {
  db.getConnection(function (err, connection) {
    if (err) {
      sendDBError(res, err);
    } else {
      connection.query('SELECT user_asn_id, full_name FROM `members` where binary `user_asn_id`=?', [req.body.id], function (error, results, fields) {
        connection.release();

        if (error) {
          sendDBError(res, error);
        } else {
          if (results.length > 0) {
            res.json({
              count: results.length,
              user: results[0]
            });
          } else {
            res.json({
              count: results.length
            });
          }
        }
      });
    }
  });
});
router.post("/admin/login", (req, res) => {
  db.getConnection(async function (err, connection) {
    if (err) {
      sendDBError(res, err);
    } else {
      let throw_error = null;
      let resp = null; // moderator check

      await new Promise(resolve => {
        connection.query('SELECT id, email, active_sts FROM `moderators` WHERE BINARY `email`=? and BINARY `password`=?', [req.body.email, req.body.password], function (error, results) {
          if (error) {
            throw_error = error;
            return resolve();
          } else {
            if (results.length === 1) {
              if (results[0].active_sts === 1) {
                resp = {
                  status: true,
                  token: tokenGen(results[0], 1),
                  user: userData(results[0], 1)
                };
              } else {
                resp = {
                  status: false,
                  message: "Your account has bees Suspended. Contact your administrator."
                };
              }
            }

            return resolve();
          }
        });
      });

      if (resp) {
        connection.release();
        return res.json(resp);
      } else if (throw_error) {
        connection.release();
        return sendDBError(res, throw_error);
      } // admin check


      await new Promise(resolve => {
        connection.query('SELECT id, email FROM `admins` where BINARY `email`=? and BINARY `password`=?', [req.body.email, req.body.password], function (error, results) {
          if (error) {
            throw_error = error;
            return resolve();
          } else {
            if (results.length === 1) {
              resp = {
                status: true,
                token: tokenGen(results[0], 2),
                user: userData(results[0], 2)
              };
            } else {
              resp = {
                status: false,
                message: "Sorry, the specified combination do not match. Reset your credentials if required."
              };
            }

            return resolve();
          }
        });
      });

      if (resp) {
        connection.release();
        return res.json(resp);
      } else if (throw_error) {
        connection.release();
        return sendDBError(res, throw_error);
      }
    }
  });
});
router.post('/login', (req, res) => {
  db.getConnection(async function (err, connection) {
    if (err) {
      sendDBError(res, err);
    } else {
      let throw_error = null;
      let resp = null; // member check

      await new Promise(resolve => {
        connection.query('SELECT id, email, active_sts, is_paid_m FROM `members` WHERE BINARY (`email`=? OR `user_asn_id`=?) AND BINARY `password`=?', [req.body.email, req.body.email, req.body.password], function (error, results) {
          if (error) {
            throw_error = error;
            return resolve();
          } else {
            if (results.length === 1) {
              if (results[0].active_sts === 1) {
                user = userData(results[0], 0);
                user['is_paid'] = results[0].is_paid_m;
                resp = {
                  status: true,
                  token: tokenGen(results[0], 0),
                  user
                };
              } else {
                resp = {
                  status: false,
                  message: "Your account has bees Suspended. Contact your administrator."
                };
              }
            } else {
              resp = {
                status: false,
                message: "Sorry, the specified combination do not match. Reset your credentials if required."
              };
            }

            return resolve();
          }
        });
      });

      if (resp) {
        connection.release();
        return res.json(resp);
      } else if (throw_error) {
        connection.release();
        return sendDBError(res, throw_error);
      }
    }
  });
});
router.post('/signup', (req, res) => {
  db.getConnection(function (err, connection) {
    if (err) {
      sendDBError(res, err);
    } else {
      connection.beginTransaction(async function (err) {
        if (err) {
          connection.release();
          sendDBError(res, err);
        } else {
          let throw_error = null;
          let mem_id = null;
          let email_grab_data = {
            template_data: {},
            email: null,
            token_id: null
          };
          await new Promise(resolve => {
            req.body.member_data['active_sts'] = 1;
            connection.query('INSERT INTO `members` SET ?', req.body.member_data, async function (error, results, fields) {
              if (error) {
                throw_error = error;
                return resolve();
              } else {
                mem_id = results.insertId; // req.body.prd_data['member_id'] = mem_id
                // req.body.bank_data['member_id'] = mem_id

                email_grab_data['email'] = req.body.member_data.email;
                email_grab_data['template_data']['name'] = req.body.member_data.full_name;
                email_grab_data['template_data']['token'] = jwt.sign({
                  data: {
                    email: req.body.member_data.email,
                    user_id: mem_id,
                    type: 0
                  }
                }, secret, {
                  expiresIn: "1 day"
                });
                await new Promise(tokenResolve => {
                  connection.query(`INSERT INTO tokens SET ?`, {
                    type: 0,
                    member_id: mem_id,
                    token: email_grab_data['template_data']['token']
                  }, function (error, result) {
                    if (error) {
                      throw_error = error;
                    } else {
                      email_grab_data['token_id'] = result.insertId;
                    }

                    tokenResolve();
                  });
                });

                if (throw_error) {
                  return resolve();
                } // check member select promotion and if exist any promotion


                if (req.body.ext_data.promotion === true) {
                  let curr_date = moment(DateTime.local().setZone("UTC+5").toString()).format("YYYY-MM-DD HH-mm-ss");
                  await new Promise(promResolve => {
                    connection.query(`SELECT id FROM disc_promotions WHERE prd_id = ${req.body.ext_data.prd_id} AND (start_prom_dt<='${curr_date}' AND end_prom_dt>='${curr_date}') limit 1`, function (error, result) {
                      if (error) {
                        throw_error = error;
                        return promResolve();
                      } else {
                        if (!result.length) {
                          return promResolve();
                        } else {
                          let prom_id = result[0].id;
                          connection.query(`INSERT INTO mem_in_prom SET ?`, {
                            member_id: mem_id,
                            disc_prom_id: prom_id
                          }, function (error) {
                            if (error) {
                              throw_error = error;
                            }

                            return promResolve();
                          });
                        }
                      }
                    });
                  });

                  if (throw_error) {
                    return resolve();
                  }
                }

                connection.query(`INSERT INTO terms_accept SET member_id=${mem_id}, accept_sts=1`, function (error, results, fields) {
                  if (error) {
                    throw_error = error;
                    return resolve();
                  } else {
                    connection.query('INSERT INTO `user_product_details` SET ?', {
                      product_id: req.body.ext_data.prd_id,
                      member_id: mem_id
                    }, function (error, results, fields) {
                      if (error) {
                        throw_error = error;
                        return resolve();
                      } else {
                        connection.query('INSERT INTO `mem_link_crzb` SET ?', {
                          member_id: mem_id,
                          crzb_id: req.body.ext_data.brn_id,
                          linked_mem_type: 1
                        }, async function (error, results, fields) {
                          if (error) {
                            throw_error = error;
                            return resolve();
                          } else {
                            connection.query('INSERT INTO `notifications` SET ?', {
                              from_type: 0,
                              to_type: 1,
                              from_id: mem_id,
                              to_id: 1,
                              // admin id
                              message: "New member added in members list. Approve it.",
                              notify_type: 1
                            }, function (error, results, fields) {
                              if (error) {
                                throw_error = error;
                              }

                              return resolve();
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          });

          if (throw_error) {
            return connection.rollback(function () {
              connection.release();
              sendDBError(res, throw_error);
            });
          } else {
            connection.commit(function (err) {
              if (err) {
                return connection.rollback(function () {
                  connection.release();
                  sendDBError(res, err);
                });
              } else {
                connection.release();
                res.render("verify-token", {
                  host: config.dev ? 'http://127.0.0.1:3000' : 'https://mj-supreme.com',
                  name: email_grab_data['template_data']['name'],
                  token: email_grab_data['template_data']['token']
                }, function (errPug, html) {
                  if (errPug) {
                    last_id_delete_token(email_grab_data['token_id'], function (err) {
                      res.json({
                        status: false,
                        message: err ? err.message : errPug.message
                      });
                    });
                  } else {
                    trans_email.sendMail({
                      from: '"MJ Supreme" <info@mj-supreme.com>',
                      to: email_grab_data['email'],
                      subject: 'Verification Token',
                      html: html
                    }, function (err, info) {
                      if (err) {
                        last_id_delete_token(email_grab_data['token_id'], function (cb_err) {
                          res.json({
                            status: false,
                            message: cb_err ? cb_err.message : err.message
                          });
                        });
                      } else {
                        res.json({
                          status: true,
                          token: tokenGen({
                            email: email_grab_data['email'],
                            type: 0,
                            id: mem_id
                          }, 0),
                          user: {
                            email: email_grab_data['email'],
                            type: 0,
                            user_id: mem_id,
                            is_paid: 0
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        }
      });
    }
  });
});
module.exports = router;

function sendDBError(res, err) {
  res.status(500).json({
    success: false,
    message: "DB Error: " + err.message
  });
}

function last_id_delete_token(lastId, cb) {
  db.getConnection(function (error, connection) {
    if (error) {
      cb(error);
    } else {
      connection.query('DELETE FROM tokens WHERE id=?', lastId, function (error, result) {
        connection.release();

        if (error) {
          cb(error);
        } else {
          cb();
        }
      });
    }
  });
}

function tokenGen(data, type) {
  let token = jwt.sign({
    data: {
      email: data.email,
      user_id: data.id,
      type: type
    }
  }, secret, {
    expiresIn: "1 days"
  });
  return token;
}

function userData(data, type) {
  return {
    email: data.email,
    type: type,
    user_id: data.id
  };
}
/* WEBPACK VAR INJECTION */}.call(this, "server\\apis"))

/***/ }),

/***/ "./server/config.js":
/*!**************************!*\
  !*** ./server/config.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  dev: true,
  db: {
    //autocomment         live: {
    //autocomment             host: 'localhost',
    //autocomment             user: 'mjsupr5_mj_usr',
    //autocomment             password: '9;jlk^cWH-k^',
    //autocomment             database: 'mjsupr5_mj_db'
    //autocomment         },
    local: {
      host: 'localhost',
      user: 'root',
      password: '',
      database: 'mjsupr5_mj_db'
    }
  },
  secret: "MJSecret33..22.1"
};

/***/ }),

/***/ "./server/db.js":
/*!**********************!*\
  !*** ./server/db.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const mysql = __webpack_require__(/*! mysql */ "mysql");

const config = __webpack_require__(/*! ./config.js */ "./server/config.js"); // live or local


const db = mysql.createPool(config.dev === true ? config.db.local : config.db.live);
module.exports = db;

/***/ }),

/***/ "./server/e-conf.js":
/*!**************************!*\
  !*** ./server/e-conf.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let nodemailer = __webpack_require__(/*! nodemailer */ "nodemailer");

const config = {
  "host": "vps44520.servconfig.com",
  "port": 465,
  "secure": true,
  "auth": {
    "user": "info@mj-supreme.com",
    "pass": "Unitedengineering171@gmail.com"
  }
};
const transporter = nodemailer.createTransport(config);
module.exports = transporter; // router.get('/email_test', function (req, res) {
//   trans_email.sendMail({
//     from: '"MJ Supreme" <info@mj-supreme.com>',
//     to: '<email receiver>',
//     subject: 'New User',
//     text: 'Hello world?', // plain text body
//     //html: "<b>This is html</b>"
//   }, function (err, info) {
//     if (err) {
//       console.log(err);
//       return res.json({
//         status: "failed",
//         message: "Error: Mail not send!"
//       });
//     }
//     return res.json({
//       status: "ok",
//       info: info
//     });
//   })
// })

/***/ }),

/***/ "./server/func/db-util.js":
/*!********************************!*\
  !*** ./server/func/db-util.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  connectTrans: async function (connection, queryFunc, cb) {
    connection.beginTransaction(async function (error) {
      if (error) {
        connection.release();
        return cb(error);
      } else {
        let throw_error = null;
        await new Promise(resolve => {
          queryFunc(resolve, function (hdl_err) {
            if (hdl_err) {
              throw_error = hdl_err;
            }
          });
        });

        if (throw_error) {
          return connection.rollback(function () {
            connection.release();
            cb(throw_error);
          });
        } else {
          connection.commit(function (err) {
            if (err) {
              return connection.rollback(function () {
                connection.release();
                cb(err);
              });
            } else {
              connection.release();
              cb(null);
            }
          });
        }
      }
    });
  }
};

/***/ }),

/***/ "./server/index.js":
/*!*************************!*\
  !*** ./server/index.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__dirname) {const {
  Nuxt,
  Builder
} = __webpack_require__(/*! nuxt */ "nuxt");

const secret = __webpack_require__(/*! ./config */ "./server/config.js").secret;

const app = __webpack_require__(/*! express */ "express")(); // const server = require('http').Server(app)


const port_http = 3000; // const port_https = 443

const path = __webpack_require__(/*! path */ "path"); // const fs = require('fs')


const http = __webpack_require__(/*! http */ "http"); // const https = require('https')
// const credentials = {
// 	key: fs.readFileSync(__dirname+'/./ssl/key.key'),
// 	cert: fs.readFileSync(__dirname+'/./ssl/cert.crt'),
// 	ca: fs.readFileSync(__dirname+'/./ssl/bundle.ca-bundle')
// };
// const httpsServer = https.createServer(credentials, app)


const httpServer = http.createServer(app); // here enable ssl and proxy
// app.enable("trust proxy");
// app.use(function (req, res, next) {
//     if (req.secure) {
//         return next();
//     }
//     res.redirect('https://' + req.headers.host + req.url);
// });

app.disable('x-powered-by');

const bodyParser = __webpack_require__(/*! body-parser */ "body-parser");

const jwt = __webpack_require__(/*! jsonwebtoken */ "jsonwebtoken");

const io = __webpack_require__(/*! socket.io */ "socket.io")(httpServer);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

const apis = __webpack_require__(/*! ./apis */ "./server/apis/index.js");

app.use('/api', apis); // We instantiate Nuxt.js with the options

let config = __webpack_require__(/*! ../nuxt.config.js */ "./nuxt.config.js");

const nuxt = new Nuxt(config);
app.use(nuxt.render);

if (config.dev) {
  new Builder(nuxt).build().catch(error => {
    console.error(error);
    process.exit(1);
  });
} // Listen the server
// server.listen(port, () => console.log('Start Nuxt Project!'))


httpServer.listen(port_http, () => {
  console.log('HTTP Server running on port ' + port_http);
}); // httpsServer.listen(port_https, () => {
// 	console.log('HTTPS Server running on port 443');
// });

let client_connected = {};
io.on('connection', function (socket) {
  client_connected[socket.id] = {};
  client_connected[socket.id].token = signedToken(socket.id);
  client_connected[socket.id].interval = setInterval(() => {
    jwt.verify(client_connected[socket.id].token, secret, function (err, decoded) {
      if (err) {
        if (err.name === "TokenExpiredError") {
          client_connected[socket.id].token = signedToken(socket.id);
          socket.emit("token", {
            token: client_connected[socket.id].token
          });
        } else {
          socket.emit("token", {
            error: err
          });
        }
      } else {
        socket.emit("token", {
          token: client_connected[socket.id].token
        });
      }
    });
  }, 300000); // 5 mins

  socket.emit("token", {
    token: client_connected[socket.id].token
  });
  socket.on('disconnect', function () {
    clearInterval(client_connected[socket.id].interval);
    delete client_connected[socket.id];
  });
});

function signedToken(data) {
  let token = jwt.sign({
    data: data
  }, secret, {
    expiresIn: "5 minutes"
  });
  return token;
}
/* WEBPACK VAR INJECTION */}.call(this, "server"))

/***/ }),

/***/ 0:
/*!*******************************!*\
  !*** multi ./server/index.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./server/index.js */"./server/index.js");


/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),

/***/ "csv-writer":
/*!*****************************!*\
  !*** external "csv-writer" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("csv-writer");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ "gm":
/*!*********************!*\
  !*** external "gm" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("gm");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),

/***/ "jsonwebtoken":
/*!*******************************!*\
  !*** external "jsonwebtoken" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("jsonwebtoken");

/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ "luxon":
/*!************************!*\
  !*** external "luxon" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("luxon");

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "multer":
/*!*************************!*\
  !*** external "multer" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("multer");

/***/ }),

/***/ "mysql":
/*!************************!*\
  !*** external "mysql" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mysql");

/***/ }),

/***/ "nodemailer":
/*!*****************************!*\
  !*** external "nodemailer" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("nodemailer");

/***/ }),

/***/ "nuxt":
/*!***********************!*\
  !*** external "nuxt" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("nuxt");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),

/***/ "request":
/*!**************************!*\
  !*** external "request" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("request");

/***/ }),

/***/ "shortid":
/*!**************************!*\
  !*** external "shortid" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("shortid");

/***/ }),

/***/ "socket.io":
/*!****************************!*\
  !*** external "socket.io" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("socket.io");

/***/ })

/******/ });
//# sourceMappingURL=main.map