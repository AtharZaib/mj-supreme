import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'

const _285437d3 = () => interopDefault(import('..\\pages\\about-us.vue' /* webpackChunkName: "pages_about-us" */))
const _5daa61d2 = () => interopDefault(import('..\\pages\\blog.vue' /* webpackChunkName: "pages_blog" */))
const _46acfb46 = () => interopDefault(import('..\\pages\\business-chart.vue' /* webpackChunkName: "pages_business-chart" */))
const _1b35549a = () => interopDefault(import('..\\pages\\career.vue' /* webpackChunkName: "pages_career" */))
const _c583d93c = () => interopDefault(import('..\\pages\\coming-soon.vue' /* webpackChunkName: "pages_coming-soon" */))
const _dc1ce08a = () => interopDefault(import('..\\pages\\contact.vue' /* webpackChunkName: "pages_contact" */))
const _67727122 = () => interopDefault(import('..\\pages\\dashboard.vue' /* webpackChunkName: "pages_dashboard" */))
const _0e35bb74 = () => interopDefault(import('..\\pages\\downloads.vue' /* webpackChunkName: "pages_downloads" */))
const _d98fd408 = () => interopDefault(import('..\\pages\\help-center.vue' /* webpackChunkName: "pages_help-center" */))
const _bea38184 = () => interopDefault(import('..\\pages\\lucky-draw.vue' /* webpackChunkName: "pages_lucky-draw" */))
const _c7aa8382 = () => interopDefault(import('..\\pages\\media.vue' /* webpackChunkName: "pages_media" */))
const _17188541 = () => interopDefault(import('..\\pages\\messages.vue' /* webpackChunkName: "pages_messages" */))
const _20934e43 = () => interopDefault(import('..\\pages\\notifications.vue' /* webpackChunkName: "pages_notifications" */))
const _3149e748 = () => interopDefault(import('..\\pages\\partners-and-associates.vue' /* webpackChunkName: "pages_partners-and-associates" */))
const _3699390a = () => interopDefault(import('..\\pages\\product.vue' /* webpackChunkName: "pages_product" */))
const _4e18b9ff = () => interopDefault(import('..\\pages\\product-details.vue' /* webpackChunkName: "pages_product-details" */))
const _bf8ced4e = () => interopDefault(import('..\\pages\\products.vue' /* webpackChunkName: "pages_products" */))
const _59bc624d = () => interopDefault(import('..\\pages\\signup.vue' /* webpackChunkName: "pages_signup" */))
const _a39aae10 = () => interopDefault(import('..\\pages\\signup-merchant.vue' /* webpackChunkName: "pages_signup-merchant" */))
const _29bd9b0c = () => interopDefault(import('..\\pages\\terms-and-condition.vue' /* webpackChunkName: "pages_terms-and-condition" */))
const _8c821488 = () => interopDefault(import('..\\pages\\winners-of-the-month.vue' /* webpackChunkName: "pages_winners-of-the-month" */))
const _783d0002 = () => interopDefault(import('..\\pages\\withdraw.vue' /* webpackChunkName: "pages_withdraw" */))
const _f6b25866 = () => interopDefault(import('..\\pages\\account\\vouchers.vue' /* webpackChunkName: "pages_account_vouchers" */))
const _2467302f = () => interopDefault(import('..\\pages\\admin\\login.vue' /* webpackChunkName: "pages_admin_login" */))
const _204eb481 = () => interopDefault(import('..\\pages\\campaign\\create.vue' /* webpackChunkName: "pages_campaign_create" */))
const _b1fff230 = () => interopDefault(import('..\\pages\\company-chart\\assign-franchise.vue' /* webpackChunkName: "pages_company-chart_assign-franchise" */))
const _3dc1e388 = () => interopDefault(import('..\\pages\\company-chart\\assign-roles.vue' /* webpackChunkName: "pages_company-chart_assign-roles" */))
const _486effdc = () => interopDefault(import('..\\pages\\company-chart\\hierarchy.vue' /* webpackChunkName: "pages_company-chart_hierarchy" */))
const _f246da9a = () => interopDefault(import('..\\pages\\company-chart\\sales.vue' /* webpackChunkName: "pages_company-chart_sales" */))
const _64b60075 = () => interopDefault(import('..\\pages\\company-chart\\sales-commission.vue' /* webpackChunkName: "pages_company-chart_sales-commission" */))
const _86f4782a = () => interopDefault(import('..\\pages\\company-chart\\zone-management.vue' /* webpackChunkName: "pages_company-chart_zone-management" */))
const _7b5e0e6a = () => interopDefault(import('..\\pages\\fund-manager\\bank-details.vue' /* webpackChunkName: "pages_fund-manager_bank-details" */))
const _76f90fc4 = () => interopDefault(import('..\\pages\\fund-manager\\commission-paid.vue' /* webpackChunkName: "pages_fund-manager_commission-paid" */))
const _f43a4546 = () => interopDefault(import('..\\pages\\fund-manager\\commission-unpaid.vue' /* webpackChunkName: "pages_fund-manager_commission-unpaid" */))
const _f2f7ffb6 = () => interopDefault(import('..\\pages\\fund-manager\\finance-details.vue' /* webpackChunkName: "pages_fund-manager_finance-details" */))
const _74746a80 = () => interopDefault(import('..\\pages\\fund-manager\\invoices.vue' /* webpackChunkName: "pages_fund-manager_invoices" */))
const _122d705c = () => interopDefault(import('..\\pages\\fund-manager\\total-finances.vue' /* webpackChunkName: "pages_fund-manager_total-finances" */))
const _72a6546e = () => interopDefault(import('..\\pages\\members-area\\add-new-member.vue' /* webpackChunkName: "pages_members-area_add-new-member" */))
const _50e1182f = () => interopDefault(import('..\\pages\\members-area\\members-profile.vue' /* webpackChunkName: "pages_members-area_members-profile" */))
const _b718ac6c = () => interopDefault(import('..\\pages\\members-area\\total-members.vue' /* webpackChunkName: "pages_members-area_total-members" */))
const _3c32c235 = () => interopDefault(import('..\\pages\\members-area\\total-members\\active-members-report.vue' /* webpackChunkName: "pages_members-area_total-members_active-members-report" */))
const _f82965a0 = () => interopDefault(import('..\\pages\\members-area\\total-members\\inactive-members-report.vue' /* webpackChunkName: "pages_members-area_total-members_inactive-members-report" */))
const _2ee1e20e = () => interopDefault(import('..\\pages\\moderators\\add-new-moderator.vue' /* webpackChunkName: "pages_moderators_add-new-moderator" */))
const _5fa709ad = () => interopDefault(import('..\\pages\\moderators\\moderators-profile.vue' /* webpackChunkName: "pages_moderators_moderators-profile" */))
const _093ed895 = () => interopDefault(import('..\\pages\\partners-area\\add-new-partner.vue' /* webpackChunkName: "pages_partners-area_add-new-partner" */))
const _d90d875e = () => interopDefault(import('..\\pages\\partners-area\\partners-profile.vue' /* webpackChunkName: "pages_partners-area_partners-profile" */))
const _43b52373 = () => interopDefault(import('..\\pages\\rewards\\rewards-completed.vue' /* webpackChunkName: "pages_rewards_rewards-completed" */))
const _6f2a21b7 = () => interopDefault(import('..\\pages\\rewards\\rewards-request.vue' /* webpackChunkName: "pages_rewards_rewards-request" */))
const _39f21321 = () => interopDefault(import('..\\pages\\system-level\\auto-rewards.vue' /* webpackChunkName: "pages_system-level_auto-rewards" */))
const _382900aa = () => interopDefault(import('..\\pages\\system-level\\campaign.vue' /* webpackChunkName: "pages_system-level_campaign" */))
const _06f3813e = () => interopDefault(import('..\\pages\\system-level\\self-rewards.vue' /* webpackChunkName: "pages_system-level_self-rewards" */))
const _93a2d91e = () => interopDefault(import('..\\pages\\user\\bank-details.vue' /* webpackChunkName: "pages_user_bank-details" */))
const _23367e38 = () => interopDefault(import('..\\pages\\user\\nomination.vue' /* webpackChunkName: "pages_user_nomination" */))
const _3d3c9519 = () => interopDefault(import('..\\pages\\user\\profile.vue' /* webpackChunkName: "pages_user_profile" */))
const _8aab7834 = () => interopDefault(import('..\\pages\\unsubscribe\\_token.vue' /* webpackChunkName: "pages_unsubscribe__token" */))
const _c59cabd8 = () => interopDefault(import('..\\pages\\verify-token\\_token.vue' /* webpackChunkName: "pages_verify-token__token" */))
const _27175fa6 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

Vue.use(Router)

const scrollBehavior = function (to, from, savedPosition) {
      return {
        x: 0,
        y: 0
      };
    }

export function createRouter() {
  return new Router({
    mode: 'history',
    base: decodeURI('/'),
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,

    routes: [{
      path: "/about-us",
      component: _285437d3,
      name: "about-us"
    }, {
      path: "/blog",
      component: _5daa61d2,
      name: "blog"
    }, {
      path: "/business-chart",
      component: _46acfb46,
      name: "business-chart"
    }, {
      path: "/career",
      component: _1b35549a,
      name: "career"
    }, {
      path: "/coming-soon",
      component: _c583d93c,
      name: "coming-soon"
    }, {
      path: "/contact",
      component: _dc1ce08a,
      name: "contact"
    }, {
      path: "/dashboard",
      component: _67727122,
      name: "dashboard"
    }, {
      path: "/downloads",
      component: _0e35bb74,
      name: "downloads"
    }, {
      path: "/help-center",
      component: _d98fd408,
      name: "help-center"
    }, {
      path: "/lucky-draw",
      component: _bea38184,
      name: "lucky-draw"
    }, {
      path: "/media",
      component: _c7aa8382,
      name: "media"
    }, {
      path: "/messages",
      component: _17188541,
      name: "messages"
    }, {
      path: "/notifications",
      component: _20934e43,
      name: "notifications"
    }, {
      path: "/partners-and-associates",
      component: _3149e748,
      name: "partners-and-associates"
    }, {
      path: "/product",
      component: _3699390a,
      name: "product"
    }, {
      path: "/product-details",
      component: _4e18b9ff,
      name: "product-details"
    }, {
      path: "/products",
      component: _bf8ced4e,
      name: "products"
    }, {
      path: "/signup",
      component: _59bc624d,
      name: "signup"
    }, {
      path: "/signup-merchant",
      component: _a39aae10,
      name: "signup-merchant"
    }, {
      path: "/terms-and-condition",
      component: _29bd9b0c,
      name: "terms-and-condition"
    }, {
      path: "/winners-of-the-month",
      component: _8c821488,
      name: "winners-of-the-month"
    }, {
      path: "/withdraw",
      component: _783d0002,
      name: "withdraw"
    }, {
      path: "/account/vouchers",
      component: _f6b25866,
      name: "account-vouchers"
    }, {
      path: "/admin/login",
      component: _2467302f,
      name: "admin-login"
    }, {
      path: "/campaign/create",
      component: _204eb481,
      name: "campaign-create"
    }, {
      path: "/company-chart/assign-franchise",
      component: _b1fff230,
      name: "company-chart-assign-franchise"
    }, {
      path: "/company-chart/assign-roles",
      component: _3dc1e388,
      name: "company-chart-assign-roles"
    }, {
      path: "/company-chart/hierarchy",
      component: _486effdc,
      name: "company-chart-hierarchy"
    }, {
      path: "/company-chart/sales",
      component: _f246da9a,
      name: "company-chart-sales"
    }, {
      path: "/company-chart/sales-commission",
      component: _64b60075,
      name: "company-chart-sales-commission"
    }, {
      path: "/company-chart/zone-management",
      component: _86f4782a,
      name: "company-chart-zone-management"
    }, {
      path: "/fund-manager/bank-details",
      component: _7b5e0e6a,
      name: "fund-manager-bank-details"
    }, {
      path: "/fund-manager/commission-paid",
      component: _76f90fc4,
      name: "fund-manager-commission-paid"
    }, {
      path: "/fund-manager/commission-unpaid",
      component: _f43a4546,
      name: "fund-manager-commission-unpaid"
    }, {
      path: "/fund-manager/finance-details",
      component: _f2f7ffb6,
      name: "fund-manager-finance-details"
    }, {
      path: "/fund-manager/invoices",
      component: _74746a80,
      name: "fund-manager-invoices"
    }, {
      path: "/fund-manager/total-finances",
      component: _122d705c,
      name: "fund-manager-total-finances"
    }, {
      path: "/members-area/add-new-member",
      component: _72a6546e,
      name: "members-area-add-new-member"
    }, {
      path: "/members-area/members-profile",
      component: _50e1182f,
      name: "members-area-members-profile"
    }, {
      path: "/members-area/total-members",
      component: _b718ac6c,
      name: "members-area-total-members",
      children: [{
        path: "active-members-report",
        component: _3c32c235,
        name: "members-area-total-members-active-members-report"
      }, {
        path: "inactive-members-report",
        component: _f82965a0,
        name: "members-area-total-members-inactive-members-report"
      }]
    }, {
      path: "/moderators/add-new-moderator",
      component: _2ee1e20e,
      name: "moderators-add-new-moderator"
    }, {
      path: "/moderators/moderators-profile",
      component: _5fa709ad,
      name: "moderators-moderators-profile"
    }, {
      path: "/partners-area/add-new-partner",
      component: _093ed895,
      name: "partners-area-add-new-partner"
    }, {
      path: "/partners-area/partners-profile",
      component: _d90d875e,
      name: "partners-area-partners-profile"
    }, {
      path: "/rewards/rewards-completed",
      component: _43b52373,
      name: "rewards-rewards-completed"
    }, {
      path: "/rewards/rewards-request",
      component: _6f2a21b7,
      name: "rewards-rewards-request"
    }, {
      path: "/system-level/auto-rewards",
      component: _39f21321,
      name: "system-level-auto-rewards"
    }, {
      path: "/system-level/campaign",
      component: _382900aa,
      name: "system-level-campaign"
    }, {
      path: "/system-level/self-rewards",
      component: _06f3813e,
      name: "system-level-self-rewards"
    }, {
      path: "/user/bank-details",
      component: _93a2d91e,
      name: "user-bank-details"
    }, {
      path: "/user/nomination",
      component: _23367e38,
      name: "user-nomination"
    }, {
      path: "/user/profile",
      component: _3d3c9519,
      name: "user-profile"
    }, {
      path: "/unsubscribe/:token?",
      component: _8aab7834,
      name: "unsubscribe-token"
    }, {
      path: "/verify-token/:token?",
      component: _c59cabd8,
      name: "verify-token-token"
    }, {
      path: "/",
      component: _27175fa6,
      name: "index"
    }],

    fallback: false
  })
}
